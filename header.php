<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

//var_dump( get_queried_object() ); 
 ?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <?php if( wpct_theme_get("sub_theme") != "coreui" ) : ?><div class="hfeed site" id="page"><?php endif;?>
        <a class="skip-link screen-reader-text sr-only" href="#content"><?php esc_html_e( 'Skip to content', 'gamer-life' ); ?></a> 
        <header class="app-header navbar <?php echo wpct_get_mod( 'navbar_scheme' ) ?>" style="background-color: <?php echo wpct_get_mod('navbar_bgcolor') ?>">
            <a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" id="site-brand-main">
                <?php wpct_get_template("brand.php",array("pos"=>"mainnav") ); ?>
                <?php // echo wpct_brand(); ?>
                <?php wpctcc_fe_edit('brand'); ?>
            </a>
        
            <!-- The WordPress Menu goes here -->
            <?php 
                $main_navmenu_args = apply_filters( 'wpct_main_nav_menu_args', array(
                    'theme_location'  => 'primary',
                    'container_class' => 'collapse navbar-collapse',
                    'container_id'    => 'navbarSupportedContent',
                    'menu_class'      => 'navbar-nav d-md-down-none',
                    'fallback_cb'     => 'wpct_main_nav_fallback',
                    'menu_id'         => 'main-menu',
                    'walker'          => new WPCT_Walker_Nav_Bootstrap(),
                ) );
                wp_nav_menu( $main_navmenu_args );
            ?>
            
            <div class="dropdown">
                <a class="btn dropdown-toggle btn-fx btn-fx--effect-boris" href="javascript:void(0)" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                	<i class="fa fa-ellipsis-h" aria-hidden="true"></i>
                </a>     
                <?php $main_dotmenu_args = apply_filters( 'wpct_main_dot_menu_args', array(
                    'theme_location'  => 'dotmenu',
                    'container_class' => 'dropdown-menu',
                    //'container_id'    => 'navbarSupportedContent',
                    //'menu_class'      => 'navbar-nav d-md-down-none',
                    'fallback_cb'     => 'wpct_dot_nav_fallback',
                    'menu_id'         => 'dot-menu',
                    'walker'          => new WPCT_Walker_Nav_Bootstrap(),
                ) );
                wp_nav_menu( $main_dotmenu_args );
				?>
            </div>
            <div>
                <!-- Search -->
                <a class="btn d-lg-none btn-fx btn-fx--effect-boris" data-toggle="collapse" href="#navsearchCollapse" aria-expanded="false" aria-controls="navsearchCollapse">
                	<i class="fa fa-search-plus" aria-hidden="true"></i>
				</a>      
                <div id="navsearchCollapse" class="collapse d-lg-block">
                    <?php wpct_get_search_form('nav') ?>
                </div>
    		</div>
                <ul class="nav navbar-nav ml-auto">
                    <!-- Notifications -->
                    <?php $navbar_notifications = wpct_get("navbar_notifications"); 
                    //  var_dump( $navbar_notifications );
                    
                    ?>
                	<?php if(  $navbar_notifications != "none" ) : ?>
                    <?php wpct_get_template("notification/" . $navbar_notifications) ?>
                    <?php endif; ?>
                    
                     <!-- User avatar n dropdown -->
                    <?php wpct_get_template("user-dropdown.php") ?>
                </ul><!-- end ul -->
	        </header>
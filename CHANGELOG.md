﻿# Changelog
Describes the changes made to the theme.

## [Unreleased]
- GiantBomb Integration
- Twitch Integration

## [1.0.0] - 2017-09-29
### Added
- Initial Release.
### Fixed
### Changed
### Removed

## [Considerations]
- 

adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).
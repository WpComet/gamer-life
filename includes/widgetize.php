<?php	/** Sidebars & Widgetizes Areas */

register_nav_menus(
	array(
		'main-nav' => __( 'The Main Menu', 'gamer-life' ),   // main nav in header
		'footer-links' => __( 'Footer Links', 'gamer-life' ), // secondary nav in footer
		'sidebar-primary' => __( 'Sidebar Primary', 'gamer-life' ),
		'sidebar-secondary' => __( 'Sidebar Secondary', 'gamer-life' ),
		'dot-menu' => __( 'Dot Menu', 'gamer-life' ),
	)
);

add_action( 'widgets_init', 'wpct_register_widget_areas' );
function wpct_register_widget_areas() {
	register_sidebar( array(
		'name'          => __( 'Sidebar - Primary', 'understrap' ),
		'id'            => 'sidebar_primary',
		'description'   => 'Primary sidebar area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Sidebar - Secondary', 'understrap' ),
		'id'            => 'sidebar_secondary',
		'description'   => 'Right sidebar widget area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Showcase Primary', 'gamer-life' ),
		'id'            => 'intro_primary',
		'description'   => 'Primary Intro area right after header. A good place for an introducion or welcome message.',
	//	'before_widget' => '<div class="carousel-item">',
	//	'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Showcase Secondary', 'gamer-life' ),
		'id'            => 'intro_secondary',
		'description'   => 'Secondary Intro area beside / after primary. A good place for a slider',
	//	'before_widget' => '<div class="carousel-item">',
	//	'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Features', 'understrap' ),
		'id'            => 'features',
		'description'   => 'Show a number of features you wanna promote here.',
		'before_widget'  => '<div id="%1$s" class="static-hero-widget %2$s '. wpct_count_widgets( 'featured' ) .'">', 
		'after_widget'   => '</div><!-- .static-hero-widget -->', 
		'before_title'   => '<h3 class="widget-title">', 
		'after_title'    => '</h3>',
	) );
    
	register_sidebar( array(
		'name'          => __( 'Footer', 'understrap' ),
		'id'            => 'footer',
		'description'   => 'Widget area below main content and above footer',
		'before_widget'  => '<div id="%1$s" class="footer-widget %2$s '. wpct_count_widgets( 'footerfull' ) .'">', 
		'after_widget'   => '</div><!-- .footer-widget -->', 
		'before_title'   => '<h3 class="widget-title">', 
		'after_title'    => '</h3>', 
	) );
	
	register_sidebar(array(
      'id' => 'header-center',
      'name' => 'Header - Center',
	  'description' => 'Display after main navigation in header. Good place to display a search widget.',
      'before_widget' => '<div id="%1$s" class="widget col-sm-4 %2$s">',
      'after_widget' => '</div>',
      'before_title' => '<h4 class="widgettitle">',
      'after_title' => '</h4>',
    ));
	
	register_sidebar(array(
      'id' => 'header-right',
      'name' => 'Header - Right',
	  'description' => 'Display on the right side of header. Ideal for Login, register, contact, language etc icons.',
      'before_widget' => '<div id="%1$s" class="widget col-sm-4 %2$s">',
      'after_widget' => '</div>',
      'before_title' => '<h4 class="widgettitle">',
      'after_title' => '</h4>',
    ));
}

add_action( 'widgets_init', 'wpct_register_widgets' );

// Register and load the widgets
function wpct_register_widgets() {
    register_widget( 'WPCT_Widget_Carousel' );
    register_widget( 'WPCT_Widget_Jumbotron' );
}
?>
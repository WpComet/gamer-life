<?php
/**
 * Layout Hooks:
 * 
 * 		wpct_body_start
 * 
 * 		wpct_header_before
 * 		wpct_header_start
 * 		wpct_header_end
 * 		wpct_header_after
 * 
 * 		wpct_layout_before
 * 
 * 		wpct_content_before 
 * 		wpct_content_start
 * 
 * 		wpct_post_before
 * 		wpct_post_start
 *		wpct_post_end
 * 		wpct_post_after
 * 
 * 		wpct_comment_before
 * 		wpct_comment_start
 * 		wpct_comment_end
 * 		wpct_comment_after
 * 
 *		wpct_content_end
 * 		wpct_content_after
 * 
 * 		wpct_sidebar_before
 * 		wpct_sidebar_start
 * 		wpct_sidebar_end
 * 		wpct_sidebar_after
 * 
 * 		wpct_layout_after
 * 
 * 		wpct_footer_before
 * 		wpct_footer_start
 * 		wpct_footer_end
 *		wpct_footer_after
 * 
 *		wpct_body_end
 * 
 * Theme Feature Hooks:
 * 
 * 		welcome_before
 * 		welcome_start
 * 		welcome_end
 * 		welcome_after
 * 
 * 		slider_before
 * 		slider_start
 * 		slider_end
 *		slider_after
 * 
 * 		footer_slider_before
 * 		footer_slider_start
 * 		footer_slider_end
 * 		footer_slider_after
 * 		
 * 		wpct_product_slider_add_to_cart_before
 * 		wpct_product_slider_add_to_cart_after
 * 
***************************************************************************/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Layout Hooks
 */

function wpct_body_start() { 		do_action( 'wpct_body_start' 	);}

function wpct_header_before() { 		do_action( 'wpct_header_before' 	);}
function wpct_header_start() { 		do_action( 'wpct_header_start' 	);}
function wpct_header_end() { 		do_action( 'wpct_header_end' 	);}
function wpct_header_after(){ 		do_action( 'wpct_header_after'	);}

function wpct_layout_before() { 		do_action( 'wpct_layout_before' 	);}

function wpct_content_before (){		do_action( 'wpct_content_before' );}
function wpct_content_start(){ 		do_action( 'wpct_content_start' 	);}

function wpct_post_before(){ 		do_action( 'wpct_post_before' 	);}
function wpct_post_start() { 		do_action( 'wpct_post_start' 	);}

function wpct_before_post_image() { 	do_action( 'wpct_before_post_image' 	);}
function wpct_after_post_image() { 	do_action( 'wpct_after_post_image' 	);}

function wpct_before_post_title() { 	do_action( 'wpct_before_post_title' );}
function wpct_after_post_title() { 	do_action( 'wpct_after_post_title' 	);}

function wpct_before_post_content(){ do_action( 'wpct_before_post_content' );}
function wpct_after_post_content() { do_action( 'wpct_after_post_content'  );}

function wpct_post_end() { 			do_action( 'wpct_post_end' 		);}
function wpct_post_after() { 		do_action( 'wpct_post_after' 	);}

function wpct_comment_before() { 	do_action( 'wpct_comment_before' );}
function wpct_comment_start() { 		do_action( 'wpct_comment_start' 	);}
function wpct_comment_end() { 		do_action( 'wpct_comment_end' 	);}
function wpct_comment_after() { 		do_action( 'wpct_comment_after' 	);}

function wpct_content_end() { 		do_action( 'wpct_content_end' 	);}
function wpct_content_after() { 		do_action( 'wpct_content_after' 	);}

function wpct_sidebar_before(){ 		do_action( 'wpct_sidebar_before' );}
function wpct_sidebar_start (){		do_action( 'wpct_sidebar_start' 	);}
function wpct_sidebar_end(){ 		do_action( 'wpct_sidebar_end' 	);}
function wpct_sidebar_after(){ 		do_action( 'wpct_sidebar_after' 	);}

function wpct_layout_after() { 		do_action( 'wpct_layout_after' 	);}

function wpct_footer_before() { 		do_action( 'wpct_footer_before' 	);}
function wpct_footer_start() { 		do_action( 'wpct_footer_start' 	);}
function wpct_footer_end() { 		do_action( 'wpct_footer_end' 	);}
function wpct_footer_after() { 		do_action( 'wpct_footer_after' 	);}

function wpct_body_end() { 			do_action( 'wpct_body_end' 		);}


/**
 * Theme Features Hooks
 */

function wpct_welcome_before(){		do_action( 'wpct_welcome_before' );}
function wpct_welcome_start(){		do_action( 'wpct_welcome_start' 	);}
function wpct_welcome_end(){			do_action( 'wpct_welcome_end' 	);}
function wpct_welcome_after(){		do_action( 'wpct_welcome_after' 	);}

function wpct_slider_before(){		do_action( 'wpct_slider_before' 	);}
function wpct_slider_start(){		do_action( 'wpct_slider_start' 	);}
function wpct_slider_end(){			do_action( 'wpct_slider_end'		);}
function wpct_slider_after(){		do_action( 'wpct_slider_after' 	);}

function wpct_footer_slider_before(){do_action( 'wpct_footer_slider_before' );}
function wpct_footer_slider_start(){ do_action( 'wpct_footer_slider_start'  );}
function wpct_footer_slider_end(){ 	do_action( 'wpct_footer_slider_end' 	  );}
function wpct_footer_slider_after(){ do_action( 'wpct_footer_slider_after'  );}

function wpct_sidebar_alt_before(){ 	do_action( 'wpct_sidebar_alt_before'	);}
function wpct_sidebar_alt_start(){ 	do_action( 'wpct_sidebar_alt_start'	);}
function wpct_sidebar_alt_end(){ 	do_action( 'wpct_sidebar_alt_end'	);}
function wpct_sidebar_alt_after(){ 	do_action( 'wpct_sidebar_alt_after'	);}

function wpct_product_slider_add_to_cart_before(){ do_action('wpct_product_slider_add_to_cart_before'); }
function wpct_product_slider_add_to_cart_after(){  do_action('wpct_product_slider_add_to_cart_after');  }
function wpct_product_slider_image_start(){ 	do_action('wpct_product_slider_image_start'); }
function wpct_product_slider_image_end(){ 	do_action('wpct_product_slider_image_end'); }
function wpct_product_slider_title_start(){ 	do_action('wpct_product_slider_title_start'); }
function wpct_product_slider_title_end(){ 	do_action('wpct_product_slider_title_end'); }
function wpct_product_slider_price_start(){ 	do_action('wpct_product_slider_price_start'); }
function wpct_product_slider_price_end(){ 	do_action('wpct_product_slider_price_end'); }

function wpct_product_image_start(){ do_action('wpct_product_image_start'); }
function wpct_product_image_end(){ 	do_action('wpct_product_image_end'); }
function wpct_product_title_start(){ do_action('wpct_product_title_start'); }
function wpct_product_title_end(){ 	do_action('wpct_product_title_end'); }
function wpct_product_price_start(){ do_action('wpct_product_price_start'); }
function wpct_product_price_end(){ 	do_action('wpct_product_price_end'); }

function wpct_product_cart_image_start(){	do_action('wpct_product_cart_image_start'); }
function wpct_product_cart_image_end(){ 		do_action('wpct_product_cart_image_end'); }

function wpct_product_single_image_before(){ do_action('wpct_product_single_image_before'); }
function wpct_product_single_image_end(){ 	do_action('wpct_product_single_image_end'); }
function wpct_product_single_title_before(){ do_action('wpct_product_single_title_before'); }
function wpct_product_single_title_end(){ 	do_action('wpct_product_single_title_end'); }
function wpct_product_single_price_before(){ do_action('wpct_product_single_price_before'); }
function wpct_product_single_price_end(){ 	do_action('wpct_product_single_price_end'); }

function wpct_shopdock_before(){ do_action('wpct_shopdock_before'); }
function wpct_shopdock_start(){ 	do_action('wpct_shopdock_start'); }
function wpct_checkout_start(){ 	do_action('wpct_checkout_start'); }
function wpct_checkout_end(){ 	do_action('wpct_checkout_end'); }
function wpct_shopdock_end(){ 	do_action('wpct_shopdock_end'); }
function wpct_shopdock_after(){ 	do_action('wpct_shopdock_after'); }

function wpct_sorting_before(){ 	do_action('wpct_sorting_before'); }
function wpct_sorting_after(){ 	do_action('wpct_sorting_after'); }
function wpct_related_products_start(){ 	do_action('wpct_related_products_start'); }
function wpct_related_products_end(){ 	do_action('wpct_related_products_end'); }

function wpct_breadcrumb_before(){ 	do_action('wpct_breadcrumb_before'); }
function wpct_breadcrumb_after(){ 	do_action('wpct_breadcrumb_after'); }

function wpct_ecommerce_sidebar_before(){ 	do_action('wpct_ecommerce_sidebar_before'); }
function wpct_ecommerce_sidebar_after(){ 	do_action('wpct_ecommerce_sidebar_after'); }
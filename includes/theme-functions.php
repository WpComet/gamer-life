<?php 
/* Common Theme functions
 * @author   WpComet
 * @version  1.0.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // File Security Check
}
 
/* Decide if it is a specific screen
 * @return boolean
 * @param  string  | $p | page to check
 * @param  boolean | $s | is strict
 * @since  1.0.0
 */
function wpct_is_it($p,$s=true) {
	global $pagenow, $wpcomet_theme;
	
	if($p == 'theme_screen') {
		// Check screen explicitly
		if( ! is_admin() ) return false;
		if( function_exists('get_current_screen')  ) {
			$scr = get_current_screen();
			if( isset($scr->id) || isset( $wpcomet_theme['screenhooks'] ) ) return false;
			return ( in_array( $scr->id,$wpcomet_theme['screenhooks'] ) ? true : false );
		}
		else if( ! $s && array_key_exists('page',$_GET) ) {
			// Check if we are on a singular theme page
			if( $_GET['page'] == $wpcomet_theme['theme_sanit)'] ) {
				return true;
			}
			// Check if we are on a menu or submenu page
			else if( strpos($_GET['page'],$wpcomet_theme['theme_slug'] ) !== false ) {
				return true;
			}
			else {
				return false;
			}
		}
		else if( ! $s && $pagenow == 'themes.php' && array_key_exists('action',$_GET) ) {	
			return true;
		}
		else { return false; }
	}
	elseif($p == 'admin') {
		return is_admin();
	}
	elseif($p == 'frontend') {
		return ( ! is_admin() || defined( 'DOING_AJAX' ) ) && ! defined( 'DOING_CRON' );
	}
	elseif($p == 'ajax') {
		return defined( 'DOING_AJAX' );
	}
	elseif($p == 'cron') {
		return defined( 'DOING_CRON' );
	}
	else {
		return false;	
	}
}

/* Get Theme property from $wpcomet_theme object.
 * @return mixed   | $val | prop of theme object, entire object, false
 * @param  boolean | $s   | strict or not
 * @since  1.0.0
 */
function wpct_theme_get($var='',$s=true) {
	global $wpcomet_theme;
	$fallback = ( $var ) ?  false : $wpcomet_theme;
	$val = ( $var && isset($wpcomet_theme->$var) ) ? $wpcomet_theme->$var : $fallback;
	return apply_filters( 'wpct_theme_get', $val );
}


/* Returns a value referenced by $var checking in various places such as theme settings, theme object or post meta data etc.
 * @return mixed
 * @param  string  | $var 
 * @param  null    | $value
 * order: theme mods > theme object > post meta > shop meta
 * WPCT-todo: Review if the checking order is optimal.
 */
 
function wpct_get($var, $value = NULL, $from = 'obj'  ) {
	global $wpcomet_theme, $post;
	
	$settings_defaults = $wpcomet_theme->settings_defaults;
	//	var_dump( $wpcomet_theme );
	//	var_dump( $settings_defaults );
	//	var_dump( $var );
	//	var_dump( isset( $settings_defaults[$var])  );
	if( isset( $settings_defaults[$var] ) ) {
		$default = isset( $settings_defaults[$var] ) ? $settings_defaults[$var] : false;
		//	var_dump($settings_defaults);
		//	var_dump($var);
		//var_dump( get_theme_mod( $var, $default ) );
		return get_theme_mod( $var, $default ) ;
		// apply_filters( 'wpct_get', $value );
	}
	elseif ( isset( $wpcomet_theme->$var ) && $wpcomet_theme->$var != '' ) {
		return $wpcomet_theme->$var;
	}
	elseif ( is_object( $post ) && get_post_meta( $post->ID, $var, true ) != '' ) {
		return get_post_meta( $post->ID, $var, true );
	}
	else if ( wpct_is_shop() ) {
		return get_post_meta( get_option( 'woocommerce_shop_page_id' ), $var, true );
	}
	//var_dump( $wpcomet_theme );
	//	return apply_filters( 'wpct_get', $value );
}

function wpct_isJSON($string){
	if( is_array( $string ) ) {
		 return $string;
	}
	else {
		$is_json = json_decode($string, true);
		return is_array($is_json) ? $is_json : $string;
	}
}

function wpct_get_mod($mod,$arg='') {
	global $wpcomet_theme;
	$settings_defaults = $wpcomet_theme->settings_defaults;

	if( isset( $settings_defaults[$mod] ) ) {
		$default = isset( $settings_defaults[$mod] ) ? $settings_defaults[$mod] : false;
		$value = get_theme_mod( $mod, $default );
		
		// convert to array if json string
		$value = wpct_isJSON($value);

		return apply_filters( 'wpct_get_mod', $value );	
	}
	else {
		return false;	
	}
}

/* WPCT-TODO: Check if this actually works by default
function themeslug_get_option_defaults() {
	$defaults = array(
		'option_1' => 'value_1',
		'option_2' => 'value_2',
		'option_3' => 'value_3'
	);
	return apply_filters( 'themeslug_option_defaults', $defaults );
}
*/

function wpct_get_mods($mod='',$arg='') {
	//	$mods = get_theme_mods();
	//	var_dump( $mods );
	$defaults = wpct_theme_get('settings_defaults');
	$theme_mods = array();
	//	return wp_parse_args( $mods, $defaults ); 
	foreach( $defaults as $k => $v ) {
		$theme_mods[$k] = wpct_get_mod($k);
	}
	
	return apply_filters( 'wpct_get_mods', $theme_mods );	
}
function wpct_fn1($str) {
	return	strtolower(substr(strrchr($file,"."),1));
}

function wpct_get_file_ext($filename) {
	return	pathinfo($filename, PATHINFO_EXTENSION);
}

/* Locate a template and return the path for inclusion.
 * @return string | filepath
 * @param string | $template_name | ex:'content-single'
 * @param string | $template_path | def:''
 * @param string | $default_path  | def:''
 * @since  1.0.0
 *
 * This is the load order:
 *		yourtheme		/	$template_path	/	$template_name
 *		yourtheme		/	$template_name
 *		$default_path	/	$template_name
 *
 */
function wpct_locate_template( $template_name, $load=false, $template_path = '', $default_path = '', $ext = '.php') {
	if ( ! $template_path ) {
		$template_path = wpct_theme_get("templates_folder");
	}

	if ( ! $default_path ) {
		$default_path = wpct_theme_get("basepath");
	}
	$ext_maybe = wpct_get_file_ext($template_name);
	$template_name = ( $ext_maybe) ? $template_name : $template_name . $ext;

	//	$template_name = $template_name . $ext;
	//var_dump( trailingslashit( $template_path ) . $template_name );
	// Look within passed path within the theme - this is priority.
	$template = locate_template(
		array(
			trailingslashit( $template_path ) . $template_name,
			$template_name
		), $load
	);

	$mode = wpct_theme_get("mode");
	//	var_dump( $min );
	
	// Ignore params and  get default template if not in production mode.
	if ( ! $template || $mode != "prod" ) {
		$template = $default_path . '/' . $template_name;
	}

	// Return what we found.
	return apply_filters( 'wpct_locate_template', $template, $template_name, $template_path );
}

/**
 * Get other templates (e.g. searchform) passing attributes and including the file.
 * @return file content
 * @param string | $template_name
 * @param array  | $args 		  | (default: array())
 * @param string | $template_path | (default: '')
 * @param string | $default_path  | (default: '')
 * @since  1.0.0 
 */
function wpct_get_template( $template_name, $args = array(), $located='', $template_path = '', $default_path = '' ) {
	if ( ! empty( $args ) && is_array( $args ) ) {
		extract( $args );
	}

	if( ! $located ) $located = wpct_locate_template( $template_name, $template_path, $default_path );
	//	var_dump( $located, $template_name );

	if ( ! file_exists( $located ) ) {
		// WPCT_Logger::log( ' file $located does not exist.' )
		return false;
	}

	// Allow 3rd party plugin filter template file from their plugin.
	$located = apply_filters( 'wpct_get_template', $located, $template_name, $args, $template_path, $default_path );

	do_action( 'wpct_before_template_part', $template_name, $template_path, $located, $args );
	

	include( $located );

	do_action( 'wpct_after_template_part', $template_name, $template_path, $located, $args );
}

/**
 * Mimick and extend WP get_sidebar function to allow loading sidebar from subfolder.
 * @return file content
 * @param string | $name  | sidebar name
 * @since  1.0.0  
 */
 
function wpct_get_sidebar( $name='',$args=array()) {
    /**
     * Fires before the sidebar template file is loaded.
     *
     * The hook allows a specific sidebar template file to be used in place of the
     * default sidebar template file. If your file is called sidebar-new.php,
     * you would specify the filename in the hook as get_sidebar( 'new' ).
     *
     */
    do_action( 'get_sidebar', $name );
 
	$defaults = array(
		'class' => ' sidebar',
		'tag' => 'div'
	);
	$args = wp_parse_args( $args, $defaults );
	if( $name ) $args['name'] = $name;
	
	wpct_get_template('sidebar.php', $args );
}

/**
 * Calls is_shop() in a safe manner
 *
 * @since 1.0.0
 * @return bool
 */
function wpct_is_shop() {
	$is_shop = class_exists( 'WooCommerce' )
		&& function_exists( 'is_shop' ) // required, it may be WC is active but the function is not available
		&& is_shop();
	return $is_shop;
}


/* Get Theme info from theme object.
 * @param | $i = info to get
 * $rel = parent or child
 */
function wpct_get_theme($i='',$rel='parent') {
 	$stylesheet = ( $rel === "parent" ) ?  get_template() : NULL;
	$theme = wp_get_theme($stylesheet);
	$theme->get( $i );
	// if( is_array($theme_data) && empty($theme_data)) return false;
	
	return ( empty($i) ) ? $theme : $theme->get( $i );
	/*if( empty($i) ) {
		return $theme;
	}
	else {
		
	}*/

}

/* Get info from theme header.
 * @param string | $h = header to get
 * @param string | $rel = parent or child
 */
function wpct_get_theme_header($h='',$rel='parent') {
 	$stylesheet = ( $rel === "parent" ) ?  get_template() : NULL;
	$theme_data = wp_get_theme($theme);
	//$themev = $theme_data->get( 'Version' );

	// if( is_array($theme_data) && empty($theme_data)) return false;

	if( empty($h) ) {
		return $theme_data;
	}
	else {
		// Check if requested header exists in theme headers.
		return ( !empty($h) && isset($theme_data[$h] ) ) ? $theme_data[$h] : false;
	}

}

function wpct_err_msg($m) {
	if(isset($_GET['action']) && $_GET['action'] == 'error_scrape') {
		echo $m;
		exit;
	} else {
		return trigger_error($m, E_USER_ERROR);
	}
}


/** 
 * Generate settings from files in settings folder
 * WPCT-TODO: complete the function;
 */
function wpct_read_customizer_settings() {
	//	$sett_dir = $plug_dir . 'includes/settings/';
	$sett_path = wpct_theme_get("customizer_settings_path");
	$customizer_settings_folder = wpct_theme_get("customizer_settings_folder");

	$setting_files = scandir($sett_path);
	$all_settings = array();
	$sections = array();
	foreach($setting_files as $setting_file) {
		if ( substr($setting_file, 0, 1) == '.' ) continue;
		//$sett_file_arr = include($sett_dir . $setting_file);
		//var_dump( $setting_file);
		//var_dump( $customizer_folder);
		$setting_section = wpct_locate_template( $setting_file, true, $customizer_settings_folder );
		
		add_filter( 'wpct_customizer_settings_read', $setting_section );
		var_dump( $setting_section );
		//$settings_key = strstr($setting_file,'_settings',true);
		//$section_key = str_replace($settings_key . '_settings_', '', strstr($setting_file,'_section',true) );
	}
	//	return $all_settings;
}

/**
 * Checks if a value referenced by $var exists in theme settings or post meta data.
 *
 * @param $var
 *
 * @return bool
 */
function wpct_check( $var ) {
	global $post, $wpcomet_theme;
	$theme_defaults = $wpcomet_theme->defaults;
	$data = array();
	if ( isset( $wpcomet_theme->$var ) && $wpcomet_theme->$var != '' ) {
		return true;
	}
	elseif ( isset( $theme_defaults[$var] ) && $theme_defaults[$var] != '' ) {
		return true;
	}
	elseif ( is_object( $post ) && get_post_meta( $post->ID, $var, true ) != '' && get_post_meta( $post->ID, $var, true ) ) {
		return true;
	} elseif( wpct_is_shop() && get_post_meta( get_option( 'woocommerce_shop_page_id' ), $var, true ) != '' && get_post_meta( get_option( 'woocommerce_shop_page_id' ), $var, true ) ) {
		return true;
	} else {
		return false;
	}
}

/**
 * Load/Check minified file is exist
 * OBSOLETE
 * @return string/bool
 * WPCT-todo remove this function
 */
function wpct_enque($url,$check=false){
    static $is_disabled = null;
    if($is_disabled===null){
        $is_disabled = defined('WP_DEBUG') && WP_DEBUG;
    }	
    if($is_disabled){
        return $check?false:$url;
    }
    $f = pathinfo($url);
    $return = 0;
    if(strpos($f['filename'],'.min.',2)===false){
            static $site_url = false;
            if($site_url===false){
                $site_url = get_site_url();
            }
            $absolute = trim(str_replace($site_url, '', $f['dirname']),'/');
//            $name = $f['filename'].'.min.'.$f['extension'];
            $name = $f['filename'].'.'.$f['extension'];
            if(is_file(ABSPATH.$absolute.'/'.$name)){
                if($check){
                    $return = 1;
                }
                else{
                    $url = trim($f['dirname'],'/').'/'.$name;
            }
        }
    }
    return $check?$return:$url;
    
}

function wpct_min() {
	$mode = wpct_theme_get("mode");
	$debug = defined('SCRIPT_DEBUG') && SCRIPT_DEBUG;
	return ( $mode == "prod" && ! $debug ) ? ".min" : '';	
}

function wpct_check_script() {
	if( $wpcomet_theme->mode == "final" ) {
		$mode = wpct_theme_get("mode");
		$debug = defined('SCRIPT_DEBUG') && SCRIPT_DEBUG;
		return ( $mode == "prod" && ! $debug ) ? ".min" : '';	
	}
}

// Load a pre-registered library 
function wpct_load_lib($lib,$load=array("css","js")) {
	global $wpcomet_theme;
	//var_dump( $wpcomet_theme );
	$compile = ( isset($_GET["wpct_compile"]) );
	//var_dump( $compile );
	$libs = wpct_libs_reg();
	$libtl = $libs[$lib];
	//	$folder = "vendor/" . $base;
	$lib_folder = ( isset ( $libtl["folder"] ) ) ? $libtl["folder"] : '';
	$ver = ( isset ( $libtl["v"] ) ) ? $libtl["v"] : false;
	$min = wpct_min();
	$assets_uri = $wpcomet_theme->assets_uri;
	$vendors = $wpcomet_theme->vendors_folder;
	
	if( isset($libtl['css'] ) && in_array("css", $load) ) {
		$med = ( isset ( $libtl["med"] ) ) ? $libtl["med"] : "all";
		$deps = ( isset ( $libtl["deps_css"] ) ) ? $libtl["deps_css"] : array();

		foreach ( $libtl['css'] as $file ) {
			// Try .min versions of the file if they exist, if not try regular
			$src = $assets_uri . '/' . $vendors . '/' . $lib_folder . '/' . $file . $min . ".css" ;
			$src_path = wpct_theme_get("assets_path") . '/' . $vendors . '/' . $lib_folder . '/' . $file . $min . ".css";
			
			// Skip file exists checks if you are sure everything is in place and it is in final mode
			if( $wpcomet_theme->mode == "final" ) {
				wpct_load_css($lib, $src, $deps, $ver, $med);
			}
			else {	
				if( file_exists( $src_path ) ) {
					 wpct_load_css($lib, $src, $deps, $ver, $med);
				}
				else {
					$src_path_maybe = $wpcomet_theme->assets_path . '/' . $vendors . '/' . $lib_folder . '/' . $file . ".css";
					if( file_exists( $src_path_maybe ) ) {
						$src = $assets_uri . '/' . $vendors . '/' . $lib_folder . '/' . $file . ".css" ;	
					}
				}
				wpct_load_css($lib, $src, $deps, $ver, $med);
			}
		}
	}
	
	if( isset($libtl['js'] ) && in_array("js", $load) ) {
		$foot = ( isset ( $libtl["nofoot"] ) ) ? false : true;
		$deps = ( isset ( $libtl["deps_js"] ) ) ? $libtl["deps_js"] : array();
		
		foreach ( $libtl['js'] as $file ) {
			$src = $assets_uri . '/' . $vendors . '/' . $lib_folder . '/' . $file . $min . ".js" ;
			wpct_load_js($lib, $src, $deps, $ver, $foot);
		}
	}
	
}

// Wrapper function to load a set of libraries
function wpct_load_libs( $libs=array() ) {
	foreach ( $libs as $lib ) {
		wpct_load_lib( $lib );
	}
}

function wpct_enqueue_js( $name, $src=NULL, $deps=array(),$foot=true, $ver=false ) {
	global $wpcomet_theme;
	$assets_uri = $wpcomet_theme->assets_uri;
	if( $ver ) $ver = $wpcomet_theme->version;
	$min = wpct_min();
	$check = ( $min == '' ) ? false : true;

	// if no src given, try the script name
	if( ! $src ) $src = $name;
	
	$pi = pathinfo($src);
	$ext = ( isset( $pi["extension"] ) ) ? $pi["extension"] : 'js';
	// if its not an uri , its prob. just filename with or without extension
	$src = ( $pi['dirname'] != "." ) ? $src : $assets_uri . '/' . $pi["filename"] . $min . "." . $ext ;
	
	wp_enqueue_script($name, $src, $deps, $ver, $foot);	
}

 /* Load a css file 
  */
function wpct_load_css($lib_name,$src='',$deps=array(),$ver=false,$med="all") {
	/*if( $lib_name == "bootstrap") {
	die(
	var_dump(
	$lib_name,
		$src,
		$deps,
		$ver,
		$med
		)
		);
	}*/
	wp_register_style( 
		$lib_name,
		$src,
		$deps,
		$ver,
		$med
	);
	wp_enqueue_style( $lib_name );	
}

/* Reigister & Enqueue a js file
 */
function wpct_load_js($lib_name,$src='',$deps=array(),$ver=false,$foot=true) {
	wp_register_script(
		$lib_name,
		$src,
		$deps,
		$ver,
		$foot
	);
	wp_enqueue_script($lib_name);
}

/**
 * Get breakpoints settings
 * if it's framework return customizer breakpoints,else if it's builder plugin builder breakpoints
 * @since 3.0.0
 * @return array
 */
function wpct_get_breakpoints( $select = 'all',$max_min=false ) {
	$breakpoints = array(
			'tablet_landscape' => array(769,1024),
			'tablet' => array(681,768),  
			'mobile' => 680
	);
	return apply_filters( 'wpct_get_breakpoints_filter', $breakpoints );
}

function wpct_vd($v,$p=false,$e='') {
	echo "<pre>";
	if( ! empty($e) )	echo $e;
	($p) ?	print_r($v) : var_dump($v);
	echo "</pre>";	
}

function wpct_percent($num_amount, $num_total,$fl=false,$e=false) {
	$count1 = $num_amount / $num_total;
	($fl) ? $count2 = floor($count1 * 100) : $count2 = $count1 * 100;
	$count = number_format($count2, 0);
	return $count;
}

/**
 * Returns a list of web safe fonts
 * @param bool $only_names Whether to return only the array keys or the values as well
 * @return mixed|void
 * @since 1.0.0
 */
function wpct_get_web_safe_font_list( $only_names = false ) {
	$web_safe_font_names = array(
		'Helvetica Neue, Helvetica, Arial, sans-serif',
		"Georgia, 'Times New Roman', Times, serif",
		"Verdana, Geneva, sans-serif",
		"'Times New Roman', Times, serif",
		"Tahoma, Geneva, sans-serif",
		"'Trebuchet MS', Arial, Helvetica, sans-serif",
		"Palatino, 'Palatino Linotype', 'Book Antiqua', serif",
		"'Lucida Grande, Lucida Sans Unicode, sans-serif'"
	);

	if( ! $only_names ) {
		$web_safe_fonts = array(
			array( 'value' => 'default', 'name' => '', 'selected' => true ),
			array( 'value' => '', 'name' => '--- '.__( 'Web Safe Fonts', 'gamer-life' ) . ' ---' )
		);

		foreach( $web_safe_font_names as $font ) {
			$web_safe_fonts[] = array(
				'value' => $font,
				'name' => $font
			);
		}
	} else {
		$web_safe_fonts = $web_safe_font_names;
	}

	return apply_filters( 'wpct_get_web_safe_font_list', $web_safe_fonts );
}

function wpct_get_google_fonts() {
	return array();
}


	/**
	 * Checks if the string ends with a certain substring.
	 * 
	 * @since 2.1.9
	 * 
	 * @param string $haystack Main string to search in.
	 * @param string $needle Substring that must be found at the end of main string.
	 * 
	 * @return bool Whether the substring is found at the end of the main string.
	 */
	function wpct_endsWith($haystack, $needle) {
		$needle_length = strlen($needle);
		$offset = strlen($haystack) - $needle_length;
		$length = $needle_length;
		return @substr_compare($haystack, $needle, $offset, $length) === 0;
	}

/*
 * Below are two functions those essentially do the same things for testing purposes. Usage: benchmark( testfunc1_strstr,testfunc1_strstr) );
 *	function testfunc1_strstr () {
 *		$str = 'string_with_a_needle';
 *		return str_replace('needle','',strstr( $str, 'needle',false) );
 *		
 *	}
 *	function testfunc2_strpos() {
 *		$str = 'string_with_a_needle';
 *		return substr($str, strpos($str, "needle") + 6);    
 *	}
 */
function wpct_benchmark($func1,$func2='',$no=100,$e=true,$f1args=null,$f2args=null,$vd=false) {
	$exists1 = (is_array($func1)) ? method_exists($func1[0],$func1[1] ) : function_exists($func1);
	$exists2 = (is_array($func2)) ? method_exists($func2[0],$func2[1]) : function_exists($func2);
	if( ! $exists1 ) return false;
	$result ='<pre>';
	// Run the function(s) given no# of time.
	$f1total = 0;
	$f2total = 0;
	$f1p = 0;
	$f2p = 0;
	
	$func1_str = ( is_array($func1) ) ? implode("->",$func1) : $func1;
	$func2_str = ( is_array($func1) ) ? implode("->",$func2) : $func2;

	$rr = rand(2, $no - 2);
	for ($i = 0; $i < $no; $i++) {
		// reset against caching
		$took1 =0; $took2 =0;
		//	clearstatcache();
		if( $exists1 ) {
			$t1 = microtime(true);
			$run1 = ( is_array($f1args) ) ? call_user_func_array($func1,$f1args) : call_user_func($func1,$f1args);
			if($vd) var_dump($run1);
			$took1 = number_format( (microtime(true) - $t1), 16);
			$f1total = $f1total + $took1;
		}

		if( $exists2 ) {
			$t2 = microtime(true);
			$run2 = ( is_array($f2args) ) ? call_user_func_array($func2,$f2args) : call_user_func($func2,$f2args);
			if($vd) var_dump($run2);
			$took2 = number_format( (microtime(true) - $t2), 16);
			$f2total = $f2total + $took2;
		}
		
		// Keep score
		if($exists2 ) ($took1 > $took2 ) ? $f2p++ : $f1p++;

		// Output first , a random and final run.
		if( $i === 0 || $i === $rr || $i === $no -1 ) {
			if($i === 0) $t = '<b>First run: </b>';
			if($i === $rr) $t = '<b>#' . $rr . 'th run: </b>';
			if($i === $no -1) $t = '<b>Last run: </b>';
			if( $exists1 ) $result .= $t . ' func1 -<b> '. $func1_str.'() </b>- took : ' . $took1 . ' s';
			$result .= ( $exists2 ) ? ' || func2 -<b> ' . $func2_str.'() </b>- took : ' . $took2. ' s' . PHP_EOL :  PHP_EOL;

		}
	}
	// totals
	$f1avr = number_format( ($f1total / $no), 10);
	$f2avr = number_format( ($f2total / $no), 10);
	$result .= '<b>In total</b> func#1 -<b>'.$func1_str.'()</b> - took '.$f1total;
	$result .= ( $exists2 ) ? ' , func#2 -<b>'.$func2_str.'()</b> - took '.$f2total . PHP_EOL : PHP_EOL;
	$result .= '<b>Averages:</b> func#1 -'.$func1_str.'- '.$f1avr;
	if( $exists2 ) $result .=' , func#2 -'.$func2_str.'- '.$f2avr . PHP_EOL;
	if( $exists2 ) $result .= 'func#1 won <b>'.$f1p.'</b> rounds, func#2 won <b>'.$f2p.'</b> rounds' . PHP_EOL;

	$result .= '</pre>';
	if($e) {
		 echo $result;
	}
	else { 
		return $result;
	}
}

function wpct_favicon() {
	$favicon = wpct_get_mod("favicon");
	$favicon_html = '<link href="' . wpct_get_mod("favicon").'" rel="icon" type="image/x-icon" />';
	return ( ! $favicon ) ? false : $favicon_html;
}


/*
function wpct_main_nav() {
	$args = apply_filters( 'wpct_main_nav_args', array(
		'theme_location'  => 'primary',
		'container_class' => 'collapse navbar-collapse',
		'container_id'    => 'navbarNavDropdown',
		'menu_class'      => 'navbar-nav',
		'fallback_cb'     => 'wpct_main_nav_fallback',
		'menu_id'         => 'main-menu',
		'walker'          => new WPCT_Bootstrap_Navwalker(),
	) );
	wp_nav_menu( $args );
}*/

// Mimick wp_page_menu since default doesnt support ul class for bootstrap
function wpct_page_menu( $args = array() ) {
	$defaults = array('sort_column' => 'menu_order, post_title', 'menu_class' => 'menu', 'echo' => true, 'link_before' => '', 'link_after' => '');
	$args = wp_parse_args( $args, $defaults );
	$args = apply_filters( 'wp_page_menu_args', $args );
	$menu = '';
	$list_args = $args;
	//	var_dump($args);
	// Show Home in the menu
	if ( ! empty($args['show_home']) ) {
		if ( true === $args['show_home'] || '1' === $args['show_home'] || 1 === $args['show_home'] )
			$text = __('Home','gamer-life');
		else
			$text = $args['show_home'];
		$class = 'nav-item';
		if ( is_front_page() && !is_paged() )
			$class = $class . ' current_page_item active';
		$menu .= '<li class="' . $class . '"><a class="nav-link" href="' . home_url( '/' ) . '">' . $args['link_before'] . $text . $args['link_after'] . '</a></li>';
		// If the front page is a page, add it to the exclude list
		if (get_option('show_on_front') == 'page') {
			if ( !empty( $list_args['exclude'] ) ) {
				$list_args['exclude'] .= ',';
			} else {
				$list_args['exclude'] = '';
			}
			$list_args['exclude'] .= get_option('page_on_front');
		}
	}
	$list_args['echo'] = false;
	$list_args['title_li'] = '';
	$list_args['walker'] = new WPCT_Walker_Page();
	
	$menu .= str_replace( array( "\r", "\n", "\t" ), '', wp_list_pages($list_args) );
	//var_dump($menu);
	if ( $menu )
		$menu = '<ul class="' . esc_attr($args['ul_class']) . '">' . $menu . '</ul>';
	$menu = '<div class="' . esc_attr($args['menu_class']) . '" id="'.$args['menu_id'].'">' . $menu . "</div>\n";
	$menu = apply_filters( 'wp_page_menu', $menu, $args );
	
	
	if ( $args['echo'] )
		echo $menu;
	else
		return $menu;
}

function wpct_main_nav_fallback() {
	
	$args = apply_filters( 'wpct_main_nav_fallback_args', array(
		'show_home' => true,
		'menu_id' => 'navbarSupportedContent',      // adding custom nav class
		'menu_class' => 'top-nav navfback collapse navbar-collapse',      // adding custom nav class
		'ul_class' => 'nav navbar-nav ',      // adding custom nav class
		'echo'        => true,
		'link_before' => '',                            // before each link
		'link_after' => ''                             // after each link
	) );
	wpct_page_menu( $args );
}

function wpct_dot_nav_fallback() {
	ob_start(); ?>
    <ul class="dropdown-menu" aria-labelledby="dropdownDotsMenuButton">
		<?php echo wpct_register() ?>
        <li><?php echo wpct_loginout( NULL, false ); ?>
        <li><a class="dropdown-item" href="<?php echo get_bloginfo( 'rss2_url' ) ?>"><i class="fa fa-rss" aria-hidden="true"></i> <abbr title="<?php _e( 'Really Simple Syndication', 'bootstrap-ultimate' ) ?>">RSS</abbr></a> </li>
    </ul>
    <?php
	echo ob_get_clean();
}

function wpct_register( $before = '<li>', $after = '</li>', $echo = true ) {
    if ( ! is_user_logged_in() ) {
        if ( get_option('users_can_register') )
            $link = $before . '<a class="dropdown-item" href="' . esc_url( wp_registration_url() ) . '">' . __('Register') . '</a>' . $after;
        else
            $link = '';
    } elseif ( current_user_can( 'read' ) ) {
        $link = $before . '<a class="dropdown-item" href="' . admin_url() . '">' . __('Site Admin') . '</a>' . $after;
    } else {
        $link = '';
    }

    return $link;
}

/**
 * Count number of widgets in a sidebar
 * Used to add classes to widget areas so widgets can be displayed one, two, three or four per row
 */
function wpct_count_widgets( $sidebar_id ) {
	// If loading from front page, consult $_wp_sidebars_widgets rather than options
	// to see if wp_convert_widget_settings() has made manipulations in memory.
	global $_wp_sidebars_widgets;
	if ( empty( $_wp_sidebars_widgets ) ) :
		$_wp_sidebars_widgets = get_option( 'sidebars_widgets', array() );
	endif;

	$sidebars_widgets_count = $_wp_sidebars_widgets;

	if ( isset( $sidebars_widgets_count[ $sidebar_id ] ) ) :
		$widget_count = count( $sidebars_widgets_count[ $sidebar_id ] );
		$widget_classes = 'widget-count-' . count( $sidebars_widgets_count[ $sidebar_id ] );
		if ( $widget_count % 4 == 0 || $widget_count > 6 ) :
			// Four widgets per row if there are exactly four or more than six
			$widget_classes .= ' col-md-3';
		elseif ( 6 == $widget_count ) :
			// If two widgets are published
			$widget_classes .= ' col-md-2';
		elseif ( $widget_count >= 3 ) :
			// Three widgets per row if there's three or more widgets 
			$widget_classes .= ' col-md-4';
		elseif ( 2 == $widget_count ) :
			// If two widgets are published
			$widget_classes .= ' col-md-6';
		elseif ( 1 == $widget_count ) :
			// If just on widget is active
			$widget_classes .= ' col-md-12';
		endif; 
		return $widget_classes;
	endif;
}
/*
function wpct_container() {
	$container = wpct_get( 'container_type' );
	return ( $container == "full" ) ? "container-fluid" : "container";
}*/

// Get layout or layout element 
function wpct_get_layout_el($el=NULL) {
	$view = wpct_theme_get("view");
	//var_dump( get_theme_mod("layout_home") );
	if( $view == '' ) {
		$layout_data =  wpct_get("layout_global");
		//var_dump( $layout );
	}
	else {
		$layout_maybe = wpct_get("layout_" . $view );
		//var_dump(  wpct_get("layout_global") );
		$layout_data = ( $layout_maybe == '' ) ?   wpct_get("layout_global")  :  $layout_maybe ;
		//$layout = json_decode( wpct_get("layout_" . $view ) );
		//var_dump( $layout_data );
	}

	$layout = ( is_array( $layout_data ) ) ?  $layout_data : json_decode($layout_data, true);
	return (isset($el) && isset( $layout[$el] ) ) ? $layout[$el] : $layout;
}

function wpct_container_cl( $cl='container' ) {
	$container = wpct_get_layout_el( 'container' ); 
	$container_cl = ( $container == 'boxed' ) ? $cl : $cl . '-fluid';  
	return $container_cl;
}

function wpct_class( $cl ) {
	$cl = ( is_array($cl) ) ? implode( ' ', $cl ) : $cl;
	return $cl;
}

function wpct_row_cl( $cl = array('row'), $el ) {
	
	$container = wpct_get_layout_el( 'container' );
	$row_cl = wpct_class( $cl );
	//die( wpct_vd($container) );
	//	esc_attr( implode( ' ', $class ) )
	//$row_cl = ( $container == 'boxed' ) ? $cl : $cl . ' no-gutters';  
	//if( $container == 'boxed' )
	return $row_cl;
}

function wpct_navbar_cl( $cl_str = 'navbar navbar-expand-lg' ) {
	$scheme = wpct_get( 'navbar_scheme' );
	$fade = wpct_get( 'navbar_fade' );
	$bg = wpct_get( 'navbar_bgcolor' );
	$toggle = wpct_get( 'navbar_toggle' );
	//die( wpct_vd( $scheme ) );
	$nav_cl = ( $scheme == "navbar-light" ) ? 'navbar-light bg-light' : 'navbar-dark bg-dark';
	//navbar-toggleable-md
	return $cl_str . ' ' . $nav_cl;
}

// Get logo or blog name to show in navbar - OBSOLETE
function wpct_brand() {
	$brand = wpct_get_mod("brand");
	//	var_dump( $brand );
	$logotext = isset( $brand["text"] ) ? $brand["text"] : get_bloginfo( 'name' );
	ob_start();
	if( $brand["type"] == "image" ) :
	//$logoimg = ( isset( $logo_args->image ) ) ? $logo_args->image : false;
	$logoimg = $brand["image"];	
	$is_image = ( ! $logoimg ) ? false : preg_match( '/(^.*\.jpg|jpeg|png|gif*)/i', $logoimg );
	?>
    <img src="<?php echo $logoimg ?>" alt="<?php echo $logotext ?>" class="img-fluid" />
    <?php else : 
	?>
    <span class="h2 site-title"><?php echo $logotext ?></span>
    <?php endif; 	
	return ob_get_clean();
}

function wpctcc_fe_edit($setting) {
	global $wp_version;
	if( $wp_version <= 4.0 || ! is_customize_preview() ) return false;
	ob_start(); ?>
	<i class="wpct-customizer-edit" data-control='{ "name":"<?php echo $setting ?>_ctrl" }'><?php esc_html_e( 'Edit Logo', 'gamer-life' ); ?></i>
    <?php echo ob_get_clean();
}

function wpct_edit_link($mod,$section='',$panel='',$control='') {
	global $wp_version;
	$query['autofocus[section]'] = 'wpcomet_theme_options';
	$customizer_uri =  add_query_arg( $query, admin_url( 'customize.php' ) );
	$welcome_uri = esc_url( admin_url( 'themes.php?page=wpct-welcome' ) );
	$name = wpct_readable_name($mod);
	ob_start(); ?>
	<a href="<?php echo $customizer_uri ?>"><?php echo sprintf( __( 'Edit %s', 'gamer-life' ), $name ); ?> </a>
	<?php echo ob_get_clean();
}

function wpct_get_post_types() {
	// get custom post types
	$post_types = get_post_types( array( '_builtin' => false, 'publicly_queryable' => true ) );
	$post_types_neu = array();
	foreach($post_types as $post_type) {
		$post_types_neu[$post_type] = ucfirst($post_type);
	}
	// add the default post type
	$post_types_neu["post"] = "Posts";
	//	$post_types_neu["page"] = "Pages";
	return $post_types_neu;
}

/**
 * Checks if Woocommerce plugin is active and returns the proper value
 * @return bool
 * @since 1.4.6
 */
function wpct_is_woocommerce_active() {
	static $is_active = null;
	if(is_null($is_active)){
		$plugin = 'woocommerce/woocommerce.php';
		$network_active = false;
		if ( is_multisite() ) {
			$plugins = get_site_option( 'active_sitewide_plugins' );
		if ( isset( $plugins[$plugin] ) )
			$network_active = true;
		}
		$active_plugins = apply_filters( 'active_plugins', get_option( 'active_plugins' ) );
		$is_active = in_array( $plugin, (array) $active_plugins ) || $network_active;
	}
	return $is_active;
}

/**
 * Get search form
 * @return html for searchform
 * @since 1.0.0
 */
function wpct_get_search_form($pos='', $args=array() ) {
	$located = wpct_locate_template('searchform.php');
	$defaults = array('label' => 'hidden');
	$args = wp_parse_args( $args, $defaults );
	if( $pos ) $args['position'] = $pos;
	if ( $located ) {
		// yep, load the page template
		wpct_get_template('searchform.php', $args, $located );
	} else {
		// nope, load the content
		get_search_form();
	}
	//$search_template_maybe = wpct_get_template( 'searchform.php' );
	//return ( $search_template_maybe ) ? $search_template_maybe : get_search_form();
}

function wpct_loginout($redirect = '', $echo = true) {
    if ( ! is_user_logged_in() )
        $link = '<a href="' . esc_url( wp_login_url($redirect) ) . '" class="dropdown-item"><i class="fa fa-lock"></i>' . __('Log in') . '</a>';
    else
        $link = '<a href="' . esc_url( wp_logout_url($redirect) ) . '" class="dropdown-item"><i class="fa fa-lock"></i>' . __('Log out') . '</a>';
 
	return $link;
}

function wpct_curruser($var='') {
	$current_user = wp_get_current_user();
	if( $var ) {
		if( $current_user->ID == 0 ) {
			if( in_array( $var, array("display_name","user_lastname","user_firstname") ) ) return __("Guest");
		}
		
		if( isset($current_user->$var) ) {
			return $current_user->$var;
		}
		else {
			return false;
		}
	}
	else {
		return $current_user;
	}
	//	return ( $var && isset($current_user->$var) ) ? $current_user->$var : $current_user;
}

// truncate string at word
function wpct_truncate($phrase, $max_words) {	
	$phrase_array = explode(' ', $phrase);
	if (count($phrase_array) > $max_words && $max_words > 0) 
		$phrase = implode(' ', array_slice($phrase_array, 0, $max_words)) . __('...', 'gamer-life');	
	return $phrase;
}

// get recent comments
function wpct_get_recent_comments( $args=array() ) {
	
	$defaults = array( 
		'date_query' => array(
		    'column' => 'comment_date_gmt',
            'before' => '1 month ago'
			/*array(
				'after'     => 'January 1st, 2014',
				'before'    => array(
					'year'  => 2014,
					'month' => 7,
					'day'   => 01,
				),
			),*/
		),
	);
	
	$args = wp_parse_args( $args, $defaults );
	
	$comments_query = new WP_Comment_Query;
	$comments = $comments_query->query($args);
	
	
	return $comments;
	/*$recent_comments = '';
	
	if ($comments) {
		
		$recent_comments .= '<ul>';
		
		foreach ($comments as $comment) {
			
			$id      = $comment->comment_ID;
			$author  = $comment->comment_author;
			$comment = $comment->comment_content;
			$date    = get_comment_date('l, F jS, Y', $id);
			$url     = get_comment_link($id);
			
			$recent_comments .= '<li><a href="'. $url .'" title="'. $date .'">'. $author .'</a>: ';
			$recent_comments .= wpct_truncate(wp_strip_all_tags($comment, true), 12);
			$recent_comments .= '</li>';
			
		}
		
		$recent_comments .= '</ul>';
		
	} else {
		
		$recent_comments = '<p>'. __('No recent comments.', 'shapespace') .'</p>';
		
	}
	
	return $recent_comments;*/
	
}

function wpct_get_default_widgets($sidebar_name='') {
	ob_start();
	if( ! $sidebar_name || $sidebar_name == "sidebar-primary") :
	?>
    <nav class="sidebar-nav open">
		<ul class="nav">
			<li class="nav-title">Search</li>
            <li class="px-3">
                <div class="widget widget-search">
                    <?php wpct_get_search_form("sidebar"); ?>
                </div>
            </li>
            <li class="nav-title">Archives</li>
            <li>
                <div class="widget widget-archives">
                    <ul>
                        <?php wp_get_archives( array( 'type' => 'monthly' ) ); ?>
                    </ul>
                </div>
            </li>
            <li class="nav-title">Meta</li>
            <li>
                <div class="widget widget-archives">
                    <ul>
                        <?php wp_register(); ?>
                        <li><?php wp_loginout(); ?></li>
                        <?php wp_meta(); ?>
                    </ul>
                </div>
            </li>

    <?php elseif( $sidebar_name == "sidebar-secondary") : ?>
    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#posts-aside" role="tab" aria-expanded="true"><i class="icon-list"></i></a>
        </li>
       
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#comments-aside" role="tab" aria-expanded="false"><i class="icon-speech"></i></a>
        </li>

    </ul>
    <div class="tab-content">
    
		<div id="posts-aside" class="tab-pane active" role="tabpanel" aria-expanded="true">    
			<?php $the_query = new WP_Query( 'posts_per_page=5' ); ?>      
            <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
                <div class="callout callout-warning m-0 py-3">
                    <div class="thumbnail float-right align-items-center">
                    
                        <img src="<?php echo wpct_theme_get("assets_uri") .'/img/placeholder.png' ?>" class="img-placeholder" alt="Placeholder Image">
                    </div>
                    <h5><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h5>
                    <small class="text-muted mr-3"><i class="icon-calendar"></i>&nbsp;<?php echo get_the_date( 'Y-m-d' ); ?></small>
                    <?php
                    $categories = get_the_category();
					$separator = ' ';
					$output = '';
					if ( ! empty( $categories ) ) :
						foreach( $categories as $category ) : ?>
							<a href="<?php echo esc_url( get_category_link( $category->term_id ) ) ?>" alt="<?php esc_attr( sprintf( __( 'View all posts in %s', 'gamer-life' ), $category->name ) ); ?>">
								<small><i class="icon-folder-alt"></i>&nbsp;<?php echo esc_html( $category->name ); ?></a><?php echo $separator; ?></small>
							</a>
						<?php endforeach; ?>
					<?php endif; ?>
                </div>
                <hr class="mx-3 my-0">
			<?php 
            endwhile;
            wp_reset_postdata();
            ?>  
		</div>
        
        <div id="comments-aside" class="tab-pane p-3" role="tabpanel" aria-expanded="true">    
	        <?php $recent_comments = wpct_get_recent_comments(); 
            foreach( $recent_comments as $recent_comment ) : 
			//	var_dump( $recent_comment ); ?>
            <div class="comment">
                <div class="py-1 pb-1 mr-1 pull-left">
                    <div class="avatar">
                    	<?php echo get_avatar( $recent_comment->user_id, 24, '', $recent_comment->comment_author_email, array("class" => "img-avatar") ); ?>
                        <span class="avatar-status badge-success"></span>
                    </div>
                </div>
                <div>
                    <small class="text-muted"><?php echo get_the_title( $recent_comment->comment_post_ID ); ?></small>
                    <small class="text-muted float-right mt-1"><?php echo date('Y-m-d', strtotime($recent_comment->comment_date) ); ?></small>
                </div>
                <div class="text-truncate font-weight-bold"><?php echo $recent_comment->comment_author ?></div>
                <small class="text-muted"><?php echo $recent_comment->comment_content ?></small>
            </div>
            <?php endforeach; ?>
		</div>
    </div>
    <?php endif; ?>
    <?php return ob_get_clean();
}

function wpct_class_attr($element,$add_class='') {
	global $wpcomet_theme;

	if ( $wpcomet_theme->sub_theme == "coreui") {
		$class = " app-body";
	}
	else {
		$class = " row";
	}

	return apply_filters("wpct_class_attr", $class);
}

/*
	returns the human friendly name

	ucwords      capitalize words
	strtolower   makes string lowercase before capitalizing
	str_replace  replace all instances of hyphens and underscores to spaces

	@param   string  $name      the name you want to make friendly
	@return  string             the human friendly name
*/

function wpct_readable_name($name) {
	// return human friendly name
	return ucwords(strtolower(str_replace("-", " ", str_replace("_", " ", $name))));

}

function wpct_debugdump() {
	global $wpcomet_theme;
			echo "<pre>";
			var_dump( wpct_get_mod( 'navbar_scheme' ) );
			var_dump( wpct_get_mods() );
			
			var_dump( get_theme_mods() );
			var_dump( get_theme_mod("layout_global") );
			var_dump( $wpcomet_theme );
			//var_dump( WPCT_Settings::get_settings() );
			
			//var_dump( get_theme_mods() );
			//var_dump( wpct_get( 'settings_defaults' ) );
			//$container   = get_theme_mod( 'understrap_container_type' );
			echo "</pre>";
} 
?>
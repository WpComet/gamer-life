<?php
/**
 * Main Theme class
 *
 * @author WpComet
 * @since 1.0.0
 */

class WPCT_Theme {	 
	public $version;
	public $name;
	public $slug;
	public $prefix;

	public $basepath;
	public $includes_path;
	public $assets_path;
	public $classes_path;
	public $template_path;
	public $customizer_path;
	public $customizer_settings_path;
	public $sub_themes_path;
	
	public $baseuri;
	public $includes_uri;
	public $assets_uri;
	public $customizer_uri;
	
	public $assets_folder;
	public $vendors_folder;
	public $includes_folder;
	public $templates_folder;
	public $settings_folder;
	public $customizer_folder;
	public $customizer_settings_folder;
	public $sub_themes_folder;
	// probable values dev | prod | final
	public $mode = "prod";
	public $script_min;
	//	public $settings;
	//	public $settings_ids;
	public $view;
	public $hooks = NULL;
	//	public $settings = array();
	public $sub_theme = "coreui";
	public $settings_defaults = array();

	// The things this theme registers such as CPT, taxonomy, setting, transient etc.
	public $registers = array();
/*
	public $layout;

	public $post_layout;
	public $post_layout_type = 'default';
	public $post_filter;
	public $hide_title;
	public $hide_meta;
	public $hide_meta_author;
	public $hide_meta_category;
	public $hide_meta_comment;
	public $hide_meta_tag;
	public $hide_date;
	public $inline_date;
	public $hide_image;
	public $media_position;

	public $unlink_title;
	public $unlink_image;

	public $display_content = '';
	public $auto_featured_image;

	public $post_image_width = '';
	public $post_image_height = '';

	public $width = '';
	public $height = '';

	public $avatar_size = 96;
	public $page_navigation;
	public $posts_per_page;

	public $image_align = '';
	public $image_setting = '';

	public $page_id = '';
	public $page_image_width = 978;
	public $query_category = '';
	public $query_post_type = '';
	public $query_taxonomy = '';
	public $paged = '';

	public $google_fonts;*/

	/////////////////////////////////////////////
	// Set Default Image Sizes 					
	/////////////////////////////////////////////

	// Default Index Layout
	static $content_width = 978;
	static $sidebar1_content_width = 714;

	// Default Single Post Layout
	static $single_content_width = 978;
	static $single_sidebar1_content_width = 670;

	// Default Single Image Size
	static $single_image_width = 1024;
	static $single_image_height = 585;

	// List Post
	static $list_post_width = 1160;
	static $list_post_height = 665;

	// Grid4
	static $grid4_width = 260;
	static $grid4_height = 150;

	// Grid3
	static $grid3_width = 360;
	static $grid3_height = 205;

	// Grid2
	static $grid2_width = 561;
	static $grid2_height = 321;

	// List Large
	static $list_large_image_width = 800;
	static $list_large_image_height = 460;

	// List Thumb
	static $list_thumb_image_width = 260;
	static $list_thumb_image_height = 150;

	// List Grid2 Thumb
	static $grid2_thumb_width = 160;
	static $grid2_thumb_height = 95;

	// Use dimensions defined in custom post type panel
	public $use_original_dimensions = 'no';

	// Sorting Parameters
	public $order = 'DESC';
	public $orderby = 'date';

	// Check whether object in shortcode loop
	public $is_shortcode = false;

	function __construct($wpct_theme) {
		
		$this->name = $name = $wpct_theme["name"];
		$this->registers = $wpct_theme["registers"];
		$this->prefix = $wpct_theme["prefix"];
		$this->version = wpct_get_theme("Version");
		$this->slug = sanitize_title($name);
		$this->basepath = get_template_directory();
		$this->baseuri = get_template_directory_uri();
		
		$this->vendors_folder = 'vendor';
		$this->includes_folder = apply_filters( 'wpct_includes_folder', 'includes' );
		$this->assets_folder = apply_filters( 'wpct_assets_folder', 'assets' );
		$this->templates_folder = apply_filters( 'wpct_templates_folder', 'templates' );
		$this->settings_folder = apply_filters( 'wpct_settings_folder', $this->includes_folder . '/settings' );
		$this->customizer_folder = $this->includes_folder . '/customizer';
		$this->customizer_settings_folder = $this->customizer_folder . '/settings';
		$this->sub_themes_folder = apply_filters( 'wpct_sub_themes_folder', 'sub-themes' );


		$this->includes_path = $this->basepath . '/' . $this->includes_folder;
		$this->assets_path = $this->basepath . '/' . $this->assets_folder;
		$this->classes_path = $this->includes_path . '/classes';
		$this->template_path = $this->basepath . '/templates';
		$this->customizer_path = $this->includes_path . '/customizer';
		$this->customizer_settings_path =  $this->customizer_path . '/settings';
		$this->sub_themes_path =  $this->includes_path . '/' . $this->sub_themes_folder;
		
		$this->assets_uri = $this->baseuri . '/' . $this->assets_folder;
		$this->includes_uri = $this->baseuri . '/' . $this->includes_folder;
		$this->customizer_uri = $this->includes_uri .'/' . 'customizer';
		
		$this->includes();
	//	$this->includes_path = $this->basedir .= $this->includes_dir ;
	
		//	var_dump( wpct_get_theme() );
/*
		///////////////////////////////////////////
		//Global options setup
		///////////////////////////////////////////
		$this->layout = wpct_get( 'setting-default_layout', 'sidebar1' );
		$this->post_layout = wpct_get( 'setting-default_post_layout', 'list-post' );
		$this->page_title = wpct_get( 'setting-hide_page_title' );
		$this->hide_title = wpct_get( 'setting-default_post_title' );
		$this->unlink_title = wpct_get( 'setting-default_unlink_post_title' );
		$this->media_position = wpct_get( 'setting-default_media_position', 'above' );
		$this->hide_image = wpct_get( 'setting-default_post_image' );
		$this->unlink_image = wpct_get( 'setting-default_unlink_post_image' );
		//	$this->auto_featured_image = ! wpct_check( 'setting-auto_featured_image' ) ? 'field_name=post_image, image, wp_thumb&' : '';
		$this->hide_page_image = wpct_get( 'setting-hide_page_image' ) == 'yes' ? 'yes' : 'no';
		$this->image_page_single_width = wpct_get( 'setting-page_featured_image_width', $this->page_image_width );
		$this->image_page_single_height = wpct_get( 'setting-page_featured_image_height', 0 );

		$this->hide_meta = wpct_get( 'setting-default_post_meta' );
		$this->hide_meta_author = wpct_get( 'setting-default_post_meta_author' );
		$this->hide_meta_category = wpct_get( 'setting-default_post_meta_category' );
		$this->hide_meta_comment = wpct_get( 'setting-default_post_meta_comment' );
		$this->hide_meta_tag = wpct_get( 'setting-default_post_meta_tag' );

		$this->hide_date = wpct_get( 'setting-default_post_date' );
		$this->inline_date = $this->hide_date == 'yes' ? false : wpct_get( 'setting-default_display_date_inline' );

		// Set Order & Order By parameters for post sorting
		$this->order = wpct_get( 'setting-index_order', 'DESC' );
		$this->orderby = wpct_get( 'setting-index_orderby', 'date' );

		$this->display_content = wpct_get( 'setting-default_layout_display' );
		$this->avatar_size = apply_filters( 'themify_author_box_avatar_size', $this->avatar_size ); */

		$this->posts_per_page = get_option( 'posts_per_page' );

		add_action( 'template_redirect', array( $this, 'template_redirect' ) );
	}
	function includes() {
		if( $this->sub_theme ) require $this->sub_themes_path . '/theme-' . $this->sub_theme . '.php';
	}
	function template_redirect() {
		global $wpcomet_theme;
			
		if ( is_front_page() && is_home() ) {
			return $wpcomet_theme->view = "home";
		} elseif ( is_front_page() ) {
			return $wpcomet_theme->view = "home-static";
		} elseif ( is_home() ) {
			return $wpcomet_theme->view = "home-blog";
		}
		elseif ( is_page() ) {
			return $wpcomet_theme->view = "page";
		}
		elseif ( is_single() ) {
			return $wpcomet_theme->view = "single";
		}
		elseif ( is_tax() ) {
			return $wpcomet_theme->view = "tax";
		}
		elseif ( is_search() ) {
			return $wpcomet_theme->view = "search";
		}
		else {
			return $wpcomet_theme->view = "";
		}

		return true;
		
		$post_image_width = $post_image_height = '';

		if ( is_page() ) {
			if ( post_password_required() ) {
				return;
			}

			$this->page_id = get_the_ID();
			$this->post_layout = wpct_get( 'layout', 'list-post' );
			// set default post layout
			if ( $this->post_layout == '' ) {
				$this->post_layout = 'list-post';
			}

			$post_image_width = wpct_get( 'image_width' );
			$post_image_height = wpct_get( 'image_height' );

			if( wpct_get( 'portfolio_post_filter' ) ) {
				$this->post_filter = wpct_get( 'portfolio_post_filter' );
			}
		}

		if ( ! is_numeric( $post_image_width ) ) {
			$post_image_width = wpct_get( 'setting-image_post_width' );
		}

		if ( ! is_numeric( $post_image_height ) ) {
			$post_image_height = wpct_get( 'setting-image_post_height' );
		}

		if ( is_singular() ) {
			$this->display_content = 'content';
		}
		
		if ( ! is_numeric( $post_image_width ) || ! is_numeric( $post_image_height ) ) {
			///////////////////////////////////////////
			// Setting image width, height
			///////////////////////////////////////////
			switch ($this->post_layout){
				case 'grid4':
					$this->width = self::$grid4_width;
					$this->height = self::$grid4_height;
				break;
				case 'grid3':
					$this->width = self::$grid3_width;
					$this->height = self::$grid3_height;
				break;
				case 'grid2':
					$this->width = self::$grid2_width;
					$this->height = self::$grid2_height;
				break;
				case 'list-large-image':
					$this->width = self::$list_large_image_width;
					$this->height = self::$list_large_image_height;
				break;
				case 'list-thumb-image':
					$this->width = self::$list_thumb_image_width;
					$this->height = self::$list_thumb_image_height;
				break;
				case 'grid2-thumb':
					$this->width = self::$grid2_thumb_width;
					$this->height = self::$grid2_thumb_height;
				break;
				default :
					$this->width = self::$list_post_width;
					$this->height = self::$list_post_height;
				break;
			}
		}

		if ( is_numeric( $post_image_width ) ) {
			$this->width = $post_image_width;
		}
		
		if ( is_numeric( $post_image_height ) ) {
			$this->height = $post_image_height;
		}

		if( is_home() || is_category() || is_tag() ) {
			$this->query_taxonomy = 'category';
			$this->post_filter = wpct_get( 'setting-post_filter', 'yes' );
			$this->post_layout_type = wpct_get( 'setting-post_content_layout' );
		}

		if ( is_page() || wpct_is_shop() ) {
			// Set Page Number for Pagination
			if ( get_query_var( 'paged' ) ) {
				$this->paged = get_query_var( 'paged' );
			} elseif ( get_query_var( 'page' ) ) {
				$this->paged = get_query_var( 'page' );
			} else {
				$this->paged = 1;
			}
			global $paged;
			$paged = $this->paged;

			// Set Sidebar Layout
			if ( wpct_get( 'page_layout' ) != 'default' && wpct_check( 'page_layout' ) ) {
				$this->layout = wpct_get( 'page_layout' );
			} elseif ( wpct_check( 'setting-default_page_layout' ) ) {
				$this->layout = wpct_get( 'setting-default_page_layout' );
			} else {
				$this->layout = 'sidebar1';
			}
			// Set Page Title
			if ( wpct_get( 'hide_page_title' ) != 'default' && wpct_check( 'hide_page_title' ) ) {
				$this->page_title = wpct_get( 'hide_page_title' );
			} elseif ( wpct_check( 'setting-hide_page_title' ) ) {
				$this->page_title = wpct_get( 'setting-hide_page_title' );
			} else {
				$this->page_title = 'no';
			}

			// Post Meta Values ///////////////////////
			$post_meta_keys = array(
				'_author'   => 'post_meta_author',
				'_category' => 'post_meta_category',
				'_comment'  => 'post_meta_comment',
				'_tag'      => 'post_meta_tag'
			);
			$post_meta_key = 'setting-default_';
			$this->hide_meta = wpct_check( 'hide_meta_all' ) ? wpct_get( 'hide_meta_all' ) : wpct_get( $post_meta_key . 'post_meta' );
			foreach ( $post_meta_keys as $k => $v ) {
				$this->{'hide_meta' . $k} = wpct_check( 'hide_meta' . $k ) ? wpct_get( 'hide_meta' . $k ) : wpct_get( $post_meta_key . $v );
			}

			// Post query query ///////////////////
			$post_query_category = wpct_get( 'query_category' );
			$portfolio_query_category = wpct_get('portfolio_query_category');

			if ( '' != $portfolio_query_category ) {

				// GENERAL QUERY POST TYPES
				if ( '' != $portfolio_query_category ) {
					$this->query_category = $portfolio_query_category;
					$this->query_post_type = 'portfolio';
				}
				$this->query_taxonomy = $this->query_post_type . '-category';

				$this->post_layout = wpct_get( $this->query_post_type . '_layout' ) ? wpct_get( $this->query_post_type . '_layout' ) : 'list-post';

				if('default' != wpct_get('portfolio_hide_meta_all')){
					$this->hide_meta = wpct_get('portfolio_hide_meta_all');
				} else {
					$this->hide_meta = wpct_check('setting-default_portfolio_index_post_meta_category')?
					wpct_get('setting-default_portfolio_index_post_meta_category') : 'no';
				}

				$this->hide_title = 'default' == wpct_get('portfolio_hide_title') ? wpct_check( 'setting-default_portfolio_index_title' ) ? wpct_get( 'setting-default_portfolio_index_title' ) : 'no' : wpct_get( 'portfolio_hide_title' );

				$this->unlink_title = 'default' == wpct_get('portfolio_unlink_title') ? wpct_check( 'setting-default_portfolio_index_unlink_post_title' ) ? wpct_get( 'setting-default_portfolio_index_unlink_post_title' ) : 'no' : wpct_get('portfolio_unlink_title');

				$this->unlink_image = 'default' == wpct_get('portfolio_unlink_image') ? wpct_check( 'setting-default_portfolio_index_unlink_post_image' ) ? wpct_get( 'setting-default_portfolio_index_unlink_post_image' ) : 'no' : wpct_get('portfolio_unlink_image');

				$this->hide_image = 'default' == wpct_get( 'portfolio_hide_image' ) ? wpct_check( 'setting-default_portfolio_index_post_image' ) ? wpct_get( 'setting-default_portfolio_index_post_image' ) : 'no' : wpct_get( 'portfolio_hide_image' );

				$this->hide_image = 'default' == wpct_get( 'portfolio_hide_image' ) ? wpct_check( 'setting-default_portfolio_index_post_image' ) ? wpct_get( 'setting-default_portfolio_index_post_image' ) : 'no' : wpct_get( 'portfolio_hide_image' );

				$this->page_navigation = 'default' != wpct_get( $this->query_post_type . '_hide_navigation' ) ? wpct_get( $this->query_post_type . '_hide_navigation' ) : 'no';

				$this->display_content = wpct_get( $this->query_post_type . '_display_content', 'excerpt' );
				$this->posts_per_page = wpct_get( $this->query_post_type . '_posts_per_page' );
				$this->order = wpct_get( $this->query_post_type . '_order', 'desc' );
				$this->orderby = wpct_get( $this->query_post_type . '_orderby' );
				$this->use_original_dimensions = 'no';

				if ( '' != $portfolio_query_category ) {
					if('' != wpct_get('portfolio_image_width')){
						$this->width = wpct_get('portfolio_image_width');
					} else {
						if ( wpct_check('setting-default_portfolio_index_image_post_width') ) {
							$this->width = wpct_get('setting-default_portfolio_index_image_post_width');
						}
					}
					if('' != wpct_get('portfolio_image_height')){
						$this->height = wpct_get('portfolio_image_height');
					} else {
						if ( wpct_check('setting-default_portfolio_index_image_post_height') ) {
							$this->height = wpct_get('setting-default_portfolio_index_image_post_height');
						}
					}
				} else {
					if ( '' != wpct_get( $this->query_post_type . '_image_width' ) ) {
						$this->width = wpct_get( $this->query_post_type . '_image_width' );
					}
					if ( '' != wpct_get( $this->query_post_type . '_image_height' ) ) {
						$this->height = wpct_get( $this->query_post_type . '_image_height' );
					}
				}

			} else {

				// GENERAL QUERY POSTS
				$this->query_category = $post_query_category;
				$this->query_taxonomy = 'category';
				$this->query_post_type = 'post';

				$this->hide_title = wpct_get( 'hide_title' );
				$this->unlink_title = wpct_get( 'unlink_title' );
				$this->hide_image = wpct_get( 'hide_image' );
				$this->unlink_image = wpct_get( 'unlink_image' );
				if ( 'default' != wpct_get( 'hide_date' ) ) {
					$this->hide_date = wpct_get( 'hide_date' );
				} else {
					$this->hide_date = wpct_check( 'setting-default_post_date' ) ?
						wpct_get( 'setting-default_post_date' ) : 'no';
				}
				$this->display_content = wpct_check( 'display_content' ) ? wpct_get( 'display_content' ) : 'excerpt';
				$this->post_image_width = wpct_get( 'image_width' );
				$this->post_image_height = wpct_get( 'image_height' );
				$this->page_navigation = wpct_get( 'hide_navigation' );
				$this->posts_per_page = wpct_get( 'posts_per_page' );

				$this->order = wpct_get( 'order', 'desc' );
				$this->orderby = wpct_get( 'orderby', 'date' );

			}
			

			$this->post_layout_type = wpct_get( $this->query_post_type . '_content_layout', 'default' ) === 'default'
				? wpct_get( 'setting-' . $this->query_post_type . '_content_layout' )
				: wpct_get( $this->query_post_type . '_content_layout' );
		}
		elseif ( is_post_type_archive( 'portfolio' ) || is_tax('portfolio-category') ) {
			$this->layout = wpct_get( 'setting-default_portfolio_index_layout', 'sidebar-none' );
			$this->post_layout = wpct_get( 'setting-default_portfolio_index_post_layout', 'grid3' );
			$this->post_layout_type = wpct_get( 'setting-portfolio_content_layout' );
			$this->post_filter = wpct_get( 'setting-portfolio_post_filter', 'yes' );
			$this->query_taxonomy = 'portfolio-category';
			$this->query_post_type = 'portfolio';

			$p_layout = str_replace( '-', '_', $this->post_layout );
			$this->width = ! empty( self::${$p_layout . '_width'} ) 
				? self::${$p_layout . '_width'} : self::$list_post_width;
			$this->height = ! empty( self::${$p_layout . '_height'} ) 
				? self::${$p_layout . '_height'} : self::$list_post_height;

			$this->display_content = wpct_get( 'setting-default_portfolio_index_display', 'none' );
			$this->hide_title = wpct_get( 'setting-default_portfolio_index_title', 'no' );
			$this->unlink_title = wpct_get( 'setting-default_portfolio_index_unlink_post_title', 'no' );
			$this->hide_meta = wpct_get( 'setting-default_portfolio_index_post_meta_category', 'yes' );
			$this->hide_date = wpct_get( 'setting-default_portfolio_index_post_date', 'yes' );
			$this->unlink_image = wpct_get( 'setting-default_portfolio_index_unlink_post_image', 'no' );

			if ( wpct_check( 'setting-default_portfolio_index_image_post_width' ) ) {
				$this->width = wpct_get( 'setting-default_portfolio_index_image_post_width' );
			}

			if ( wpct_check( 'setting-default_portfolio_index_image_post_height' ) ) {
				$this->height = wpct_get( 'setting-default_portfolio_index_image_post_height' );
			}
		}
		elseif ( is_single() ) {
			$is_portfolio = is_singular('portfolio');
			$this->post_layout_type = wpct_get('post_layout');
			if (!$this->post_layout_type || $this->post_layout_type === 'default') {
				$this->post_layout_type = $is_portfolio ? wpct_get('setting-default_portfolio_single_portfolio_layout_type') : wpct_get('setting-default_page_post_layout_type');
			}
			$this->hide_title = ( wpct_get( 'hide_post_title' ) != 'default' && wpct_check( 'hide_post_title' ) ) ? wpct_get( 'hide_post_title' ) : wpct_get( 'setting-default_page_post_title' );
			$this->unlink_title = ( wpct_get( 'unlink_post_title' ) != 'default' && wpct_check( 'unlink_post_title' ) ) ? wpct_get( 'unlink_post_title' ) : wpct_get( 'setting-default_page_unlink_post_title' );
			$this->hide_date = ( wpct_get( 'hide_post_date' ) != 'default' && wpct_check( 'hide_post_date' ) ) ? wpct_get( 'hide_post_date' ) : wpct_get( 'setting-default_page_post_date' );
			if($this->hide_date!='yes'){
				$this->inline_date = wpct_get( 'setting-default_page_display_date_inline' );
			}
			$this->hide_image = ( wpct_get( 'hide_post_image' ) != 'default' && wpct_check( 'hide_post_image' ) ) ? wpct_get( 'hide_post_image' ) : wpct_get( 'setting-default_page_post_image' );
			$this->unlink_image = ( wpct_get( 'unlink_post_image' ) != 'default' && wpct_check( 'unlink_post_image' ) ) ? wpct_get( 'unlink_post_image' ) : wpct_get( 'setting-default_page_unlink_post_image' );
			$this->media_position = 'above';

			// Post Meta Values ///////////////////////
			$post_meta_keys = array(
				'_author'   => 'post_meta_author',
				'_category' => 'post_meta_category',
				'_comment'  => 'post_meta_comment',
				'_tag'      => 'post_meta_tag'
			);

			$post_meta_key = 'setting-default_page_';
			$this->hide_meta = wpct_check( 'hide_meta_all' ) ? wpct_get( 'hide_meta_all' ) : wpct_get( $post_meta_key . 'post_meta' );
			foreach ( $post_meta_keys as $k => $v ) {
				$this->{'hide_meta' . $k} = wpct_check( 'hide_meta' . $k ) ? wpct_get( 'hide_meta' . $k ) : wpct_get( $post_meta_key . $v );
			}
			if($this->post_layout_type !== 'split'){
				$sidebar_mode = array('sidebar-none', 'sidebar1','sidebar1 sidebar-left', 'sidebar2', 'sidebar2 content-left', 'sidebar2 content-right');
				$this->layout = in_array( wpct_get( 'layout' ), $sidebar_mode )  ? wpct_get( 'layout' ) : wpct_get( 'setting-default_page_post_layout' );
				// set default layout
				if ( $this->layout == '' ) {
					$this->layout = 'sidebar1';
				}
			}

			$this->display_content = '';

			if ( $is_portfolio ) {
				if ( wpct_check( 'hide_post_meta' ) && 'default' != wpct_get( 'hide_post_meta' ) ) {
					$this->hide_meta = wpct_get( 'hide_post_meta' );
				} else {
					$this->hide_meta = wpct_check( 'setting-default_portfolio_single_post_meta_category' ) ? wpct_get( 'setting-default_portfolio_single_post_meta_category' ) : 'no';
				}
				if($this->post_layout_type !== 'split'){
					if ( wpct_get('layout') != 'default' && wpct_get('layout') != '' ) {
						$this->layout = wpct_get('layout');
					} elseif( wpct_check('setting-default_portfolio_single_layout') ) {
						$this->layout = wpct_get('setting-default_portfolio_single_layout');
					} else {
						$this->layout = 'sidebar-none';
					}
				}

				$this->hide_title = (wpct_get('hide_post_title') != 'default' && wpct_check('hide_post_title')) ? wpct_get('hide_post_title') : wpct_get('setting-default_portfolio_single_title');
				$this->unlink_title = (wpct_get('unlink_post_title') != 'default' && wpct_check('unlink_post_title')) ? wpct_get('unlink_post_title') : wpct_get('setting-default_portfolio_single_unlink_post_title');
				$this->unlink_image = (wpct_get('unlink_post_image') != 'default' && wpct_check('unlink_post_image')) ? wpct_get('unlink_post_image') : wpct_get('setting-default_portfolio_single_unlink_post_image');
                                $post_image_width = wpct_get('setting-default_portfolio_single_image_post_width');
				$post_image_height = wpct_get('setting-default_portfolio_single_image_post_height');
			}
			else{
				$post_image_width = wpct_get('setting-image_post_single_width');
				$post_image_height = wpct_get('setting-image_post_single_height');
			}
			if ($this->post_layout_type === 'split') {
				$this->layout = 'sidebar-none';
			}

			// Set Default Image Sizes for Single
			self::$content_width = self::$single_content_width;
			self::$sidebar1_content_width = self::$single_sidebar1_content_width;

			// Set Default Image Sizes for Single
			$this->width =is_numeric($post_image_width)?$post_image_width:($is_portfolio?self::$single_image_width:self::$single_image_width);
			$this->height = is_numeric($post_image_height)?$post_image_height:($is_portfolio ?self::$single_image_height:self::$single_image_height);
		}

		if ( is_single() && $this->hide_image != 'yes' ) {
			$this->image_align = '';
			$this->image_setting = 'setting=image_post_single&';
		} elseif ( $this->query_category != '' && $this->hide_image != 'yes' ) {
			$this->image_align = '';
			$this->image_setting = '';
		} else {
			$this->image_align = wpct_get( 'setting-image_post_align' );
			$this->image_setting = 'setting=image_post&';
		}
	}
}
?>
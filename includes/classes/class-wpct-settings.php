<?php
/**
 * Theme Settings Setup
 * @author   WpComet
 * @package  WPCT/Classes
 * @version  1.0.0
 */

if ( ! defined( 'ABSPATH' ) )	exit;

if ( ! class_exists( 'WPCT_Settings' ) ) :

class WPCT_Settings {
	public static $settings;
	public static $suggestions;
	
	public static function init() {
		add_action( 'init', array( __CLASS__, 'setup' ) );
	//	add_action( 'init', array( __CLASS__, 'suggest_search_setting' ) );
		add_action( 'wp_ajax_wpct_suggest_search_setting', array( __CLASS__, 'suggest_search_setting') );
		add_action( 'wp_ajax_nopriv_wpct_suggest_search_setting', array( __CLASS__, 'suggest_search_setting') );
	}
	
	public static function setup() {
		global $wpcomet_theme;
		$settings_folder = wpct_theme_get("settings_folder");
		// var_dump( $settings_folder );
		self::$settings = apply_filters('wpct_settings_before', array());
		self::$suggestions = apply_filters('wpct_setting_suggestions_before', array());
		//	var_dump( self::$settings );
		//	var_dump( wpct_locate_template( 'general_settings.php', false, $settings_folder ) );

		self::$settings["general"] =  require wpct_locate_template( 'general_settings.php', false, $settings_folder );
		self::$settings["layout"] =  require wpct_locate_template( 'layout_settings.php', false, $settings_folder );
		self::$settings["header"] =  require wpct_locate_template( 'header_settings.php', false, $settings_folder );
		self::$settings["content"] =  require wpct_locate_template( 'content_settings.php', false, $settings_folder );
		self::$settings["media"] =  require wpct_locate_template( 'media_settings.php', false, $settings_folder );
		self::$settings["footer"] =  require wpct_locate_template( 'footer_settings.php', false, $settings_folder );
		self::$settings["development"] =  require wpct_locate_template( 'development_settings.php', false, $settings_folder );
		//	self::$settings["performance"] =  require wpct_locate_template( 'performance_settings.php', false, $settings_folder );
		//	self::$settings["typography"] =  require wpct_locate_template( 'layout_settings.php', false, $customizer_settings_folder );
		//	self::$settings["development"] =  require wpct_locate_template( 'layout_settings.php', false, $customizer_settings_folder );
		//	var_dump( self::$settings  );

		self::$settings = apply_filters('wpct_settings_after', self::$settings);
		
		//section
		foreach (self::$settings as $section_key => $settings) {
			//setting
			foreach ($settings as $key => $setting) {
			//	var_dump( $setting );
				$default = ( isset($setting['control']['value']) ) ? $setting['control']['value'] : '';
				$settings_defaults[$key] = $default;
				
				if( isset(  $setting['control']['label'] ) ) {
					$label = ( isset ($setting['control']['label']['text'] ) ) ? $setting['control']['label']['text'] : $setting['control']['label'];
				}
				else {
					$label = $key;
				}
				$setting_suggestion = array(
					'key' => $key,
					'label' => $label,
					//'keywords' =>  $label,
					//'section' => $section_key,
				);
				if( isset($setting['control']['keywords']) ) {
					$setting_suggestion['keywords'] = $setting['control']['keywords'];
					//var_dump(  $setting['control']['keywords'] );
					//$setting_suggestion['keywords'] = $setting_suggestion['keywords'] . ',' . $setting['control']['keywords'];
				}
				self::$suggestions[$section_key][] = $setting_suggestion;
			}
		}
		self::$suggestions = apply_filters('wpct_setting_suggestions_after', self::$suggestions);
		$wpcomet_theme->settings_defaults = $settings_defaults;
	}
	
	public static function get($var=NULL) {
		//var_dump( $var );
		return ( $var && isset( self::$$var ) ) ? self::$$var : self::$settings;
	}
	
	function suggest_search_setting(){
		$suggestions = self::$suggestions;
		/*$suggestions=array();
		var_dump( $settings );
		foreach ( $settings as $setting ) {
			$suggestions[] = array(
				'key' => '',
				'section' => '',
			);
		}*/
		echo json_encode($suggestions);
		exit();
	}
}
endif;
<?php
/**
 * Generate interface elements. Helper class used by WpComet themes & plugins
 * @author      WpComet
 * @package     Nukleus/Interface
 * @version     1.0.6-t
 * TODO: Rework taxonomy field / prevent double parsing of args
 * TODO: Class-ify types ?
 */

if ( ! defined( 'ABSPATH' ) )	exit; 

class WPCT_Interface {
	static $fonts_dir = '11';
	/** Generate a field
	 * @param  string | $key |  key from option array
	 * @param  array  | $args | field arguments type etc.
	 * @param  string | $setting_key | field arguments
	 * @param  string | $storage | Where the values are stored option or meta
	 */
	 
	public static function gen_before_field($key,$args) {
		ob_start(); ?>
        <?php if( isset($args["wrap"]["outer"] ) ) : ?>
        <div class="<?php echo esc_attr( implode( ' ', $args['wrap']['outer'] ) ) ?>" 
		<?php if( isset( $args["depends"] ) && is_array($args["depends"]) ) : ?> data-depends="<?php echo esc_attr( json_encode($args["depends"]) ); ?>"<?php endif; ?>
		<?php if( isset( $args["affects"] ) && is_array($args["affects"]) ) : ?> data-affects="<?php echo esc_attr( json_encode($args["affects"]) ); ?>"<?php endif; ?>
        id="wrap-<?php echo $key ?>">
		<?php endif; ?>
       
		<?php if( $args['before'] )	echo wp_kses_post($args['before']); ?>    
        <?php //	var_dump( $args["wrap"]["group"] ) ?>
        <?php if( isset($args["wrap"]["group"] ) ) : ?><div class="<?php echo esc_attr( implode( ' ', $args['wrap']['group'] ) ) ?>" id="groupwrap-<?php echo $key ?>"><?php endif; ?>    
		<?php if ( ( is_array( $args['label'] ) && $args['label']['text'] || is_string( $args['label']) ) && $args['label']['show'] ) : ?>
            <label for="<?php echo $key ?>" 
            class="<?php if( isset( $args['label']['class'] ) ) : ?>
            <?php echo esc_attr( implode( ' ', $args['label']['class'] ) ) ?>"
            <?php endif; ?>
            >
			<?php echo isset( $args['label']['text'] ) ? $args['label']['text'] : $args["label"]; ?><?php if ( $args['required'] ) : ?><span class="rq" title="<?php echo esc_attr( __( 'Required'  ) )  ?>">*</span><?php endif; ?>
            
            <?php if ( $args['hint'] ) : ?><i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-html="true" title="<?php echo $args["hint"]; ?>"></i> <?php endif; ?>
    
            </label>
        <?php endif; ?>

        <?php if( isset($args["wrap"]["input"] ) ) : ?><div class="<?php echo esc_attr( implode( ' ', $args['wrap']['input'] ) ) ?>" id="inputwrap-<?php echo $key ?>"><?php endif; ?>
        
		<?php
		return ob_get_clean();
	}
	
	public static function gen_after_field($key,$args) {
		$desc_label = ( $args['desc']['label'] ) ? $args['desc']['label'] : '<i class="fa fa-info" aria-hidden="true"></i>';  
		$descr = $args['desc'];
		ob_start(); ?>
		<?php if( isset($args["wrap"]["input"] ) ) : ?></div><!-- input-wrap --><?php endif; ?>
		<?php if( isset($args["wrap"]["group"] ) ) : ?></div><!-- group-wrap --><?php endif; ?>
        <?php if ( $args['desc']["text"] ) : ?>
	        <input id="<?php echo $key ?> _cboxt" type="checkbox" 
            <?php if($args['desc']['collapse']) : ?>
            	checked="checked"
            <?php endif; ?>
            class="cbox_toggle_i" role="button">
			<label class="cbox_toggle descr_lbl <?php echo esc_attr( implode( ' ', $descr['label_class'] ) ) ?>" for="<?php echo esc_attr( $key ) ?>_cboxt">
				<span class="btn btn-sm btn-info cbt_s"><?php echo $desc_label ?></span><span class="btn btn-info cbt_h">— <?php echo $desc_label ?></span>
			</label>
			<div class="cbt_cont inp_desc"><?php echo $args['desc']['text']; ?></div>
		<?php endif; ?>
        <?php if( $args['after'] )	echo wp_kses_post($args['after']);	?>
        <?php if( isset($args["wrap"]["outer"] ) ) : ?></div><!-- outer-wrap --><?php endif; ?>
        <?php return ob_get_clean();
	}
	
	public static function show_field( $key, $args=array(), $storage='none', $setting_key='', $pid=NULL, $r = false ) {
		$storage = apply_filters( "wpctint_gen_field_storage", $storage, $key, $args );
		$setting_key = apply_filters( "wpctint_gen_field_setting_key", $setting_key, $key, $args );
		$pid = apply_filters( "wpctint_gen_field_pid", $pid, $key, $args );
	}
	
	static function do_storage_mods($storage, $key, $args, $setting_key, $pid) {
		if( $storage == "option" ) {
			// Get stored values from options option.
			$stored_vals = get_option($setting_key);
			//	var_dump( $setting_key );
			//	var_dump( $stored_vals );
			if($stored_vals && array_key_exists($key,$stored_vals) ) {
				$value = $stored_vals[$key];
			}
			//	var_dump( $value );
		}
		elseif( $storage == "postmeta" ) {
			global $post_ID;
			$single = true;
			$pid = ( isset( $pid ) ) ? $pid : $post_ID;

			$meta_key = str_replace( 'meta_','',$key);
			// var_dump( $pid,$meta_key );
			$value = get_post_meta( $pid, $meta_key, $single );
			// var_dump( $value );
		}
		elseif( $storage == "usermeta" ) {
			$current_user = wp_get_current_user();
			$meta_key = str_replace( 'user_','',$key);
			$user_id = $current_user->ID;
			$value = get_user_meta( $user_id, $meta_key, true );
			// var_dump( $value );
		}
		elseif( $storage === "get" && ! empty( $_GET ) ) {
		//	if( ! isset(  $_GET[$args["group"]][$args["name"]] ) ) wpevd( $_GET["prop_filters"]);
			if( $args["group"] ) {
				//	wpevd( $_GET);
				//	var_dump( $_GET[$args["group"]][$args["name"]] );
				$value = isset( $_GET[$args["group"]][$key] ) ? $_GET[$args["group"]][$key] : false;
				//	$args["name"]
				
			}
			else {
				$value = isset( $_GET[$key] ) ? $_GET[$key] : false;
			}
		//	var_dump( $args["name"] );
			
			//$value = $_GET[$key];
			// var_dump( $value );
		}
		elseif( $storage == "customizer") {
			//	$args["attr"]["data-customize-setting-link"] = $key;
		}
		else {
			if ( strpos( $key, 'core_' ) === 0 ) {
				// var_dump("33");
					$meta_key = str_replace( 'core_','',$key);
					//	var_dump( $pid );
					$value = ( $pid ) ? get_post_field( $meta_key, $pid ) : '';
					//	var_dump( $value );
					//var_dump( $value, $pid );
			}		
			elseif( strpos( $key, 'tax_' ) === 0 ) {
				//	if( $args["type"] === "taxonomy" )
				$terms = wp_get_object_terms( $pid, $args["misc"]["tax"], array('fields' => 'ids', 'order' => 'ASC','orderby' => 'parent' ) );
				
				if( $args["type"] === "text" && empty( $terms ) ) {
					$terms = "";
				}
				//	var_dump( $terms );
				//	$value = ( ! empty( $terms ) ) ? $terms : false;
				//	var_dump( $pid, $args["misc"]["tax"] );
				//	var_dump( $terms );
				
				$value = $terms;
				// var_dump( $value );
				//	if( empty( $value_maybe ) ) $value_maybe = $
				//	 var_dump( $value_maybe );
				// var_dump( $args["misc"]["tax"], $pid  );
			}
			else {
				$value = $args['value'];
			}	
		}
		if( ! isset( $value ) ) $value = $args['value'];

		return $value;
	}
	
	/**
	 * Do args modifications before the field is generated 
	 */
	static function do_args_mods_before($args,$key,$storage) {
		// add common input class
		if( isset( $args["affects"] ) && is_array($args["affects"]) ) $args['class'][] = 'affects';
		if( isset( $args["placeholder"] ) ) $args["attr"]["placeholder"] = $args["placeholder"];
		// if the label is a string, apply it to the desc text array
		if( isset( $args["label"] ) && ! is_array($args["label"]) ) $args["label"] = array( "text" => $args["label"]);
		// if the description is a string, apply it to the desc text array
		if( isset( $args["desc"] ) && ! is_array($args["desc"]) ) $args["desc"] = array( "text" => $args["desc"]);
		// Assign keywords to search for a setting
		//if( isset( $args["keywords"] ) ) $args["attr"]["data-search-term"] = $args["keywords"];

		//	var_dump( $args["label"] );
		//	var_dump( $storage );
		if(  $storage == "customizer" )  $args["attr"]["data-customize-setting-link"] = $key;
		//	var_dump( $args);
		return $args;
	}
	
	static function do_args_mods_after($args,$key) {
		//var_dump($args['type']);
		$args['class'][] = 'input-' . $args['type'];
		$args["attr"]["data-field-type"] = $args["type"];
		if( $args["type"] == "radiogroup" )	$args["wrap"]["input"][] = 'radio-gr';
		if ( $args['type'] == 'colorpicker' )	{
			$args['class'][] = 'color-field';
			$args["attr"]["data-default-color"] = $args["value"];
		}
		// add validate classes
		foreach( $args['validate'] as $validate ) {
			$args['class'][] = 'valid8-' . $validate;
		}
		return $args;
	}
	
	static function do_name_mods($name,$key,$args) {
		// todo review  taxonomy creating process 
		if( $args["type"] !== 'taxonomy' ) {
			if( $args["group"] )  {
				$args["name"] = $args["group"] . '[' . $args["name"] . ']';
			}
			elseif( strpos( $key, 'meta_' ) === 0 )  {
				$args["name"] = 'wpc_meta[' . $args["name"] . ']';
			}
			/*elseif( strpos( $key, 'core_' ) === 0 )  {
				$args["name"] = 'wpc_core[' . $args["name"] . ']';
			}*/
			elseif( isset( $args["misc"]["tax"] ) )  {
				$args["name"] = 'wpc_tax['.$args["misc"]["tax"].']['.$args["name"].']';
			}
		}
		return $name;
	}
	
	/**
	 * Sanitize and compress field output
	 */
	static function sanitize_output($buffer){
		$search = array(
			'/\>[^\S ]+/s', //strip whitespaces after tags, except space
			'/[^\S ]+\</s', //strip whitespaces before tags, except space
			'/(\s)+/s'  // shorten multiple whitespace sequences
			);
		$replace = array(
			'>',
			'<',
			'\\1'
			);
	  $buffer = preg_replace($search, $replace, $buffer);
		return $buffer;
	}
	
	/**
	 * Generate and output field.
	 */
	public static function gen_field( $key, $args=array(), $storage='none', $setting_key='', $pid=NULL, $r = false ) {
		if ( $key == "section-icon" ) return false;
		$storage = apply_filters( "wpctint_gen_field_storage", $storage, $key, $args );
		$setting_key = apply_filters( "wpctint_gen_field_setting_key", $setting_key, $key, $args );
		$pid = apply_filters( "wpctint_gen_field_pid", $pid, $key, $args );
		$r = apply_filters( "wpctint_gen_field_return", $r, $key, $args );
		$safe = apply_filters( "wpctint_gen_field_safe", true );
		$compress = apply_filters( "wpctint_gen_field_compress", true );
		//	var_dump( self::$fonts_dir );
		//	$fonts_dir = apply_filters( "wpctint_gen_field_fonts_dir", true );
		$key = ( $safe ) ? $key : esc_attr( $key );

		$defaults = apply_filters( "wpctint_field_default_args", array(
			'type'              => 'text',
			'value'               => '',
			'options'           => false,
			'label'             => array( 'text' => false, 'class' => array('form-control-label'), 'for' => false, 'show' => true, ),
			'hint'              => false,
			'icon'              => false,
			'class'             => array( 'wpc-input','form-control' ),
			'name'              => $key,
			'wrap'              => array( 'group' => array('form-group'), 'outer' => array('outer-wrap','wpc-field') ),
			'required'          => false,
			'desc'              => array( 'text' => false, 'label' => false, 'collapse' =>true, 'label_class' => array() ),
			'attr'              => array(),
			'validate'          => array(),
			'sanitize'          => false,
			'before'            => false,
			'after'             => false,
			'return'            => false,
			'misc'              => false,
			'group'             => false,
			'cap'               => false,
			'depends'           => false,
			'affects'           => false,
			// 'wrap'              => array( 'outer' => array('outer-wrap'), 'group' => array('form-group'), 'input' =>  array('input-group'), 'preview' =>  array('outer-wrap') ),
			// 'misc' => array('style' => '', 'preview_class', 'input_class'),
			// 'placeholder'       => false,
			// 'label_for'         => false,
			// 'abbr'              => false,
			// 'title'             => '',
			// 'maxlength'         => false,
			// 'id'                => $key,
		) );
		$args = self::do_args_mods_before( $args, $key, $storage  );

		foreach ( $args as $k => $sub_arg ) {
			if( is_array( $sub_arg ) )	$args[$k] = wp_parse_args( $args[$k], $defaults[$k]  );	
		}
		$args = wp_parse_args( $args, $defaults  );
		//	var_dump( $args );
		$args = self::do_args_mods_after( $args, $key, $storage  );
		$args = apply_filters( "wpctint_gen_field_args", $args );

		$name = self::do_name_mods( $args["name"],$key,$args);
		$name = apply_filters( "wpctint_gen_field_name", $pid, $key, $args );

		$before = self::gen_before_field($key,$args);
   		$after = self::gen_after_field($key,$args);
			
		$value = $args['value'];

		$value = self::do_storage_mods( $storage, $key, $args, $setting_key, $pid );
		//var_dump( $value);
		$value = apply_filters( 'wocnuk_modify_field_value', $value, $key, $args, $pid);
		//var_dump( $args);

		?>
   		<?php
		
		switch ( $args['type'] ) :
		case "text" :
		case "colorpicker" :
		case "number" :
		case "search" :
		case "email" :
		case "url" :
		case "tel" :
		case "range" :
		case "date" :
		case "month" :
		case "week" :
		case "time" :
		case "datetime" :
		case "datetime-local" :
		case "color" :
			$field = $before;
			ob_start(); ?>
			<input type="<?php echo $args['type'] ?>" class="<?php echo esc_attr( implode( ' ', $args['class'] ) ) ?>" name="<?php echo esc_attr( $args['name'] ); ?>" id="<?php echo $key ?>"
            <?php foreach( $args["attr"] as $atk => $attr ) : ?><?php echo $atk ?>="<?php echo $attr ?>" <?php endforeach; ?>
            value="<?php echo esc_attr( $value ) ?>"
            <?php if( $args["required"] ) : ?> required<?php endif; ?> />
			<?php $field .= ob_get_clean();
			$field .= $after;
			break;
	
		case "upload":
			// WPCT-todo: standartize before due to form group has-warning
			$args["class"][] = 'upload';
			
			$args["attr"]["placeholder"] = __('No file chosen', 'gamer-life' );
			
			$is_image = ( $value == '' ) ? false : preg_match( '/(^.*\.jpg|jpeg|png|gif|ico*)/i', $value );
			if( ! $is_image ) {
				$args["wrap"]["group"][] = 'has-warning';
				$args["class"][] = 'form-control-warning';
			}
			
			//	$field = $before;
			$field = $before = self::gen_before_field($key,$args);

			//	var_dump( $args );
			ob_start(); ?>
            
            <?php if( isset($args["wrap"]["input_inner"] ) ) : ?><div class="<?php echo esc_attr( implode( ' ', $args['wrap']['input_inner'] ) ) ?>"><?php endif; ?>
            
            <input type="<?php echo $args['type'] ?>" class="<?php echo esc_attr( implode( ' ', $args['class'] ) ) ?>" name="<?php echo esc_attr( $args['name'] ); ?>" id="<?php echo $key ?>"
            <?php foreach( $args["attr"] as $atk => $attr ) : ?><?php echo $atk ?>="<?php echo $attr ?>" <?php endforeach; ?>
            value="<?php echo esc_attr( $value ) ?>"
            <?php if( $args["required"] ) : ?> required<?php endif; ?> />  
            
            <?php if ( ( $value == '' ) ) : ?>
            <span class="input-group-btn">
				<a id="upload-<?php echo $key ?>" class="upload-button btn btn-sm btn-info" href="#" role="button">
                	<i class="fa fa-upload" aria-hidden="true"></i>
                    <span>&nbsp;<?php _e( 'Upload', 'gamer-life' ) ?></span>
                </a>
            </span>
			<?php else : ?>
            <span class="input-group-btn">
				<a id="remove-<?php echo $key ?>" class="remove-file btn btn-sm btn-warning" href="#" role="button">
                	<i class="fa fa-trash" aria-hidden="true"></i>
                	<span>&nbsp;<?php _e( 'Remove', 'gamer-life' ) ?></span>
				</a>
            </span>
			<?php endif; ?>	
            
            
			<?php if( isset($args["wrap"]["input_inner"] ) ) : ?></div><!-- close input_inner-wrap --><?php endif; ?>
                        <?php if( ! $is_image ) : ?><div class="form-control-feedback">Doesn't appear to be a valid image, check the source or reupload.</div><?php endif; ?>

            <?php if( isset($args["wrap"]["preview"] ) ) : ?><div class="<?php echo esc_attr( implode( ' ', $args['wrap']['preview'] ) ) ?>"><?php endif; ?>
            
            <div class="uplprev wpct-preview preview-upload" id="<?php echo $key ?>-preview">
                <?php if ( $is_image ) : ?>
                <a class="remove-image-inline btn btn-sm btn-danger">X Remove</a>
                <img src="<?php echo $value ?>" alt="" class="img-fluid img-thumbnail" />
                <?php else : ?>
                <div class="no-image"><span class="file_link"><a href="<?php echo $value ?>" target="_blank" rel="external"><?php __( 'View File', 'gamer-life' );?></a></span></div>
                <?php endif; ?>	
            </div>
   
			<?php if( isset($args["wrap"]["preview"] ) ) : ?></div><!-- preview-wrap --><?php endif; ?>

            
			<?php $field .= ob_get_clean();
			//	$field .= self::gen_uploader( $setting_key, $key, $value, null, '',  wpct_getvar('TextDomain') );
			$field .= $after;
			break;
			
		case "datepicker" :
			$args["class"][] = 'datepicker';
			$field = $before;
			ob_start(); ?>
			<input type="text" class="<?php echo esc_attr( implode( ' ', $args['class'] ) ) ?>" name="<?php echo esc_attr( $args['name'] ); ?>" id="<?php echo $key ?>"
            <?php foreach( $args["attr"] as $atk => $attr ) : ?><?php echo $atk ?>="<?php echo $attr ?>"<?php endforeach; ?>
            value="<?php echo esc_attr( $value ) ?>"
            <?php if( $args["required"] ) : ?> required<?php endif; ?> />
			<?php $field .= ob_get_clean();
			$field .= $after;
			break;
			
		case "row" :
			ob_start(); ?>
            <div class="row">
            <?php
            $field .= ob_get_clean();
			break;
			
		case "select" :
		case "multiselect" :
			$args["class"] = array_diff( $args["class"], array("form-control") );

			if( $args["type"] === "select" ) $args["class"][] = 'custom-select';
			//	var_dump(  $args["attr"] );
			if( $args["type"] === "multiselect" ) $args["attr"]['multiple'] = 'multiple';
			if( $args["type"] === "multiselect" ) $args["name"] = $args["name"] . '[]';
			 // var_dump( $value );
			// var_dump( $args["options"] );
			$field = $before;
			if( ! in_array('select2', $args["class"]) ) 
			if (ctype_digit($value))  $value = (int) $value;
			
			// wpevd( $args["options"] );
			ob_start(); ?>
			<select class="<?php echo esc_attr( implode( ' ', $args['class'] ) ) ?>" name="<?php echo esc_attr( $args['name'] ); ?>" id="<?php echo $key ?>"
				<?php foreach( $args["attr"] as $atk => $attr ) : ?>
                <?php echo $atk ?>="<?php echo $attr ?>" 
                <?php endforeach; ?>
            <?php if( $args["required"] ) : ?>
            required
            <?php endif; ?>>
	            <option value=""><?php if( isset( $args["attr"]["placeholder"] ) ) : ?> <?php echo $args["attr"]["placeholder"] ?> <?php else : ?><?php _e("— Select —") ?><?php endif; ?></option>
            	<?php foreach( $args["options"] as $k => $opttxt ) : ?>
                	<?php $selected = ( $args["type"] === "multiselect" ) ? selected( in_array( $k, $value ), true, false ) : selected( $value, $k, false );?>
                    <option value="<?php echo esc_attr( $k ); ?>"<?php echo $selected; ?>>
                        <?php echo esc_html( $opttxt ); ?>
                    </option>
				<?php endforeach; ?>
            </select>
			<?php
			$field .= ob_get_clean();
			$field .= $after;
			break;
			
		case "checkbox" :
			$field = $before;
			// var_dump( $value );
			ob_start(); 
			$args['class'][] = 'custom-control-input';
			array_push( $args['label']['class'], 'custom-control','custom-checkbox');
			?>
            <label for="<?php echo esc_attr( $key . '_' . $k ) ?>" class="<?php echo implode( ' ', $args['label']['class'] ) ?>">
                <input type="checkbox" class="<?php echo esc_attr( implode( ' ', $args['class'] ) ) ?>" name="<?php echo esc_attr( $args['name'].'[]' ) ?>"
                 id="<?php echo esc_attr( $key . '_' . $k ) ?>" <?php echo checked($value,1,true) ?> value="<?php echo $k ?>"
                 <?php if( $args["required"] ) : ?> required<?php endif; ?> />
                <span class="custom-control-indicator"></span>
                <span class="custom-control-description"><?php echo $args['label']['text'] ?></span>
            </label>
			<?php
			$field .= $after;
			$field .= ob_get_clean();
			break;	
			
		case "taxonomy" : 
			$field = '';
			$taxonomy = isset( $args["misc"]["tax"] ) ? $args["misc"]["tax"] : "category";
			$mode = isset( $args["misc"]["mode"] ) ? $args["misc"]["mode"] : "select";
			
			ob_start();
			$tax_args = array(
				'taxonomy'     => $taxonomy,
				//	'parent'        => 0,
				'number'        => 80,
				'fields'        => 'id=>name',
				'hide_empty'    => false        
			);
			if( isset( $args["misc"]["args"] ) )  $tax_args = wp_parse_args( $args["misc"]["args"], $tax_args );
			// if(  $args["misc"]["tax"] === "locale" ) die( var_dump( $tax_args ) );
			$args["options"] = get_terms( $tax_args );
			
			$args["class"][] = "tax-" . $taxonomy; 
			// if( ! empty( $value) ) $args["value"] = ( $args["misc"]["mode"] === "multicheck" ) ? $value : $value[0];
			$args["value"] = $value;
			// var_dump( $args["value"], $value );
			//if(  $args["misc"]["tax"] === "locale" ) die( var_dump( $args["options"]  ) );
			if( $mode == "text" ) {
				/* var_dump(  $args["value"] );
				$pto = get_term_by( 'id', $args["value"], $args["misc"]["tax"] );
				if( $pto ) $args["value"] = $pto->name;*/
			}

			// var_dump( $args["value"] );
			?>
            
            <?php if( $mode === "select" ) : 
				// $args["class"][] = "select2";
				$args["type"] = "select"; ?>
            	<?php self::gen_field( $key, $args ) ?>            
            <?php elseif( $mode === "multicheck" ) : 
				$args["type"] = "multicheck"; 
				// var_dump( $args ); ?>
            	<?php self::gen_field( $key, $args ) ?>
            <?php elseif( $mode === "text" ) : 
			// var_dump( $args );
				$args["type"] = "text"; ?>
            	<?php self::gen_field( $key, $args ) ?>
            <?php endif; ?>
			
			<?php
			$field .= '';
			$field .= ob_get_clean();
			break;
			
		case "textarea" : 
			if( ! isset( $args["attr"]["rows"] ) ) $args["attr"]["rows"] = 3;
			if( ! isset( $args["attr"]["cols"] ) ) $args["attr"]["cols"] = 20;
			//	var_dump( $args );
			$field = $before;
			//	var_dump( $value );
			ob_start(); ?>
            <textarea name="<?php echo esc_attr( $args['name'] ) ?>" class="<?php echo esc_attr( implode( ' ', $args['class'] ) ) ?>" id="<?php echo $key ?>"
            <?php foreach( $args["attr"] as $atk => $attr ) :
            echo ' ' .$atk ?>="<?php echo $attr ?>"
            <?php endforeach; ?>
            ><?php echo esc_textarea( $value  ) ?></textarea>
			<?php
			$field .= $after;
			$field .= ob_get_clean();
			break;
				
		case "aceeditor" : 
			$field = $before;
			ob_start(); ?>
			<div class="ace-wrapper">
            	<textarea name="<?php echo esc_attr( $args['name'] ) ?>" class="ace-editor <?php echo esc_attr( implode( ' ', $args['class'] ) ) ?>" id="<?php echo $key ?>"
                data-editor="<?php echo $key ?>-editor"
                data-mode="<?php echo ( array_key_exists( "data-mode", $args['custom_attributes'] ) ) ? $args['custom_attributes']["data-mode"] : "php"; ?>"
                data-theme="<?php echo ( array_key_exists( "data-theme", $args['custom_attributes'] ) ) ? $args['custom_attributes']["data-theme"] : "Chrome"; ?>"
                <?php implode( ' ', $custom_attributes ) ?>>
                	<?php echo $value  ?>
                </textarea>
                <pre id="<?php echo $key ?>-editor" class="ace-editor-area"><?php echo htmlspecialchars( $value ); ?></pre>
            </div>
        	<?php
			$field .= $after;
			$field .= ob_get_clean();
			break;
	
			
		case "radio" :
			$field = $before;	
			foreach($args['options'] as $k => $optxt) {
				$label_for = $args["label_for"] ? $args["label_for"] : $key . '_' . $k;
				$field .='<input type="radio" class="' . esc_attr( implode( ' ', $args['class'] ) ) .'" name="' . esc_attr( $args['name'] ) . '" id="' . esc_attr( $key . '_' . $k ) . '" value="' . esc_attr( $k ) .'" ';
				$field .= checked( $value, $k, false ) .' />';	
				$field .= ($optxt === strip_tags($optxt)) ? '<label for="' . esc_attr( $label_for ) . '" class="' . implode( ' ', $args['label_class'] ) .'">' . $optxt . '</label>' : $optxt;	
			}
	
			$field .= $after;
			break;
			
			
			
		case "payment" :	
			// var_dump( $value );
			if( empty( $value ) ) $value = array();
			$field = $before;
			$args['class'][] = 'form-check-input';
			$args['class'][] = 'custom-control-input';
			$args['label']['class'][] = 'form-check-label';
			// $field = '';
			// $value = array();
			ob_start(); ?>
            <?php foreach($args['options'] as $k => $ctxt) :
			
			 ?>
             
            <div class="form-check form-check-inline">
            <?php //var_dump( $value[$k] ) ?>
            <?php // var_dump( $value ) ?>
            <?php //var_dump( $k ) ?>
            <?php //var_dump( array_key_exists( $k, $value ) ); ?>
            	<?php $is_checked = array_key_exists( $k, $value ); ?>
				<label for="<?php echo esc_attr( $key . '_' . $k ) ?>" class="<?php echo implode( ' ', $args['label']['class'] ) ?>">
                     <input type="checkbox" class="<?php echo esc_attr( implode( ' ', $args['class'] ) ) ?>" name="<?php echo esc_attr( $args["name"] . '[' . $k .']' ) ?>" id="<?php echo esc_attr( $key . '_' . $k ) ?>" 					 <?php checked($is_checked,true) ?>><span><i class="fa fa-<?php echo $k ?>" style="font-size:24px"></i> <em><?php echo $ctxt ?></em></span>
				</label>  
            </div>
             
            <?php endforeach; ?>

			<?php
			$field .= $after;
			$field .= ob_get_clean();
			break;
			
						
		case "dndupload" :		
			$field = $before;
			// $field = '';
			ob_start(); ?>	
            <?php // $post = get_post( $pid );
            ?>
            <div id="dndwrap">
                <div id="uploadContainer" class="wpcnuk-dndupload clearfix">
                    <!-- Uploader section -->
                    <div id="uploaderSection">
                        <div class="loading">
                            <img src="<?php echo wpct_theme_get("baseuri") ?>/assets/img/loading.gif" alt="Loading..." />
                        </div>
                        <div id="plupload-upload-ui" class="hide-if-no-js">
                            <div id="drag-drop-area">
                                <div class="drag-drop-inside">
                                    <p class="drag-drop-info"><?php _e('DROP YOUR IMAGE(S) HERE', 'gamer-life'); ?></p>
                                    <p><?php _e('or', 'Uploader: Drop files here - or - Select Files', 'gamer-life'); ?></p>
                                   <p class="drag-drop-buttons">
                                    <label class="custom-file">
                                      <input id="plupload-browse-button" class="custom-file-input" type="button" value="<?php esc_attr_e('Browse Files', 'gamer-life'); ?>" class="button" />
                                      <span class="custom-file-control"></span>
                                    </label>
                                   </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!-- Current image -->
                    <div id="current-uploaded-image" class="clearfix <?php echo has_post_thumbnail() ? 'open' : 'closed'; ?>">
                    <?php  if( $pid ) : ?>
                    <?php  $media = get_attached_media( 'image', $pid );
                        foreach ( $media as $img ) :
                        // var_dump( $img );
                    ?>
                     <div class="existimgwrap imgttwrap" id="mitem<?php echo $img->ID ?>">
                        <?php $ajax_nonce = wp_create_nonce("remove_media-$img->ID"); ?>
                        <?php echo wp_get_attachment_image($img->ID,"thumbnail", false, array( "class" => "img-fluid" )) ?>
                        <a class="btn-sm btn btn-danger remthum" href="javascript:void(0)" data-tid="<?php echo $img->ID ?>" data-nonce="<?php echo $ajax_nonce; ?>">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </a>
                    </div>
                    <?php endforeach; ?>
                    <?php endif; ?>
                    </div>
                    
                </div><!-- #uploadContainer -->
            </div>
			<?php 
			$field .= ob_get_clean();
			$field .= $after;
			break;	
			
		case "pages" :
			$field = $before;	
			foreach($args['options'] as $k => $optxt) {
				$field .='<input type="radio" class="' . esc_attr( implode( ' ', $args['class'] ) ) .'" name="' . esc_attr( $args['name'] ) . '" id="' . esc_attr( $key . '_' . $k ) . '" value="' . esc_attr( $k ) .'" ';
				$field .= checked( $value, $k, false ) .' />';	
				$field .='<label for="' . esc_attr( $key . '_' . $k ) . '" class="' . implode( ' ', $args['label_class'] ) .'">' . $optxt . '</label>';	
			}
	
			$field .= $after;
			break;
			
			
		case "images" :
			$field = $before;
			//	var_dump($args);
			$field .= '<input id="' . esc_attr( $key ) . '_imgs_cboxt" type="checkbox" ';
			$field .= 'class="cbox_toggle_i" role="button"><label class="cbox_toggle" for="' . esc_attr( $key ) . '_imgs_cboxt">';
			$field .= '<span class="btn btn-sm btn-success cbt_s">+ Show Images</span><span class="btn btn-sm btn-primary cbt_h">— Hide Images</span>';
			$field .='</label>';
		
			$field .= '<div class="row imagesrow card cbt_cont togglebox">';
			foreach($args['options'] as $k => $imgsrc) {
				list($w, $h) = getimagesize($imgsrc); 
				$irat = $w / $h;
				$rcl = ( $irat > 1 ) ? 'horz' : 'vert';
				if($irat > 2) { $rcl .= ' h2x';}
				elseif($irat > 3) {$rcl .= ' h3x'; }
				$selected = ( $value != '' && ($value == $k) ) ?  ' selected' : '';
				
				$field .= '<div class="radio-img-wrap '.$rcl.$selected.' col-xs-6 col-md-4 col-lg-3">';
				$field .= '<span class="radio-imglbl">' . esc_html( $k ) . '</span>';
				$field .='<input type="radio" class="' . esc_attr( implode( ' ', $args['class'] ) ) .'" name="' . esc_attr( $args['name'] ) . '" id="' . esc_attr( $key .'_'. $k ) . '" value="' . esc_attr( $k ) .'" ';
				$field .= checked( $value, $k, false ) .' />';	
				$field .= '<img src="' . esc_url( $imgsrc ) . '" alt="' . $k .'" title="' . $k .'" class="radio-img img-fluid" />';
				$field .= '</div>';
			}
			$field .= '</div>';
			$field .= $after;
			break;
			
		case "multicheck" :
			$field = $before;
			// var_dump( $value );
			ob_start(); 
			$args['class'][] = 'custom-control-input';
			array_push( $args['label']['class'], 'custom-control','custom-checkbox');
			?>

            <?php foreach($args['options'] as $k => $ctxt) :
				if (ctype_digit($k))  $k = (int) $k;
				// var_dump( $k, $ctxt );
				$is_checked = in_array( $k, $value ); ?>
				<label for="<?php echo esc_attr( $key . '_' . $k ) ?>" class="<?php echo implode( ' ', $args['label']['class'] ) ?>">
                    <input type="checkbox" class="<?php echo esc_attr( implode( ' ', $args['class'] ) ) ?>" name="<?php echo esc_attr( $args['name'].'[]' ) ?>"
                     id="<?php echo esc_attr( $key . '_' . $k ) ?>" <?php checked($is_checked,true) ?> value="<?php echo $k ?>">
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description"><?php echo $ctxt ?></span>
                </label>
            <?php endforeach; ?>
			<?php
			$field .= $after;
			$field .= ob_get_clean();
			break;
		
		case "radiogroup" :
			$assets_uri = wpct_theme_get("assets_uri");
			$field = $before;
			$rg_style = ( isset( $args["misc"]["style"] ) ) ? $args["misc"]["style"] : "default";
			$sub_lbl_cl = array("radiogr-label");

			if( isset( $args['misc']['sublabel_class'] ) ) $sub_lbl_cl = array_merge($sub_lbl_cl, $args['misc']['sublabel_class']);
			if ( isset( $args["misc"]["style"] ) ) $args['class'][] = $args["misc"]["style"];

			if( $rg_style === "button" ) {
				array_push( $args['label']['class'], 'btn','btn-primary');
			}
			elseif( $rg_style === "custom" ) {
				$args['class'][] = 'custom-control-input';
				array_push( $args['label']['class'], 'custom-control','custom-radio');
			}
			// var_dump( $value );
			ob_start(); 
			//	$args['class'][] = 'custom-control-input';
			//	var_dump( $args);

			?>
			<?php if( $rg_style == "button" ) : ?><div class="btn-group" data-toggle="buttons"><?php endif; ?>
				<?php foreach($args['options'] as $k => $ctxt) :
                    if (ctype_digit($k))  $k = (int) $k;
                    // var_dump( $k, $ctxt );
                    $is_checked = $value == $k;
					//	var_dump( $value,$k);
					//	var_dump( $is_checked);
					//	var_dump( $args["class"]);

                    if( $is_checked ) $args['class'][] = "active";
					//	var_dump( $args["class"]);
                     ?>
                    <label for="<?php echo esc_attr( $key . '_' . $k ) ?>" class="<?php echo implode( ' ', $sub_lbl_cl ) ?>">
                        <input type="radio"
                         class="<?php echo esc_attr( implode( ' ', $args['class'] ) ) ?>"
                         name="<?php echo esc_attr( $args['name'] ) ?>"
                         id="<?php echo esc_attr( $key . '_' . $k ) ?>" <?php checked($value,$k); ?> value="<?php echo $k ?>"
                         autocomplete="off" <?php if( $args["required"] ) : ?> required<?php endif; ?> />
                         <?php if( $rg_style ==="custom" ) : ?>
                             <span class="custom-control-indicator"></span>
                             <span class="custom-control-description"><?php echo $ctxt ?></span>
                         <?php elseif( $rg_style ==="image" ) : 
						 $img_ext = ( isset( $args['misc']['extension'] ) ) ? $args['misc']['extension'] : 'jpg';
						 $img_name = ( isset( $args['misc']['prefix'] ) ) ? $args['misc']['prefix'] . '-' . $k : $k;
						 ?>
                         <img src="<?php echo $assets_uri ?>/img/<?php echo $img_name . '.' . $img_ext ?>" alt="<?php echo $ctxt ?>" data-toggle="tooltip" class="img-fluid" title="<?php echo $ctxt ?>" />
                         <?php else : ?>
                         	&nbsp;<?php echo $ctxt ?>
                         <?php endif; ?> 
                    </label>
                <?php endforeach; ?>
                
                <input type="hidden" class="wpctcc-value-field <?php echo esc_attr( implode( ' ', $args['class'] ) ) ?>" name="<?php echo esc_attr( $args['name'] ); ?>" id="<?php echo $key ?>"
                <?php foreach( $args["attr"] as $atk => $attr ) : ?><?php echo $atk ?>="<?php echo $attr ?>" <?php endforeach; ?>
                value="<?php echo esc_attr( $value ) ?>" />
            
			<?php if( $rg_style == "button" ) : ?></div><?php endif; ?>
			<?php
			$field .= ob_get_clean();
			$field .= $after;
			break;
		
		case "switch" :
			// WPCT-TODO: Add default switch style
			$field = $before;
			$style = ( isset( $args["misc"]["style"] ) ) ? $args["misc"]["style"] : "coreui";
			$data_on = ( isset( $args["misc"]["data"]["on"] ) ) ? $args["misc"]["data"]["on"] : "On";
			$data_off = ( isset( $args["misc"]["data"]["off"] ) ) ? $args["misc"]["data"]["off"] : "Off";
			$is_checked = ( $value == $data_on );
			if( $style === "coreui" ) array_push( $args['class'], 'switch-input');
			ob_start(); ?>
			<?php if( $style == "coreui" ) : ?>

			<label class="switch switch-text switch-primary">
				<input type="checkbox" class="<?php echo esc_attr( implode( ' ', $args['class'] ) ) ?>" name="<?php echo esc_attr( $args['name'] ) ?>"
				<?php foreach( $args["attr"] as $atk => $attr ) : ?><?php echo $atk ?>="<?php echo $attr ?>" <?php endforeach; ?>
                 id="<?php echo esc_attr( $key ) ?>" <?php checked($is_checked,true) ?> value="<?php echo $value ?>"
                 <?php if( $args["required"] ) : ?> required<?php endif; ?> />
				<span class="switch-label" data-on="<?php echo $data_on ?>" data-off="<?php echo $data_off ?>"></span>
				<span class="switch-handle"></span>
			</label>

			<?php else : ?>
			
			<?php endif ?>
			<?php
			$field .= ob_get_clean();
			$field .= $after;
			break;
			
		case "rating" :
			$field = $before;
			$args["class"] = array_diff( $args["class"], array("form-control") );
			$args["class"][] = "star-rating__input";
			//	var_dump( $value );
			ob_start(); ?>
     
             <div class="wpc-star-rating">
                  <div class="star-rating__wrap">
                
                    <input  class="<?php echo esc_attr( implode( ' ', $args['class'] ) ) ?>" id="star-rating-5" type="radio" name="<?php echo esc_attr( $args['name'] ); ?>" value="5"
                    <?php if( $value == "5" ) echo ' checked="checked"'; ?> />
                    <label class="star-rating__ico fa fa-star-o" for="star-rating-5" title="5 out of 5 stars"></label>
                    <input  class="<?php echo esc_attr( implode( ' ', $args['class'] ) ) ?>" id="star-rating-4" type="radio" name="<?php echo esc_attr( $args['name'] ); ?>" value="4"
                    <?php if( $value == "4" ) echo ' checked="checked"'; ?> />
                    <label class="star-rating__ico fa fa-star-o" for="star-rating-4" title="4 out of 5 stars"></label>
                    <input  class="<?php echo esc_attr( implode( ' ', $args['class'] ) ) ?>" id="star-rating-3" type="radio" name="<?php echo esc_attr( $args['name'] ); ?>" value="3"
                    <?php if( $value == "3" ) echo ' checked="checked"'; ?> />
                    <label class="star-rating__ico fa fa-star-o" for="star-rating-3" title="3 out of 5 stars"></label>
                    <input  class="<?php echo esc_attr( implode( ' ', $args['class'] ) ) ?>" id="star-rating-2" type="radio" name="<?php echo esc_attr( $args['name'] ); ?>" value="2"
                    <?php if( $value == "2" ) echo ' checked="checked"'; ?> />
                    <label class="star-rating__ico fa fa-star-o" for="star-rating-2" title="2 out of 5 stars"></label>
                    <input  class="<?php echo esc_attr( implode( ' ', $args['class'] ) ) ?>" id="star-rating-1" type="radio" name="<?php echo esc_attr( $args['name'] ); ?>" value="1"
                    <?php if( $value == "1" ) echo ' checked="checked"'; ?> />
                    <label class="star-rating__ico fa fa-star-o" for="star-rating-1" title="1 out of 5 stars"></label>
                  </div>
            </div>
            		
			<?php
			$field .= ob_get_clean();
			$field .= $after;
			break;
			
		
			
		case 'typography':
			$field = $before;
			unset( $font_face, $font_style, $font_size, $font_color, $font_subset );
			//var_dump( self::fo
			$os_font_faces =  require_once( $plugin_info['plug_dir'] . 'nukleus/xtra/os_fonts.php' );
			
			// If we have a proper Google Fonts transient in place
			$gf_trans_name =  $vars['reg_transients']['google_fonts'];
			$gfjson_trans = get_transient($gf_trans_name);
			if( $gfjson_trans && is_array($gfjson_trans) && ! empty($gfjson_trans) ) {
				$google_font_faces = $gfjson_trans;
			}
			else {
				// _eo-review: Merge local file & google api requested json data process into 1 step as $gf_json_src ?
				
				$google_font_faces_rebuilt = array();
				// For google fonts we should have a prefetched json file already.
				$gfjson_file = $vars['plug_dir'] . 'nukleus/xtra/gfjsondump.txt';
				if( file_exists($gfjson_file) ) {
					$gfjson_file_uri = $vars['plug_uri'] . 'nukleus/xtra/gfjsondump.txt';
					$gf_json = wp_remote_fopen($gfjson_file_uri);
					$gf_json_data = json_decode($gf_json,true);
					//	var_dump($gf_json_data);
					
					//$google_font_faces = $gf_json_data['items'];
					// Get rid of unnecessary data, key the items, and save as transient for convenience.
					foreach ( $gf_json_data['items'] as $gfitem ) {
						$lite_item = array(
							'family' => $gfitem['family'],
							'category' => $gfitem['category'],
							'variants' => $gfitem['variants'],
							'subsets' => $gfitem['subsets'],
						);							
						// _eo-review: should we sanitize the family name to save as key ?
						$google_font_faces_rebuilt[$gfitem['family']] = $lite_item;
					}
				}
				// if the file does not exist for some reason, try fetching from google webfont api
				else {
					$gwf_api_key = 'AIzaSyAMO-3ly3H7EJ801RbtRAQ2bkTqyVtGLoE';
					$gf_json_uri = 'https://www.googleapis.com/webfonts/v1/webfonts?key=' . $gwf_api_key;
					$gf_json = wp_remote_fopen($gf_json_uri);
					$has_error = array_key_exists('error',$gf_json_data);
					if( ! $has_error ) {
						$gf_json_data = json_decode($gf_json,true);
						
						
						//$google_font_faces = $gf_json_data['items'];
						foreach ( $gf_json_data['items'] as $gfitem ) {
							$lite_item = array(
								'family' => $gfitem['family'],
								'category' => $gfitem['category'],
								'variants' => $gfitem['variants'],
								'subsets' => $gfitem['subsets'],
							);							
							// _eo-review: should we sanitize the family name to save as key ?
							$google_font_faces_rebuilt[$gfitem['family']] = $lite_item;
						}
					}
					// If all fails - not very likely - 
					else {
						$err_reason = $gf_json_data["error"]["errors"][0]["reason"];
						$curruri = Nukleus_Generic::curruri();
						$redir_uri = add_query_arg(array('ske_error' => $err_reason,'ske_errid' => 'gwf_err'), $curruri);
						Nukleus_Generic::redir( $redir_uri, NULL, array('ske_error'=>'') );
						
						$google_font_faces_rebuilt = array(
							'Open Sans' => array(
								'family' => 'Open Sans',
								'category' => 'sans-serif',
								'variants' => array( "300", "300italic", "regular","italic","600","600italic","700","700italic","800","800italic"),
								'subsets' => array( "latin-ext", "cyrillic-ext", "latin", "greek-ext","cyrillic","greek","vietnamese"),
							),
							'Roboto' => array(
								'family' => 'Roboto',
								'category' => 'sans-serif',
								'variants' => array("100", "100italic", "300", "300italic", "regular", "italic", "500", "500italic", "700", "700italic", "900", "900italic"),
								'subsets' => array("latin-ext", "cyrillic-ext", "latin", "greek-ext", "cyrillic", "greek", "vietnamese"),
							),
						);
					}
					
				}
				$google_font_faces = $google_font_faces_rebuilt;
				// save google font faces as transient for convenience and performance. A week should suffice. alt: 7 * DAY_IN_SECONDS
				set_transient($gf_trans_name, $google_font_faces_rebuilt, 7 * 86400);
			}
			$font_source_names = array(
				'os_fonts' => 'Stock OS Fonts',
				'gwf_fonts' => 'Google Fonts'
			);
			$subset_names = array(
				"greek-ext" => 'Greek Extended',
			    "cyrillic" => 'Cyrillic',
			    'vietnamese' => 'Vietnamese',
			    'greek' => 'Greek',
			    'latin-ext' => 'Latin Extended',
			    'cyrillic-ext' => 'Cyrillic Extended',
			    'latin' => 'Latin',
				'arabic' => 'Arabic',
				'devanagari' => 'Devanagari',
				'hebrew' => 'Hebrew',
				'khmer' => 'Khmer',
				'tamil' => 'Tamil',
				'telugu' => 'Telugu',
				'thai' => 'Thai',
			);
			$combined_font_faces = array(
				'os_fonts' => $os_font_faces,
				'gwf_fonts' => $google_font_faces
			);
			
			/*
			$gfjson_trans = get_transient('ske_gwf_trans');
			if( ! $gfjson_trans ) {
				
				$gfjson_file = $vars['plug_uri'] . 'nukleus/xtra/gfjsondump.txt';
				//$gfjson = wp_remote_fopen($gfjson_file);
				
				// $gwf_api_key = 'AIzaSyAMO-3ly3H7EJ801RbtRAQ2bkTqyVtGLoE';
				// https://www.googleapis.com/webfonts/v1/webfonts?key=YOUR-API-KEY
				// $gf_json_uri = 'https://www.googleapis.com/webfonts/v1/webfonts?key=' . $gwf_api_key;
				$gf_json = wp_remote_fopen($gf_json_src);
				$gf_json_data = json_decode($gf_json,true);
				$has_error = array_key_exists('error',$gf_json_data);

				if( $has_error ) {
					$err_reason = $gf_json_data["error"]["errors"][0]["reason"];
					$curruri = Nukleus_Generic::curruri();
			//		var_dump($curruri);
					$redir_uri = add_query_arg(array('ske_error' => $err_reason,'ske_errid' => 'gwf_err'), $curruri);
				//	var_dump($redir_uri);
				//	$redir_uri = $curruri . '&ske_error='.$err_reason . '&ske_errid=gwf_err';
					Nukleus_Generic::redir( $redir_uri, NULL, array('ske_error'=>'') );
				}
			}*/


			//	var_dump($gf_json_data);

			//	require $vars['plug_dir'] . 'nukleus/xtra/gfdump.php';
			//	var_dump($def_gf_array);

			//	array_merge( self::combined_font_faces();

			$def_font_styles = array(
				'normal'      => 'Normal',
				'italic'      => 'Italic',
				'bold'        => 'Bold',
				'bold-italic' => 'Bold Italic',
			);
			
			$sizes = range( 9, 71 );
			$font_sizes = array_map( 'absint', $sizes );
			$font_styles = array(
				'normal'      => 'Normal',
				'italic'      => 'Italic',
				'bold'        => 'Bold',
				'bold-italic' => 'Bold Italic',
			);

			$typography_defaults = array(
			//	'source' => 'os_font',
				'face' => '',
				'style' => 'normal',
				'subset' => 'Latin',
				'size' => '14px',
				'color' => '',
			);

			$typography_stored = wp_parse_args( $value, $typography_defaults );
			


			//	var_dump($fonts_all);
			//	$all_fonts_sources[$all_font["font_src_slug"]] = $all_font["font_src_name"];
			
			$typography_options = array(
			//	'sources' => $font_sources,
				'faces' => $combined_font_faces,
				'subsets' => '',
				'variants' => array("regular" => "Regular"),
				'sizes' => $font_sizes,
				'styles' => $font_styles,
				'color' => true
			);

			if ( isset( $value['options'] ) ) {
				$typography_options = wp_parse_args( $value['options'], $typography_options );
			}
			
			/*		$font_source = '<select class="ske-typo-source" name="' . esc_attr( $args["name"] . '[source]' ) . '" id="' . esc_attr( $key ) . '-source' . '">';	
			$sources =	$typography_options['sources'];
			foreach ( $sources as $key => $source ) {
			//	$font_source .= '<option value="' . esc_attr( $key ) . '" ' . selected( $typography_stored['source'], $key, false ) . '>' . esc_html( $source ) . '</option>';
				$font_source .= '<option'.  selected( $typography_stored['source'], $key, false )  .' value="' . esc_attr( $key ) . '">' . esc_html( $source ) . '</option>';
			}
			$font_source .= '</select>';*/



			// Font Face
			//	$font_sources = $typography_options['sources'];
			$all_faces = $typography_options['faces'];

				// Keep the select as is if it's the stored font, else modify the id and hide
			//	($stored_s
				$font_face = '<select class="ske-typo-face" name="' . esc_attr( $args["name"] . '[face]' ) . '" id="' . esc_attr( $key ) . '_face' . '">';	
				$font_face .= '<option value=""> — Font Face —</option>';
				foreach ( $all_faces as $font_src_k => $font_faces ) {
					$fontsrcname = $font_source_names[$font_src_k];
					$disabled = '';
					$font_face .= '<optgroup label="'.$fontsrcname.'".'.$disabled.'>';
					foreach ( $font_faces as $face ) {
			//			var_dump($typography_stored['face'], $face['family']);
						$fflabel = (array_key_exists('label',$face) ) ? $face['label'] : $face['family'];
						$font_face .= '<option value="' . wp_kses_normalize_entities( $face['family'] ) . '" ' . selected( $typography_stored['face'], $face['family'], false ) . '>' . esc_html( $fflabel ) . '</option>';
					}
					$font_face .= '</optgroup>';
					
				}
			//		$font_face .= '<select class="of-typography of-typography-face '.$k.' '.$hiddclas.'" name="'. $hidd. esc_attr( $option_name . '[' . $value['id'] . '][face]' ) . '" id="'. $hidd. esc_attr( $value['id'] . '_face' ) . '">';
			/*					foreach ( $all_faces[$k] as $face ) {
					$fflabel = (array_key_exists('label',$face) ) ? $face['label'] : $face['family'];
					$font_face .= '<option value="' . wp_kses_normalize_entities( $face['family'] ) . '" ' . selected( $typography_stored['face'], $key, false ) . '>' . esc_html( $fflabel ) . '</option>';
				}*/
				$font_face .= '</select>';
				 
			//	var_dump($faces);
				

			// Font Styles
	
				// if this is a google font face, get specific styles for the font.
				$is_google_font = $typography_stored['face'] && array_key_exists($typography_stored['face'],$google_font_faces) ;
				$styles = ( $is_google_font ) ? $google_font_faces[$typography_stored['face']]['variants'] : $typography_options['styles'];
			//	$styles = ( $typography_stored['face'] && array_key_exists($typography_stored['face'],$google_font_faces) ) ? $google_font_faces[$typography_stored['face']]['variants'] : $typography_options['styles'];

			//		var_dump($google_font_faces);
			//				var_dump( in_array($typography_stored['face'],$google_font_faces) );
			//var_dump(array_search('Roboto',$google_font_faces) );
				// Display styles only for standart fonts, google has variants.
				$font_style = '<select class="ske-typo-style" name="' . esc_attr( $args["name"] . '[style]' ) . '" id="' . esc_attr( $key ) . '_style' . '">';	
				$font_style .= '<option value=""> — Style — </option>';
			//					$font_style = '<select class="of-typography of-typography-style'.$hiddclas.'" name="'.$option_name.'['.$value['id'].'][style]" id="'. $value['id'].'_style">';
				foreach ( $styles as $style ) {
					$font_style .= '<option value="' . esc_attr( $style ) . '" ' . selected( $typography_stored['style'], $style, false ) . '>' . esc_html( $style ) . '</option>';
				}
				$font_style .= '</select>';
				
				// Font subset 
				if( $is_google_font ) {
			//		$subsets = $google_font_faces[$typography_stored['face']]['subsets'];
					$facesubsets = $google_font_faces[$typography_stored['face']]['subsets'];

					$font_subset = '<select multiple="multiple" class="ske-typo-subset" name="' . esc_attr( $args["name"] . '[subset][]' ) . '" id="' . esc_attr( $key ) . '_subset' . '">';	
					$font_subset .= '<option value=""> — Subset — </option>';
					foreach ( $facesubsets as $subset ) {
						$is_selected = ! empty($typography_stored['subset']) && is_array($typography_stored['subset']) && in_array($subset,$typography_stored['subset']);
				//		var_dump(in_array($subset,$typography_stored['subset']));
			//	var_dump(selected( $is_selected, true, false ));
						$font_subset .= '<option value="' . esc_attr( $subset ) . '" ' . selected( $is_selected, true, false ) . '>' . esc_html( $subset_names[$subset] ) . '</option>';
					}
					$font_subset .= '</select>';
				}
			
			
			/*
			if ( $typography_options['variants'] ) {
				$ftg = $typography_stored['face'];
			//	var_dump( $value['id'],$typography_stored['variant'] );
				$tsrcs = $typography_stored['source'];
				( $tsrcs == "os_font") ? $hiddclas = ' hidv' : $hiddclas = '';
				if($typography_stored['face'] && $tsrcs != "os_font") {
					$variants = eo_get_gwf_variant($ftg);
				}
				else {
					$variants = $typography_options['variants'];
					
				}
					// Display styles only for standart fonts, google has variants.
					$font_variant = '<select class="of-typography of-typography-variant'.$hiddclas.'" name="'.$option_name.'['.$value['id'].'][variant]" id="'. $value['id'].'_variant">';
					// get the variant for the selected font if there's a stored val
					foreach ( $variants as $variant ) {
						$font_variant .= '<option value="' . esc_attr( $variant ) . '" ' . selected( $typography_stored['variant'], $variant, false ) . '>'. $variant .'</option>';
					}
					$font_variant .= '</select>';
			}*/
			
			// Font Size
			if ( $typography_options['sizes'] ) {
				$font_size = '<select class="ske-typo-size" name="' . esc_attr( $args["name"] . '[size]' ) . '" id="' . esc_attr( $key ) . '_size' . '">';	
				$font_size .= '<option value=""> — Size — </option>';

			//	$font_size = '<select class="of-typography of-typography-size" name="' . esc_attr( $option_name . '[' . $value['id'] . '][size]' ) . '" id="' . esc_attr( $value['id'] . '_size' ) . '">';
				$sizes = $typography_options['sizes'];
				foreach ( $sizes as $i ) {
					$size = $i . 'px';
					$font_size .= '<option value="' . esc_attr( $size ) . '" ' . selected( $typography_stored['size'], $size, false ) . '>' . esc_html( $size ) . '</option>';
				}
				$font_size .= '</select>';
			}
			
			// Font Color
			if ( $typography_options['color'] ) {
				//var_dump($typography_stored);
				$default_color = '';
				if ( isset( $value['value']['color'] ) ) {
					if ( $val !=  $value['value']['color'] )
						$default_color = ' data-default-color="' .$value['value']['color'] . '" ';
				}
				$font_color = '<input type="text" class="ske-typo-color input-colorpicker" name="' . esc_attr( $args["name"] . '[color]' ) . '" id="' . esc_attr( $key ) . '_color" placeholder="' . esc_attr( $args['placeholder'] ) . '" '.$args['maxlength'].' value="' . esc_attr( $typography_stored['color'] ) . '"' . $default_color .'  />';

			//	$font_color = '<input name="' . esc_attr( $option_name . '[' . $value['id'] . '][color]' ) . '" id="' . esc_attr( $value['id'] . '_color' ) . '" class="of-color of-typography-color  type="text" value="' . esc_attr( $typography_stored['color'] ) . '"' . $default_color .' />';
			}

			// Allow modification/injection of typography fields 
			$typography_fields = compact( 'font_face','font_style','font_size', 'font_color','font_subset' );
			//	var_dump($typography_fields);
			$typography_fields = apply_filters( 'ske_typography_fields', $typography_fields, $typography_stored, $setting_key, $value );
			//var_dump($typography_fields);
			$field .= implode( '', $typography_fields );
			$field .= $after;
			break;
			//		break;
			/*
			foreach ($args['options'] as $key => $option) {
				$selected = '';
				$label = $option;
				$option = preg_replace('/[^a-zA-Z0-9._\-]/', '', strtolower($key));
				$id = $option_name . '-' . $value['id'] . '-'. $option;
				$args["name"] = $option_name . '[' . $value['id'] . '][' . $option .']';
				
				if ( isset($val) && is_array($val) ) {
					if( array_key_exists( $key, $val)) {
						if($val[$key]) {
							$selected = ' selected="selected" ';
						}
					}
				}
	
				$field .= '<option'. $selected .' value="' . esc_attr( $key ) . '">' . esc_html( $label ) . '</option>';
				
			}*/
			$field .= '</select>';
			$field .= $after;
			break;
			
		case 'editor':
			$field = $before;
			$default_editor_settings = array(
				'textarea_name' => $args["name"],
				'media_buttons' => false,
			//	'tinymce' => array( 'plugins' => 'wordpress,wplink' )
			);
			$editor_settings = array();
			if ( isset( $args['options'] ) ) {
				$editor_settings = $args['options'];
			}
			$editor_settings = array_merge( $default_editor_settings, $editor_settings );
			
			$field .= wp_editor( $value, $key, $editor_settings );
			$field .= $after;
			break;
			
		case "gr_start" :
			$field = $before;	
			$field .= wp_kses_post($args['after']);
			break;
			
		case "gr_end" :
			$field = wp_kses_post($args['before']);
			$field = $after;
			break;
			
		case "togglebox_start" :
			$field = wp_kses_post($args['before']);	
			if( ! isset($key) ) $key = 'cbt_'.uniqid();
			$field .= '<input id="' . esc_attr( $key ) . '_cboxt" type="checkbox" ';
			if(!empty($value)) $field .= 'checked="checked" ';
			$field .= 'class="cbox_toggle_i" role="button"><label class="cbox_toggle '. esc_attr( implode( ' ', $args['label_class'] ) ) .'" for="' . esc_attr( $key ) . '_cboxt">';
			if($args['label']) {
				$field .= '<span class="btn btn-sm btn-success cbt_s">+ Show {' . esc_attr( $args['label'] ) . '}</span><span class="btn btn-sm btn-primary cbt_h">— Hide {' . esc_attr( $args['label'] ) . '}</span>';
			} else {
				$field .= '<span class="btn btn-sm btn-info cbt_s">i</span><span class="btn btn-sm btn-primary cbt_h">i</span>';
			}
			$field .='</label>';
			$field .= '<div class="cbt_cont togglebox">';
			$field .= wp_kses_post($args['after']);
			break;
		
		case "togglebox_end" :
		case "closediv" :
			$field = wp_kses_post($args['before']);
			$field .= '</div>';
			$field .= wp_kses_post($args['after']);
			break;
		
		case "hr" :
			$field = wp_kses_post($args['before']);
			$field .= '<hr class="' . esc_attr( implode( ' ', $args['class'] ) ) .'" />';
			$field .= wp_kses_post($args['after']);
			break;	
					
		case "free_html" :
			$field = $before;	
			$field .= $value;
			$field .= $after;	
			break;
	
		default :	
			$field = $before;
			ob_start(); ?>
			<input type="text" class="<?php echo esc_attr( implode( ' ', $args['class'] ) ) ?>" name="<?php echo esc_attr( $args['name'] ); ?>" id="<?php echo $key ?>"
            <?php foreach( $args["attr"] as $atk => $attr ) : ?>
            	<?php echo $atk ?>="<?php echo $attr ?>"
            <?php endforeach; ?>
            value="<?php echo esc_attr( $value ) ?>"
            <?php if( $args["required"] ) : ?>
            	required
            <?php endif; ?> />
			<?php
			$field .= $after;
			$field .= ob_get_clean();
			break;
		endswitch;
		
		$field = apply_filters( 'wpctint_gen_field_' . $args['type'], $field, $key, $args, $value );
		    //	$field =  preg_replace('/\r?\n\t*/','',$field);
		//$field = preg_replace('/\s+/', '', $field);
		if( $compress ) $field = self::sanitize_output( $field );
		
		if ( $r ) {
			return $field;
		}
		else {
			echo $field;
		}
	}

/*
Inp.Types: 
* text input
* textarea
* checkbox | checkbox (onoff)
* select
* radio buttons
* upload (image/file uploader)
* images (use images instead of radio buttons)
* background (a set of options to define a background)
* multicheck
* multiselect
* colorpicker
* datepicker
* typography (a set of options to define typography)
* editor
* password

Interface elements
* gr_start, gr_end
* togglebox_start, togglebox_end
* closediv | </div> closer
* hr | <hr />
* Free_html | non-sanitized, return as is.
*/	
}
?>
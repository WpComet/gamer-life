<?php
/**
 * Theme Settings Setup
 * @author   WpComet
 * @package  WPCT/Classes
 * @version  1.0.0
 */

if ( ! defined( 'ABSPATH' ) )	exit;

if ( ! class_exists( 'WPCT_Update' ) ) :

class WPCT_Update {
	public static $settings;
	public static $suggestions;
	
	public static function init() {
		//add_action( 'after_switch_theme', array( __CLASS__, 'update' ) );
		//add_action( 'admin_init', array( __CLASS__, 'update' ) );
		//add_action( 'init', array( __CLASS__, 'update' ) );
	}
	
	static function update($mode="live") {
		$bsul = wp_get_theme(get_template());
		$bsulv = $bsul->get( 'Version' );
		$bsul_options = get_option("bootstrap_ultimate");
		
		$options_obs_arr = array(
			'branding_logo' => 'logo',
			'favicon_url' => 'favicon',
			'main_layout' => 'layout_global',
			//'branding_logo' => 'logo',
			'loop_whtd' => 'loop_ptypes',
		);
		
		foreach ( $bsul_options as $option_obs ) {
			var_dump(  $option_obs );
		}
	
		wpct_vd(  $bsul_options );
		return true;
		die("1234");
		$current_db_version = get_option( 'woocommerce_db_version' );
		$logger             = new WC_Logger();
		$update_queued      = false;
	
		foreach ( self::$db_updates as $version => $update_callbacks ) {
			if ( version_compare( $current_db_version, $version, '<' ) ) {
				foreach ( $update_callbacks as $update_callback ) {
					$logger->add( 'wc_db_updates', sprintf( 'Queuing %s - %s', $version, $update_callback ) );
					self::$background_updater->push_to_queue( $update_callback );
					$update_queued = true;
				}
			}
		}
	
		if ( $update_queued ) {
			self::$background_updater->save()->dispatch();
		}
	}
	function bsul40_migrate_option_main_layout($v) {
		/*$cont_v = ($v='full') ? $
		$set = array(
			'container' => $cont_v
		);
		set_theme_mod('');*/
		set_theme_mod( '' );
	}
}
endif;
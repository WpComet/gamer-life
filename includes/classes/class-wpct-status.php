<?php
/**
 * Theme Status change ( Activation - deactivation ) related functions and actions.
 * @author   WpComet
 * @category Admin
 * @package  WPCT/Classes 
 * @version  1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'WPCT_Status' ) ) :

class WPCT_Status {
	public static function init() {
		// admin notice
		add_action( 'admin_notices', array( __CLASS__, 'activation_admin_notice' ) );
			
		// create dashbord page
		add_action( 'admin_menu', array( __CLASS__, 'welcome_register_menu' ) );
		// welcome page css
		add_action( 'admin_enqueue_scripts', array( __CLASS__, 'css_activation' ) );
		//add_action( 'admin_head-appearance_page_wpct-welcome', array( __CLASS__, 'inline_styles') );
		
	}
	
	public static function activate() {
		self::activation_admin_notice();
	}
	
	public static function activation_admin_notice() {
		global $pagenow;
		if ( isset( $_GET['activated'] ) && 'themes.php' == $pagenow ) {
	
			add_action( 'admin_notices', array( __CLASS__, 'welcome_admin_notice' ), 99 );
		}
	}

	function inline_styles() {
		/*	echo '<style type="text/css">
			</style>'; */
	}

	function css_activation($hook)
	{
		// $pagenow, is a global variable referring to the filename of the current page, 
		// such as ‘admin.php’, ‘post-new.php’
		//	global $pagenow;
		//	die(var_dump($hook));
	 
		if ($hook != 'appearance_page_wpct-welcome') {
			return false;
		}
		 
		// loading css
		wp_enqueue_style( 'wpct-activation',  get_template_directory_uri() . '/assets/css/admin/wpct-activation.css', array(), '', 'all' );
	}
	 
	
	/**
	 * Display an admin notice linking to the about page
	 */
	public static function welcome_admin_notice() {
		//var_dump( wpct_theme_get("slug"));
		global $pagenow;
		$query['autofocus[section]'] = 'wpcomet_theme_options';
		$customizer_uri =  add_query_arg( $query, admin_url( 'customize.php' ) );
		$welcome_uri = esc_url( admin_url( 'themes.php?page=wpct-welcome' ) );
		//	var_dump( wpct_get_theme('Name') );
		$notification = '<p>' . sprintf( 'Welcome and thank you for choosing %1$s %3$s.',wpct_get_theme('Name'), 
		'<a href="' . esc_url( $customizer_uri ) . '">', '</a>' ) . '</p>
		<p><a href="' . esc_url( $welcome_uri ) . '" class="button button-primary" style="text-decoration: none;">'
		 . sprintf( 'Introduction' ) . '</a>
		 <a href="' . esc_url( $customizer_uri ) . '" class="button button-secondary" style="text-decoration: none;">'
		 . sprintf( 'Start customizing' ) . '</a>
		 </p>';
	
		// display notice if not previously dismissed
		if ( current_user_can( 'edit_theme_options' ) 
		// && !get_option( 'hide_welcome_notice' ) 
		&& 'themes.php' == $pagenow ) : ?>

			<div class="updated notice is-dismissible">
			<a class="notice-dismiss" href="<?php echo esc_url(	wp_nonce_url(
				remove_query_arg(array('activated'), add_query_arg('wpct-hide-notice', 'welcome')), 'wpct_hide_notices_nonce', '_wpct_notice_nonce'
				)) ?>" style="z-index: 0;padding: 10px;text-decoration: none;" >
			<span class="screen-reader-text"><?php echo esc_html__('Dismiss this notice.','gamer-life') ?></span>
			</a>
			<?php echo wp_kses_post( $notification ); ?>
			</div>
		
		<?php endif; 
	}
	
	/**
	 * Creates the dashboard page
	 * @see  add_theme_page()
	 * @since 1.0.0
	 */
	public static function welcome_register_menu() {
		add_theme_page( 'About ' . wpct_get_theme('Name') , 'About ' . wpct_get_theme('Name'), 'edit_theme_options', 'wpct-welcome', array( __CLASS__, '_intro' ) );
	}
	
	/**
	 * Generic intro header for each of the screens.
	 * @since   1.0.0
	 * @return  void
	 */
	static function _intro () {
		global $wpcomet_theme;
		?>
        <div class="wrap about-wrap wf-wrap">
            <div id="current-theme" class="has-screenshot">
                <img class="theme-screenshot" src="<?php echo $wpcomet_theme->assets_uri . '/img/screenshot-mini.png' ?>" alt="<?php esc_attr_e( 'Current theme preview', 'gamer-life' ); ?>" />
            <h1><?php echo sprintf( __( 'Welcome to %s', 'gamer-life' ), '<span class="theme-name">' . $wpcomet_themewpct->name . '</span>' ); ?></h1>
            <div>
        </div>
	<?php }
}
endif;
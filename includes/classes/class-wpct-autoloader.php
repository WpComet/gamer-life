<?php
if ( ! defined( 'ABSPATH' ) )	exit;
/**
 * WpComet Theme Autoloader.
 *
 * @class 		WPCT_Autoloader
 * @version		1.0.0
 * @package		WPCT/Common
 * @category	Class
 * @author 		WpComet
 */

class WPCT_Autoloader {
	private $include_path = '';

	/**
	 * The Constructor.
	 */
	public function __construct() {
		//	global $wpcomet_theme;
		//	var_dump( $wpcomet_theme );
		$this->include_path =  apply_filters( 'wpcomet_theme_includes',	untrailingslashit(  get_template_directory() ) . '/includes/' );
		if ( function_exists( "__autoload" ) ) {
			spl_autoload_register( "__autoload" );
		}
		spl_autoload_register( array( $this, 'autoload' ) );
	}

	/**
	 * Take a class name and turn it into a file name.
	 *
	 * @param  string $class
	 * @return string
	 */
	private function get_file_name_from_class( $class ) {
		$class_stl = strtolower( $class );		
		$classname = 'class-' . str_replace( '_', '-', $class_stl ) . '.php';
		//	var_dump( $classname );
		return apply_filters( 'wpct_get_file_name_from_class', $classname, $class );
	}
	
	/**
	 * Take a class name and turn it into a file name.
	 *
	 * @param  string $class
	 * @return string
	 */
	private function get_file_name_control( $class ) {
		$class_stl = strtolower( $class );		
		//var_dump( $class_stl );
		$classname = 'class-' . str_replace( array('_','wpctcc-'), array('-',''), $class_stl ) . '.php';
		//	var_dump( $classname );
		return apply_filters( 'wpct_get_file_name_control', $classname, $class );
	}
	
	/**
	 * Take a class name and turn it into a file name.
	 *
	 * @param  string $class
	 * @return string
	 */
	private function get_file_name_widget( $widgetname,$class ) {
		//$class_stl = strtolower( $class );		
		//var_dump( $class_stl );
		//$classname = 'class-' . str_replace( array('_','wpctcc-'), array('-',''), $class_stl ) . '.php';
		//	var_dump( $classname );
		//return apply_filters( 'wpct_get_file_name_widget', $widgetname, $classname, $class );
		$filename = $widgetname . '.php';
		return apply_filters( 'wpct_get_file_name_widget', $filename, $widgetname, $class );
	}

	/**
	 * Include a class file.
	 *
	 * @param  string $path
	 * @return bool successful or not
	 */
	private function load_file( $file ) {
		if ( $file && is_readable( $file ) ) {
			include_once( $file );
			return true;
		}
		return false;
	}

	/**
	 * Auto-load WC classes on demand to reduce memory consumption.
	 *
	 * @param string $class
	 */
	public function autoload( $class ) {
		do_action( "wpct_before_autoload" );
		if( strpos( $class, 'WPCT' ) !== 0  ) return;
		//	var_dump( $class );
		$file  = $this->get_file_name_from_class( $class );
		//	var_dump( $this->include_path . $file  );

		$path = $this->include_path;

		if( strpos($class,'Pro') ) {
			
			$path = $path . 'pro/';
			$file = str_replace('pro-','',$file);
			
			//	var_dump( $path . $file );
		}
		elseif( strpos( $class, 'WPCT_Widget_' ) === 0 ) {
			$widgetname = strtolower( str_replace( "WPCT_Widget_", "", $class ) );
			$file  = $this->get_file_name_widget( $widgetname, $class );
			$path = ( is_dir($path . $widgetname) ) ? $path . $widgetname : $this->include_path . 'widgets/';
			//	var_dump( is_readable(  $path . $file )  );
			//	var_dump( $path . $file );
		} 
		elseif ( strpos( $class, 'WPCT_Customize' ) === 0 )  {
			
			$path = $this->include_path . 'customizer/';
			//	var_dump( $path . $file   );
		}
		elseif ( strpos( $class, 'WPCTCC_' ) === 0 )  {
			
			$path = $this->include_path . 'customizer/controls/';
			$file  = $this->get_file_name_control( $class );
			//	var_dump( $path . $file   );
		}
		else {
			$path = $this->include_path . 'classes/';
		}
		
		if ( empty( $path ) || ( ! $this->load_file( $path . $file ) && strpos( $class, 'WPCT_' ) === 0 ) ) {
			$this->load_file( $path . $file );
		}
	}
}

new WPCT_Autoloader();

<?php $google_font_faces = array(
	array(
		'family' => 'Open Sans',
		'category' => 'sans-serif',
		'variants' => array( "300", "300italic", "regular","italic","600","600italic","700","700italic","800","800italic"),
		'subsets' => array( "latin-ext", "cyrillic-ext", "latin", "greek-ext","cyrillic","greek","vietnamese"),
	),
	array(
		'family' => 'Roboto',
		'category' => 'sans-serif',
		'variants' => array("100", "100italic", "300", "300italic", "regular", "italic", "500", "500italic", "700", "700italic", "900", "900italic"),
		'subsets' => array("latin-ext", "cyrillic-ext", "latin", "greek-ext", "cyrillic", "greek", "vietnamese"),
	),
	array(
		'family' => 'Lato',
		'category' => 'sans-serif',
		'variants' => array("100", "100italic", "300", "300italic", "regular", "italic", "700", "700italic", "900", "900italic"),
		'subsets' => array("latin-ext", "latin")
	),
	array(
		'family' => 'Oswald',
		'category' => 'sans-serif',
		'variants' => array("300", "regular", "700"),
		'subsets' => array("latin-ext", "latin")
	),
	array(
		'family' => 'Roboto Condensed',
		'category' => 'sans-serif',
		'variants' => array("300", "300italic", "regular", "italic", "700", "700italic"),
		'subsets' => array("latin-ext", "cyrillic-ext", "latin", "greek-ext", "cyrillic", "greek", "vietnamese")
	),
	array(
		'family' => 'Lora',
		'category' => 'serif',
		'variants' => array("regular", "italic", "700", "700italic"),
		'subsets' => array("latin-ext", "latin", "cyrillic")
	),
	array(
		'family' => 'Source Sans Pro',
		'category' => 'sans-serif',
		'variants' => array("200", "200italic", "300", "300italic", "regular", "italic", "600", "600italic", "700", "700italic", "900", "900italic"),
		'subsets' => array("latin-ext", "latin", "vietnamese")
	),
	array(
		'family' => 'PT Sans',
		'category' => 'sans-serif',
		'variants' => array("regular", "italic", "700", "700italic"),
		'subsets' => array("latin-ext", "cyrillic-ext", "latin", "cyrillic")
	),
	array(
		'family' => 'Open Sans Condensed',
		'category' => 'sans-serif',
		'variants' => array("300", "300italic", "700"),
		'subsets' => array("latin-ext", "cyrillic-ext", "latin", "greek-ext", "cyrillic", "greek", "vietnamese")
	),
	array(
		'family' => 'Montserrat',
		'category' => 'sans-serif',
		'variants' => array("regular", "700"),
		'subsets' => array("latin")
	),
	array(
		'family' => 'Raleway',
		'category' => 'sans-serif',
		'variants' => array("100", "200", "300", "regular", "500", "600", "700", "800", "900"),
		'subsets' => array("latin")
	),
	array(
		'family' => 'Droid Sans',
		'category' => 'sans-serif',
		'variants' => array("regular", "700"),
		'subsets' => array("latin")
	),
	array(
		'family' => 'Ubuntu',
		'category' => 'sans-serif',
		'variants' => array("300", "300italic", "regular", "italic", "500", "500italic", "700", "700italic"),
		'subsets' => array("latin-ext", "cyrillic-ext", "latin", "greek-ext", "cyrillic", "greek")
	),
	array(
		'family' => 'Droid Serif',
		'category' => 'serif',
		'variants' => array("regular", "italic", "700", "700italic"),
		'subsets' => array("latin")
	),
	array(
		'family' => 'Roboto Slab',
		'category' => 'serif',
		'variants' => array("100", "300", "regular", "700"),
		'subsets' => array("latin-ext", "cyrillic-ext", "latin", "greek-ext", "cyrillic", "greek", "vietnamese")
	),
	array(
		'family' => 'Merriweather',
		'category' => 'serif',
		'variants' => array("300", "300italic", "regular", "italic", "700", "700italic", "900", "900italic"),
		'subsets' => array("latin-ext", "latin")
	),
	array(
		'family' => 'Lobster',
		'category' => 'display',
		'variants' => array("regular"),
		'subsets' => array("latin-ext", "latin", "cyrillic", "vietnamese")
	),
	array(
		'family' => 'Righteous',
		'category' => 'display',
		'variants' => array("regular"),
		'subsets' => array("latin-ext", "latin", "cyrillic", "vietnamese")
	),
	array(
		'family' => 'Indie Flower',
		'category' => 'handwriting',
		'variants' => array("regular"),
		'subsets' => array("latin")
	),
	array(
		'family' => 'Indie Flower',
		'category' => 'handwriting',
		'variants' => array("regular"),
		'subsets' => array("latin")
	),
	array(
		'family' => 'Shadows Into Light',
		'category' => 'handwriting',
		'variants' => array("regular"),
		'subsets' => array("latin")
	),
	array(
		'family' => 'Architects Daughter',
		'category' => 'handwriting',
		'variants' => array("regular"),
		'subsets' => array("latin")
	),
	array(
		'family' => 'Inconsolata',
		'category' => 'monospace',
		'variants' => array("regular","700"),
		'subsets' => array("latin","latin-ext")
	),
	array(
		'family' => 'Source Code Pro',
		'category' => 'monospace',
		'variants' => array("200", "300", "regular", "500", "600", "700", "900"),
		'subsets' => array("latin","latin-ext")
	),			
);
<?php return array(
	array(
		'family' => 'Helvetica Neue',
		'alternates' => 'Helvetica, Arial',
		'label' => 'Helvetica/Arial',
		'category' => 'sans-serif',
	),
	array(
		'family' => 'Palatino Linotype',
		'alternates' => 'Book Antiqua, Palatino',
		'label' => 'Palatino/Book',
		'category' => 'serif',
	),
	array(
		'family' => 'Verdana',
		'alternates' => 'Verdana, Geneva',
		'label' => 'Verdana/Geneva',
		'category' => 'sans-serif',
	),
	array(
		'family' => 'Lucida Grande',
		'alternates' => 'Lucida Sans Unicode',
		'label' => 'Lucida Grande',
		'category' => 'sans-serif',
	),
	array(
		'family' => 'Trebuchet',
		'alternates' => 'Trebuchet MS',
		'label' => 'Trebuchet',
		'category' => 'sans-serif',
	),
	array(
		'family' => 'Times New Roman',
		'alternates' => 'Times New Roman, Times',
		'label' => 'Times New Roman',
		'category' => 'serif',
	),
	array(
		'family' => 'Georgia',
		'alternates' => 'Georgia',
		'label' => 'Georgia',
		'category' => 'serif',
	),
	array(
		'family' => 'Tahoma',
		'alternates' => 'Geneva',
		'label' => 'Tahoma/Geneva',
		'category' => 'sans-serif',
	),
	array(
		'family' => 'Courier New',
		'alternates' => 'Courier New',
		'label' => 'Courier New',
		'category' => 'monospace',
	),
	array(
		'family' => 'Courier New',
		'alternates' => 'Courier New',
		'label' => 'Courier New',
		'category' => 'monospace',
	),
	array(
		'family' => 'MS Sans Serif',
		'alternates' => 'Geneva',
		'label' => 'MS Sans Serif/Geneva',
		'category' => 'sans-serif',
	),
	array(
		'family' => 'MS Serif',
		'alternates' => 'New York',
		'label' => 'MS Serif/New York',
		'category' => 'serif',
	),
	array(
		'family' => 'Lucida Console',
		'alternates' => 'Monaco',
		'label' => 'Lucida Console/Monaco',
		'category' => 'monospace',
	),					
	// 'Impact, Impact, Charcoal, sans-serif' => 'Impact', // Not sure why would anyone want this one, but maybe for headings...
	// 'Arial Black, Gadget, sans-serif' => 'Arial Black' // Bah ! Who'd use this anyway ? 
	// 'Comic Sans MS, Comic Sans MS, cursive' //  NO!.. just no! 
);
<?php
/**
 * Theme Plugin Compatibility
 * @author   WpComet
 * @version  1.0.0
 */
 
add_action( 'pre_get_posts', 'wpct_query_mod' );
function wpct_query_mod( $query ) {
	global $komet_options;
		$loop_ptypes = wpct_get( 'loop_ptypes' );
		if ( $loop_ptypes ) return false;

	if ( array_key_exists("loop_whtd",$komet_options) ) return;
	$wtd = $komet_options['loop_whtd'];
	if(! empty($wtd) && $query->is_main_query() && !is_admin() && ! is_singular() && ! is_404()) {
		$cu_post_type = get_query_var( 'post_type' );
		//var_dump($my_post_type);
		
		if ( empty( $my_post_type ) ) {
			$types = array();
			foreach ( $wtd as $typ => $v) {
				if($v == "1")	$types[] = $typ;
			}
			if(empty($types)) $types = array("post");
			if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
				if( is_woocommerce() || is_cart() || is_checkout() ) $types = array("product");	
			}
			//var_dump($less_rsrcs_arr);
			$args = array(
				'exclude_from_search' => false,
				'public' => true,
				'_builtin' => false
			);
			$output = 'names';
			$operator = 'and';
			//$post_types = array_merge( $post_types, array( 'post' ) );
			$query->set( 'post_type', $types );		
		}	
	}
}

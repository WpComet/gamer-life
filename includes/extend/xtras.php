<?php
/**
 * @author   WpComet
 * @version  1.0.0
 */
 
 add_filter('body_class', 'wpct_body_class');

/**
 * Adds body classes for special theme features.
 * @since 1.0.0
 * @param $classes
 * @return array
 */
function wpct_body_class( $classes ) {
	$mods = wpct_get_mods();
	$sidebar_pref = wpct_get_layout_el("sidebar");
	
	//var_dump( $sidebar_pref );
	//var_dump( wpct_get_layout_el("sidebar") );
	//	var_dump( $mods );
	//		var_dump( wpct_get_layout_el( 'primary_sidebar_style' ) );
	// ' . $mods["header_style"] . '
	
	$classes[] = 'app';
	if( 'nosb' == $sidebar_pref ) $class[] = 'sidebars-' . wpct_get_layout_el("sidebar") . ' sidebar-' . wpct_get_layout_el( 'primary_sidebar_style' );
//	if( $mods["header_style"]; == "fixed" )$classes[] = 'header-fixed';
	//	"app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden"
	//	$classes[] = themify_theme_fixed_header() ? 'fixed-header' : 'no-fixed-header';
	
	return $classes;
}

function wpct_image_tag_class($class) {
    $class .= ' img-fluid';
    return $class;
}
add_filter('get_image_tag_class', 'wpct_image_tag_class' );
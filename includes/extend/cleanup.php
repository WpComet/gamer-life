<?php
/* Remove unnecessary stuff.
 * @version 1.0
 * @since 4.0
*/

add_action( 'init', 'wpct_head_cleanup' );
add_filter( 'wp_head', 'wpct_remove_wp_widget_recent_comments_style', 1 );
add_action( 'wp_head', 'wpct_remove_recent_comments_style', 1 );
add_filter( 'the_generator', 'wpct_rss_version' );
add_filter( 'login_errors', 'wpct_show_less_login_info' );
add_filter( 'style_loader_src', 'wpct_remove_wp_ver_css_js', 9999);
add_filter( 'script_loader_src', 'wpct_remove_wp_ver_css_js', 9999);
add_filter( 'gallery_style', 'wpct_gallery_style' );
add_filter( 'the_content_more_link', 'wpct_remove_more_jump_link' );
add_filter( 'widget_tag_cloud_args', 'wpct_widget_tag_cloud_args' );

// Enable shortcodes in widgets
add_filter( 'widget_text', 'do_shortcode' );

//	Clean up wp_head() from unused or unsecure stuff
function wpct_head_cleanup() {
	// category feeds
	 remove_action( 'wp_head', 'feed_links_extra', 3 );
	// post and comment feeds
	 remove_action( 'wp_head', 'feed_links', 2 );
	// EditURI link
	remove_action( 'wp_head', 'rsd_link' );
	// windows live writer
	remove_action( 'wp_head', 'wlwmanifest_link' );
	// previous link
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
	// start link
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
	// links for adjacent posts
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
	// rel=shortlink in the head
	remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
	// WP version
	remove_action( 'wp_head', 'wp_generator' );
	// remove WP version from css
	add_filter( 'style_loader_src', 'wpct_remove_wp_ver_css_js', 9999 );
	// remove Wp version from scripts
	add_filter( 'script_loader_src', 'wpct_remove_wp_ver_css_js', 9999 );
}

// remove WP version from RSS
function wpct_rss_version() { return ''; }


/**
 * Show less info to users on failed login for security.
 * (Will not let a valid username be known.)
 *
 * @return string
 */
function wpct_show_less_login_info() {
	return '<strong>ERROR</strong>: Stop guessing!';
}

// remove WP version from scripts
function wpct_remove_wp_ver_css_js( $src ) {
	if ( strpos( $src, 'ver=' ) )
		$src = remove_query_arg( 'ver', $src );
	return $src;
}


// remove injected CSS for recent comments widget
function wpct_remove_wp_widget_recent_comments_style() {
	if ( has_filter( 'wp_head', 'wp_widget_recent_comments_style' ) ) {
		remove_filter( 'wp_head', 'wp_widget_recent_comments_style' );
	}
}

// remove injected CSS from recent comments widget
function wpct_remove_recent_comments_style() {
	global $wp_widget_factory;
	if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
		remove_action( 'wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style') );
	}
}

// Disable jump in 'read more' link
function wpct_remove_more_jump_link( $link ) {
	$offset = strpos($link, '#more-');
	if ( $offset ) {
		$end = strpos( $link, '"',$offset );
	}
	if ( $end ) {
		$link = substr_replace( $link, '', $offset, $end-$offset );
	}
	return $link;
}

// remove injected CSS from gallery
function wpct_gallery_style($css) {
	return preg_replace( "!<style type='text/css'>(.*?)</style>!s", '', $css );
}
?>
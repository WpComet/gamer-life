<?php if ( ! defined( 'ABSPATH' ) ) exit;	// File Security Check
//wp_register_script( $handle, $src, $deps, $ver, $in_footer ); 
function wpct_regenq_libjs($ln,$fn,$dep,$lv,$inf=true) {
	wp_register_script(
		$ln,
		get_template_directory_uri() . '/lib/'.$ln.'/'.$fn.'.js',
		$dep,
		$lv,
		$inf
	);
	wp_enqueue_script($ln);
}

/* 
* REGISTER & ENQUEUE STYLES 
* wp_register_style( $handle, $src, $deps, $ver, $media );
* load regular way or via less
*/
function wpct_theme_js(){
	// Get option to load which js files to load
//	$load_js_files = wpct_get_opt('load_js_files');
//	var_dump($load_js_files); return;
	
	/* LOAD singular JSes*/
	if( wpct_get_opt('use_less') != "1" ) {
		wp_register_script( 'less_js', get_template_directory_uri() . '/rsc/js/less.min.js', array(), '2.5.0', false );
		wp_enqueue_script('less_js');
	}
	if(wpct_get_opt( 'use_inf_scroll') == "1" ) 	 {
		wp_register_script( 'infinite-scroll',  get_template_directory_uri() . '/rsc/js/jquery.infinitescroll.min.js', array('jquery'), '2.1.0', true );
		wp_enqueue_script('infinite-scroll');	  
	}
	/* LOAD LIBS */
	// Get libraries
	$rlibs = wpct_getreg_libs();
	foreach( $rlibs as $ln => $largs) {
		// Check if we want to use the lib.
		$loadlib = !empty($load_css_arr) && $load_css_arr[$ln] != "0" && ! wpct_get_opt('use_'.$ln,true);
		if($loadlib) {
			$dep = ( array_key_exists('dep',$largs) && is_array($largs['dep']) ) ? $largs['dep'] : array();
			$inf = ( array_key_exists('inf',$largs) ) ? $largs['inf'] : true;
			// load single / multiple javascript file(s) for the library if js arg is array or str.
			if( ! is_array($largs['js']) ) {
				wpct_regenq_libjs($ln,$largs['js'],$dep,$largs['v'],$inf);
			}
			else {
				foreach ( $largs['js'] as $libjs ) {
					wpct_regenq_libjs($ln . '-' . $libjs,$libjs,$dep,$largs['v'],$inf);
				}
			}
		}
	}

 
	if( wpct_get_opt( 'bs_js_seperate') ) {
		$seperate_bs_jses = wpct_get_opt('bs_js_seperate');
		if( !empty($seperate_bs_jses) && is_array($seperate_bs_jses) && wpct_get_opt( 'load_bs_fe') != "1" ) {
			$sp_bsjs_arr = array();
			foreach ( $seperate_bs_jses as $seperate_bs_js => $v) {
				if($v == "1")	$sp_bsjs_arr[] = $seperate_bs_js;
			}
			foreach( $sp_bsjs_arr as $sp_bsjs ) {
				wp_register_script( $sp_bsjs,  get_template_directory_uri() . '/lib/bootstrap/js/'.$sp_bsjs.'.js', array(), '3.0.1', true );
				wp_enqueue_script($sp_bsjs);	
			}
		}
	}
	 if(wpct_get_opt( 'use_lightbox') == "1" ) {
	 	wp_register_script( 'colorbox',  get_template_directory_uri() . '/lib/colorbox/jquery.colorbox.js', array('jquery'), '1.4.33', true );	  
 		wp_enqueue_script('colorbox');
 	 }
	 
	 if( wpct_get_opt('use_chosen_fe') == "cl_chosen" || wpct_get_opt('use_chosen_fe') == "all"  ) {
		wp_register_script( 'chosen-fe', get_template_directory_uri() . '/lib/chosen/chosen.jquery.min.js', array('jquery') );
		wp_enqueue_script( 'chosen-fe' );
		
		wp_register_style( 'chosen-fe', get_template_directory_uri() . '/lib/chosen/chosen.min.css', array(), '3.0.1', 'all' );
		wp_enqueue_style( 'chosen-fe' );
	}
	wp_register_script(  'modernizr', get_template_directory_uri() . '/rsc/js/modernizr.custom.min.js',  array('jquery'),'2.8.3' );
	wp_enqueue_script('modernizr');
	 
/*  	 if(wpct_get_opt( 'use_placeholder') == "1" ) 	 {
	 	wp_register_script( 'placeholder',  get_template_directory_uri() . '/rsc/js/holder.js', array(), '2.0', true );	  
 		wp_enqueue_script('placeholder');
 	 }*/
  
	wp_register_script( 'eot-scripts', get_template_directory_uri() . '/rsc/js/scripts.js', array('jquery'), '1.0', true );
	wp_enqueue_script('eot-scripts');
}

function eo_infinite_scroll_js() {
	global $wpct_options;
	if ( ( $wpct_options['inf_scroll'] == '1' ) && ( is_home() || is_archive() ) ) { ?>
	<script>
	if ( ! navigator.userAgent.match(/(iPod|iPhone|iPad|Android)/)) {
		var infinite_scroll = {
			loading: {
				img: "<?php echo get_template_directory_uri(); ?>/panel/rsc/img/loading.gif",
				msgText: "<?php _e( 'Loading the next set of posts...', 'eo_theme' ); ?>",
				finishedMsg: "<?php _e( 'All posts loaded.', 'eo_theme' ); ?>"
			},
			"nextSelector":".pagination a.next",
			"navSelector":".pagination",
			"itemSelector":"#main .post",
			"contentSelector":"#main .postshold"
		};
		jQuery( infinite_scroll.contentSelector ).infinitescroll( infinite_scroll );
	}
	</script>
	<?php
	}
}

function wpct_inline_js_per_post(){
	if(!is_singular()) return;
	global $post;
	$post_inline_js = get_post_meta($post->ID,'_eo_cust_post_js',true);
	
	if( $post_inline_js && !empty($post_inline_js) ) {
		// Remove script tags
		$post_inline_js = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $post_inline_js);
		// inject js
		 echo '<script type="text/javascript">'
        . $post_inline_js . '
        </script>';
	}
}

function eo_front_js_runtime() {
?>
	<script type="text/javascript">
    jQuery(document).ready(function($) {     
		<?php if( wpct_get_opt('nav_select_menu',true) ) { ?>
		$(function(){
			$('input.ms_gobut').remove();
			$(".eo-mobile-select-nav").removeClass().addClass("form-control eo-mobile-select-nav").parent().removeClass().addClass("col-xs-12");
		
		  // bind change event to select
		  $('.eo-mobile-select-nav').bind('change', function () {
			  var url = $(this).val(); // get selected value
			  if (url) { // require a URL
				  window.location = url; // redirect
			  }
			  return false;
		  });
		});
		<?php } ?>
		<?php if( wpct_get_opt( 'use_chosen_fe' ) == "cl_chosen" ) { ?>
				$("select.chosen").chosen({disable_search_threshold: 6});
		<?php }
			else if (wpct_get_opt( 'use_chosen_fe' ) == "all") { ?>
				$("select").chosen({disable_search_threshold: 6});
		<?php } ?>
		<?php if( wpct_get_opt( 'use_lightbox' ) == "1" ) { ?>
        $('a.cboxElement').colorbox({maxWidth:'95%', maxHeight:'95%'});
        $("a.cboxElement.iframe").colorbox({iframe:true, width:"80%", height:"80%",maxWidth:'95%', maxHeight:'95%'});
        $("a.cboxElement.inline").colorbox({inline:true, width:"80%",maxWidth:'95%', maxHeight:'95%'});
		// Colorbox resize function - http://stackoverflow.com/questions/12182668/how-to-make-colorbox-responsive 
		var resizeTimer;
		function resizeColorBox()
		{
			if (resizeTimer) clearTimeout(resizeTimer);
			resizeTimer = setTimeout(function() {
					if (jQuery('#cboxOverlay').is(':visible')) {
							jQuery.colorbox.load(true);
					}
			}, 300)
		}
		// Resize Colorbox when resizing window or changing mobile device orientation
		jQuery(window).resize(resizeColorBox);
		window.addEventListener("orientationchange", resizeColorBox, false);
		<?php } ?>    
     
        // check input & selects for default bootstrap3 class .form-control
        $("input:not([type='checkbox']),select,textarea").each(function(index, element) {
            if(!$(this).hasClass("form-control") ) {
                $(this).addClass("form-control");
            }
        });
        $("form").each(function(index, element) {
            if(!$(this).hasClass("form-inline") ) {
                $(this).addClass("form-inline");
            }
        });
    });
    </script>
 <?php if( wpct_get_opt( 'eo_custom_footer_js' )  ) { ?>
 <script type="text/javascript" >
 <?php	 echo wpct_get_opt( 'eo_custom_footer_js' ); ?>
 </script>
 <?php } // emd custom_footer_js?>
<?php
}

?>
<?php if ( ! defined( 'ABSPATH' ) ) exit; // File Security Check
function wpct_remove_dimensions_thumbmarkup( $html ) {
    $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
    return $html;
}
function wpct_add_class_attachment_link( $html ) {
    $postid = get_the_ID();
    $html = str_replace( '<a','<a class="thumbnail"',$html );
    return $html;
}
/************* THUMBNAIL SIZE OPTIONS *************/
// Thumbnail sizes
add_image_size( 'eo-featurette', 350, 290, true );
//add_image_size( 'eo-highlight', 140, 140, true);
//add_image_size( 'eo-carousel', 970, 360, true);
?>
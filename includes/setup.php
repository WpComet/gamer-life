<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
 
/* BASIC SETUP
* @vesion 1.0
* @since 4.0
* 1- Set up & register theme setting / option(s).
* 2- Perform updates upon activation.
* 3- Set up theme environment.
*/

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 980; /* pixels */
}


/* Load Localisation files.
 * Note: the first-loaded translation file overrides any following ones if the same translation is present.
 */
function wpct_theme_setup() {
	// wp-content/languages/themes/uniq_slug-it_IT.mo
	load_theme_textdomain( "gamer-life", trailingslashit( WP_LANG_DIR ) . 'themes/' );
	// wp-content/themes/child-theme-name/lang/it_IT.mo
	load_theme_textdomain( "gamer-life-child", get_stylesheet_directory() . 'includes/i18n/' );
	// wp-content/themes/uniq_slug/lang/it_IT.mo
	load_theme_textdomain( "gamer-life", get_template_directory() . 'includes/i18n/' );
	//Allow editor style.
	add_editor_style( get_stylesheet_directory_uri() . '/assets/css/admin/editor-style.css' );
	/* Enable support for Post Thumbnails on posts and pages.
	 * Sizes handled in ./inc/func/thumbnails.php
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size(125, 125, true);
	// Setup the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'wpct_custom_background_args', array(
		'default-color' => apply_filters( 'wpct_default_background_color', 'fcfcfc' ),
		'default-image' => '',
	) ) );
	//	Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );	
	// adding post format support
	add_theme_support( 'post-formats',
		array(
			'aside',             // title less blurb
			'gallery',           // gallery of images
			'link',              // quick link to other site
			'image',             // an image
			'quote',             // a quick quote
			'status',            // a Facebook like status update
			'video',             // video
			'audio',             // audio
			'chat'               // chat transcript
		)
	);
	
	// wp menus
	add_theme_support( 'menus' );


	/*
	 * Switch default core markup for search form, comment form, comments, galleries, captions and widgets
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
		'widgets',
	) );

	// Add support for the Site Logo plugin and the site logo functionality in JetPack
	// https://github.com/automattic/site-logo
	// http://jetpack.me/
	add_theme_support( 'site-logo', array( 'size' => 'full' ) );

	// Declare WooCommerce support
	add_theme_support( 'woocommerce' );
	//	add_theme_support( 'wc-product-gallery-zoom' );
	//	add_theme_support( 'wc-product-gallery-lightbox' );
	//	add_theme_support( 'wc-product-gallery-slider' );

	// Declare support for title theme feature
	add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'wpct_theme_setup' );
 ## COMMON / UNCATEGORIZED ##
- WPCT_Interface .js split | Review option dependancy
- radiogroup[name]
- Additional responsive settings per mod (mobile-tablet-dekstop) ?
- Custom "page attributes" for specific page templates.
- Sidebar styles ( fixed off-canvas minimized etc.)
- Lazy load / ajaxify invisible-ondemand content ( notifications, aside commemnts-posts)
- Test hooks Review hooks class - hooks-content.php
- Review Interface::Typography
- CPT static to construct ?
- Automate slugs for CPTs - from singular name ?
- narrow wpct_get calls, especially for getting theme mod
- Check if accordions can be done via php or if you still need js relayout (due to autobalancing tags)
- Change sidebar tag to <aside> ?
- select2 ?

## UI ##
### CUSTOMIZER ###
#### CONSIDERED / PLANNED ####
- Implement Panels ( post > 4.0)
- Implement Selective refresh ( post > 4.5)
#### NEED FIXING ####
- Destroy tooltips on hiding panel / change behavior from click to hover ?
### IMPROVEMENTS ###
- rgbA colorpicker
- adjustable sidebars


## BACKEND ##
### NEED FIXING ###
### PERFORMANCE ###
- Scripts minify
- Implement Cache
- wpct_class esc_attr, safe checks
### IMPROVEMENTS ###
- Implement Logger
- Jetpack & other plugin tests
- Extend plugin compat


## UX ##
## COMPAT ###

## LONG TERM ##
- Switch to Namespaces
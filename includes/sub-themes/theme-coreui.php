<?php
add_filter( 'wpct_main_nav_fallback_args', 'wpct_subt_coreui_page_menu_args' );
function wpct_subt_coreui_page_menu_args( $args ) {
    //$args["menu_id"] = 'main-nav-menu';
    $args["menu_class"] = 'main-nav navfback';
    return $args;
}

add_filter( 'wpct_main_nav_menu_args', 'wpct_subt_coreui_main_nav_menu_args' );
function wpct_subt_coreui_main_nav_menu_args( $args ) {
    //$args["container_id"] = 'mainNavContainer';
	$args['container_class'] = '';
    return $args;
}

//add_filter( 'wp_nav_menu_items', 'addHomeMenuLink', 10, 2 );
function  wpct_subt_add_toggle($menuItems, $args)
{
	if('main' == $args->theme_location)
	{
		$class = '';
		$homeMenuItem = '<li ' . $class . '>' .
			$args->before .
			'<a href="' . home_url( '/' ) . '" title="Home">' .
					$args->link_before .
					'Home' .
					$args->link_after .
			'</a>' .
			$args->after .
			'</li>';
		$menuItems = $homeMenuItem . $menuItems;
	}
	return $menuItems;
}

?>
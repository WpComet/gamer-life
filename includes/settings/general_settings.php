<?php
return array(
	'brand_subacc' => array(
		'control' => array(
			'type'    => 'WPCTCC_Subaccordion',
			'label'    => array( 'text' => __( 'Brand-Logo', 'gamer-life' ), 'show' => false ),
			//	'desc'   => __( 'Some desc', 'gamer-life' ),
		),
	),
	//	'start_body_acc' => WPCT_Customizer::sub_accordion_start( __( 'Body', 'themify' ) ),
	'brand_type' => array(
		'control' => array(
			'type' => 'WPCTCC_Generic',
			'subtype' => 'radiogroup',
			'options' => array(
				"text" => "Text",
				"image" => "Image",
			),
			'value' => 'image',
			'wrap' => array(
				'input' => array('input-wrap','row','no-gutters'),
			),
			'misc' => array(
				'sublabel_class' => array('mr-2'),
			),
			'label'   => array(
				'text' => __( 'Logo type', 'gamer-life' ),
				'show' => true,
				//	'class' => array('col-2 pr-1'),
			),
			'affects' => array( 'brand_image', 'brand_text'),
			'attr' => array(
				'data-subaccordion' => 'brand',
			)
		),
		'setting' => array(
			'transport' => 'refresh'
		),
	),
	'brand_image' => array(
		'control' => array(
			'type'     => 'WPCTCC_Generic',
			'subtype'  => 'upload',
			'label'    => array( 'text' => __( 'Logo Image', 'gamer-life' ), 'show' => false ),
			'value'    => wpct_theme_get("assets_uri") . '/img/gl-logo.png',
			'wrap'     => array(
				'input_inner' => array('input-group'),
			),
			'keywords' => 'brand',
			'depends'  => array(
				'key'  => 'brand_type',
				'val'  => 'image',
			),
			'misc'     => array(
				'sublabel_class' => array('mr-2'),
			),
			'attr' => array(
				'data-subaccordion' => 'brand',
			),
		),
	),
	'brand_text' => array(
		'control' => array(
			'type'    => 'WPCTCC_Generic',
			'subtype' => 'text',
			'label'   => array( 'text' => __( 'Logo text', 'gamer-life' ), 'show' => false ),
			'value' => get_bloginfo( 'name' ),
			'keywords' => 'brand',
			'depends' => array(
				'key' => 'brand_type',
				'val' => 'text',
			),
			'attr' => array(
				'data-subaccordion' => 'brand',
			),
		),
	),
/*
	'brand' => array(
		'control' => array(
			'type'     => 'WPCTCC_Brand',
			'label'    => array( 'text' => __( 'Brand / Logo', 'gamer-life' ), 'show' => false ),
			'value'    => 	array( 
				'type' => 'image',
				'image' => wpct_theme_get("assets_uri") . '/img/gl-logo.png',
				'text' => get_option('blogname'),
			),
			'wrap'     => array(
				'input_inner' => array('input-group'),
			),
			'keywords' => 'brand, logo',
		),
		'setting' => array(
			'transport' => 'refresh'
		),
		'selector' => 'a#site-brand-main',
	),*/
	
	'favicon' => array(
		'control' => array(
			'type'    => 'WPCTCC_Upload',
			'label'   => array( 'text' => __( 'Favicon', 'gamer-life' ), 'show' => false ),
			'value' => '',
			'wrap' => array(
				'input_inner' => array('input-group'),
			),
			'keywords' => 'site-icon',
		),
		'selector' => '#site-logo-top a',
		'prop' => 'background',
	),
	
	
	'custom_feed' => array(
		'control' => array(
			'type'    => 'WPCTCC_Generic',
			'subtype' => 'url',
			'label'   => __( 'Custom Feed URL', 'gamer-life' ),
			'hint'   => __( 'If you wish to use a custom RSS feed like Feedburner', 'gamer-life' ),
			//'wrap' => false,
			'placeholder' => 'http://feeds.feedburner.com/rttest'
			//	'desc'   => __( 'Some desc', 'gamer-life' ),
		),

	),
);
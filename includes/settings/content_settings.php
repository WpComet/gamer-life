<?php
return array(	
	'breadcrumbs_show' => array(
		'control' => array(
			'type' => 'WPCTCC_Generic',
			'subtype' => 'switch',
			'value' => 'Off',
			'label'   => __( 'Show Breadcrumbs', 'gamer-life' ),			
		),
		'setting' => array(
			'transport' => 'refresh'
		),
	),
	'loop_ptypes' => array(
		'control' => array(
			'label'   => __( 'Loop Contents', 'gamer-life' ),
			'type'    => 'WPCTCC_Generic',
			'subtype'    => 'multiselect',
			'hint' => 'Choose from available custom post types to display in global loop',
			'options' => wpct_get_post_types(),
			'keywords' => "post type, cpt",
			'show' => true,
		),
	),
);
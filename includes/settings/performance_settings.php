<?php
return array(
	'logo' => array(
		'control' => array(
			'type'    => 'WPCTCC_Upload',
			'label'   => array( 'text' => __( 'Logo', 'gamer-life' ), 'show' => false ),
			'value' => wpct_theme_get("assets_uri") . '/img/logo-generic.png',
			'wrap' => array(
				'input_inner' => array('input-group'),
			),
			'keywords' => 'brand',
		),
		'selector' => '#site-logo-top a',
		'prop' => 'background',
	),
	
	'favicon' => array(
		'control' => array(
			'type'    => 'WPCTCC_Upload',
			'label'   => array( 'text' => __( 'Favicon', 'gamer-life' ), 'show' => false ),
			'value' => '',
			'wrap' => array(
				'input_inner' => array('input-group'),
			),
			'keywords' => 'site-icon',
		),
		'selector' => '#site-logo-top a',
		'prop' => 'background',
	),
	
	
	'custom_feed' => array(
		'control' => array(
			'type'    => 'WPCTCC_Generic',
			'subtype' => 'url',
			'label'   => __( 'Custom Feed URL', 'gamer-life' ),
			'hint'   => __( 'If you wish to use a custom RSS feed like Feedburner', 'gamer-life' ),
			//'wrap' => false,
			'placeholder' => 'http://feeds.feedburner.com/rttest'
		//	'desc'   => __( 'Some desc', 'gamer-life' ),
		),

	),
);
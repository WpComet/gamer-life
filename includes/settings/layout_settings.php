<?php
return array(	
	'layout_global' => array(
		'control' => array(
			'type'   => 'WPCTCC_Layout',
			'label'  =>  __( 'Global Layout', 'gamer-life' ),
			'hint'   => __( 'Global Layout. Can be overriden for different views from below', 'gamer-life' ),
			'value' => array(
				'container' => 'fluid',
				'sidebar' => 'sbl',
				'posts' => 'grid',
				'primary_sidebar_style' => 'fixed'
			),
			'keywords' => "sidebar, container, layout, grid, main"
		),
		'setting' => array(
			'transport' => 'refresh'
		),
	),
	
	'layout_home' => array(
		'control' => array(
			'type'   => 'WPCTCC_Layout',
			'label'  => __( 'Home Layout', 'gamer-life' ),
			'hint'   => __( 'Home / Frontpage layout', 'gamer-life' ),
			'keywords' => "sidebar, container, layout, grid, home"
		),
		'setting' => array(
			'transport' => 'refresh'
		),
	),
	'layout_archive' => array(
		'control' => array(
			'type'   => 'WPCTCC_Layout',
			'label'  => __( 'Archive Layout', 'gamer-life' ),
			'hint'   => __( 'Applies to Taxonomy / Archive view like Categories / Tags etc.', 'gamer-life' ),
			'keywords' => "sidebar, container, layout, grid, home, category, taxonomy, tag"
		),
	),
	'layout_single' => array(
		'control' => array(
			'type'   => 'WPCTCC_Layout',
			'label'  => __( 'Single Post Layout', 'gamer-life' ),
			'hint'   => __( 'Single post view', 'gamer-life' ),
			'keywords' => "sidebar, container, layout, grid, home, category, post, single"
		),
	),
	'layout_page' => array(
		'control' => array(
			'type'   => 'WPCTCC_Layout',
			'label'  => __( 'Page Layout', 'gamer-life' ),
			'hint'   => __( 'Page view', 'gamer-life' ),
			'keywords' => "sidebar, container, layout, grid, home, category, page, single"
		),
		'setting' => array(
			'transport' => 'refresh'
		),
	),
);
<?php
return array(
	'footer_container' =>  array(
		'control' => array(
			'type'    => 'WPCTCC_Generic',
			'subtype' => 'radiogroup',
			//'label'   => __( 'Container Type', 'wpcomet-theme' ),
			'options' => array(
				"full" => "Full",
				"boxed" => "Boxed",
			),
			'value' => "full",
			'wrap' => array(
				'input' => array('input-wrap','row','no-gutters'),
			),
			'misc' => array(
				'style' => 'image',
				'prefix' => 'container',
				'extension' => 'png',
				'sublabel_class' => array('col-6 pr-2'),
			),
			'label'   => array(
				'text' => __( 'Container Type', 'gamer-life' ),
				'show' => true,
				//	'class' => array('col-2 pr-1'),
			),	
		),	
	),	
	'footer_style' => array(
		'control' => array(
			'type'    => 'WPCTCC_Generic',
			'subtype' => 'radiogroup',
			'label'   => __( 'Footer style', 'wpcomet-theme' ),
			'options' => array(
				"footer-fixed" => "Fixed",
				"footer-scroll" => "Scroll",
			),
			'value' => 'footer-fixed',
		),
	),
);
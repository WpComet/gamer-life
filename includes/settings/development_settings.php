<?php
return array(
	'demo_mode' => array(
		'control' => array(
			'type' => 'WPCTCC_Generic',
			'subtype' => 'switch',
			'value' => 'Off',
			'label'   => __( 'Demo Mode', 'gamer-life' ),			
		),
		'setting' => array(
			'transport' => 'refresh'
		),
	),

	'theme_mode' => array(
		'control' => array(
			'type' => 'WPCTCC_Generic',
			'subtype' => 'radiogroup',
			'label'   => __( 'Theme Mode', 'gamer-life' ),					
			'options' => array(
				'prod' => 'Production',
				'dev' => 'Development',
				'final' => 'Final',
				'debug' => 'Debug'
			),
			'value' => 'prod',
			'hint'  => __( 'Leave this at Production if you are not sure.', 'gamer-life' ),			
			'desc'  => array( 
				'text' => __( 'Some detailed desc', 'gamer-life' ), 
				'label' => false, 'collapse' =>true, 
				'label_class' => array()
			),		
		),
		'setting' => array(
			'transport' => 'refresh'
		),
	),

);
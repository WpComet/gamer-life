<?php
return array(
	'media_placeholder_type' => array(
		'control' => array(
			'type'    => 'WPCTCC_Generic',
			'subtype' => 'radiogroup',
			'label'   => __( 'Placeholder Type', 'gamer-life' ),
			'hint'   => __( 'What should be displayed when there is no post image to display ?', 'gamer-life' ),
			'options' => array(
				"none" => "Nothing",
				"image" => "Image",
				"initials" => "Initials",
			),
			'value' => "image",
			'misc' => array(
				'style' => 'image',
				'prefix' => 'media-placeholder',
				'extension' => 'png',
			//	'sublabel_class' => array('col-6 pr-1'),
			),
			'keywords' => 'media,thumbnail,placeholder',
		),
	),
	
	'media_placeholder_image' => array(
		'control' => array(
			'type'     => 'WPCTCC_Generic',
			'subtype'  => 'upload',
			'label'    => array( 'text' => __( 'Placeholder Image', 'gamer-life' ), 'show' => false ),
			'value'    => wpct_theme_get("assets_uri") . '/img/placeholder.png',
			'wrap'     => array(
				'input_inner' => array('input-group'),
			),
			'keywords' => 'media,thumbnail,placeholder',
			'depends'  => array(
				'key'  => 'media_placeholder_type',
				'val'  => 'image',
			),
			'misc'     => array(
				'sublabel_class' => array('mr-2'),
			),
		),
	),
	
	'avatar_type' => array(
		'control' => array(
			'type'    => 'WPCTCC_Generic',
			'subtype' => 'radiogroup',
			'label'   => __( 'Avatar Type', 'gamer-life' ),
			'hint'   => __( 'What should be displayed in place of avatars ?', 'gamer-life' ),
			'options' => array(
				"none" => "Nothing",
				"image" => "Image",
				"initials" => "Initials",
			),
			'value' => "image",
			'misc' => array(
				'style' => 'image',
				'prefix' => 'media-avatars',
				'extension' => 'png',
			//	'sublabel_class' => array('col-6 pr-1'),
			),
			'keywords' => 'media,thumbnail,placeholder',
		),
	),
);
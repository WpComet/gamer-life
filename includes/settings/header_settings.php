<?php
return array(	
	'header_style' => array(
		'control' => array(
			'type'    => 'WPCTCC_Generic',
			'subtype' => 'radiogroup',
			'label'   => __( 'Header style', 'gamer-life' ),
			'options' => array(
				"header-fixed" => "Fixed",
				"header-scroll" => "Scroll",
			),
			'value' => "header-fixed",
		),
	),
	
	'navbar_scheme' => array(
		'control' => array(
			'label'   => __( 'Navbar Scssheme', 'gamer-life' ),
			'type'    => 'WPCTCC_Generic',
			'subtype'    => 'radiogroup',
			'wrap' => array(
				'input' => array('input-wrap','row','no-gutters'),
			),

			'misc' => array(
				'style' => 'image',
				'extension' => 'png',
				'sublabel_class' => array('col-6 pr-2'),
			),
			'options' => array(
				"navbar-light" => "light",
				"navbar-dark" => "Dark",
			),
			'value' => "navbar-dark",
		),
	),
	
	'navbar_bgcolor' => array(
		'control' => array(
			'label'   => __( 'Navbar Background', 'gamer-life' ),
			'type'    => 'WPCTCC_Colorpicker',
			'subtype'    => 'colorpicker',
			'value' => "#48337a",
		),
	),
	
	'navbar_container' => array(
		'control' => array(
			'label'   => __( 'Navbar Container', 'gamer-life' ),
			'type'    => 'WPCTCC_Generic',
			'subtype'    => 'radiogroup',
			'wrap' => array(
				'input' => array('input-wrap','row','no-gutters'),
			),

			'misc' => array(
				'style' => 'image',
				'extension' => 'png',
				'sublabel_class' => array('col-6 pr-2'),
			),
			'options' => array(
				"navbar-light" => "light",
				"navbar-dark" => "Dark",
			),
			'value' => "navbar-dark",
		),
	),
	
	'navbar_notifications' => array(
		'control' => array(
			'label'   => __( 'Navbar Notifications', 'gamer-life' ),
			'type'    => 'WPCTCC_Generic',
			'hint'   => __( 'What should display as notifications ?', 'gamer-life' ),
			'subtype'    => 'select',

			'options' => array(
				"" => "None",
				"recent-posts" => "Recent Posts",
				"recent-comments" => "Recent Comments",
			),
		),
		'setting' => array(
			'transport' => 'refresh'
		),
	),
	
	'navbar_user_info' => array(
		'control' => array(
			'label'   => __( 'Navbar User Info', 'gamer-life' ),
			'type'    => 'WPCTCC_Generic',
			'hint'   => __( 'Display user avatar and info', 'gamer-life' ),
			'subtype'    => 'switch',
			'value' => 'Off',
		),
		'setting' => array(
			'transport' => 'refresh'
		),
	),
	/*'navbar_scheme' => array(
		'control' => array(
			'label'   => __( 'Navbar Scheme', 'gamer-life' ),
			'type'    => 'WPCTCC_Generic',
			'subtype'    => 'radiogroup',
			'wrap' => array(
				'input' => array('input-wrap','row','no-gutters'),
			),

			'misc' => array(
				'style' => 'image',
				'extension' => 'png',
				'sublabel_class' => array('col-6 pr-2'),
			),
			'options' => array(
				"navbar-light" => "light",
				"navbar-dark" => "Dark",
			),
			'value' => "navbar-light",
		),
	),*/
);
<?php
/**
 * Enqueue styles & scripts
 *
 */

/**
 * Define libraries to load
 *
 * WPCT-todo Auto scan and register libraries maybe
 */
 
function wpct_libs_reg() {
	$libraries = array(
		"popper" => array(
			"folder" => "popper.js-1.11.1",
			"js" => array("popper"),
			"deps_js" => array("jquery"),
		),
		"bootstrap" => array(
			"folder" => "bootstrap-4.0.0-beta-dist",	
			"css" => array("css/bootstrap"),
			"js" => array("js/bootstrap"),
			"deps_js" => array("jquery","popper"),
			"v" => "4.0.0-beta-dist"
		),
		"owlcarousel" => array(
			"folder" => "OwlCarousel2-2.2.1",	
			"css" => array("assets/owl.carousel"),
			"js" => array("owl.carousel"),
			"deps_js" => array("jquery"),
		),
		"coreui" => array(
			"folder" => "coreui-1.0.6",	
			"css" => array("css/style"),
			"js" => array("js/app"),
			"deps_js" => array("jquery","popper","bootstrap"),
		),
		"font-awesome" => array(
			"folder" => "font-awesome-4.7.0",
			"css" => array("css/font-awesome"),
			"v" => "4.7.0"
		),
		"simple-line-icons" => array(
			"folder" => "simple-line-icons-2.4.1",
			"css" => array("css/simple-line-icons"),
		),
		"animate.css" => array(
			"folder" => "animate.css-3.5.1",
			"css" => array("animate"),
			"v" => "3.5.1"
		),
		"select2" => array(
			"folder" => "select2-4.0.3",
			"css" => array("css/select2"),
			"js" => array("js/select2"),
		),
		"tse-scroll" => array(
			"folder" => "trackpad-scroll-emulator-1.0.8",
			"css" => array("css/trackpad-scroll-emulator"),
			"js" => array("jquery.trackpad-scroll-emulator"),
		),
	);
	
	return apply_filters( 'wpct_libs_reg_arr', $libraries );
}

function wpct_enqueue_main() {
	
	//	wpct_load_libs( "popper","bootstrap","font-awesome" );
	wpct_load_lib("font-awesome");
	wpct_load_lib("simple-line-icons");
	wpct_load_lib("popper");
	wpct_load_lib("bootstrap",array("js"));
	wpct_load_lib("coreui");
	wp_enqueue_style('wpct-theme-common',  wpct_theme_get("assets_uri") . '/css/theme-common.css' );
	wpct_load_lib("tse-scroll");
	//wp_enqueue_style('wpct-theme-core',  wpct_theme_get("assets_uri") . '/css/theme-core.css' );
	wpct_load_js('wpct-theme',  wpct_theme_get("assets_uri") . '/js/theme.js' );
	wp_enqueue_style('wpct-sub-theme-coreui',  wpct_theme_get("assets_uri") . '/css/sub-themes/subtheme-coreui.css' );
	//	wpct_load_lib("tether");

}
add_action( 'wp_enqueue_scripts', 'wpct_enqueue_main' );

//require get_template_directory() . '/includes/func/styles.php';
//require get_template_directory() . '/includes/func/scripts.php';
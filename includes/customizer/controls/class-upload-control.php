<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Class to create a control that injects the markup for an image in an element.
 *
 * @since 1.0.0
 */
class WPCTCC_Upload_Control extends WPCT_Customize_Control {

	/**
	 * Type of this control.
	 * @access public
	 * @var string
	 */
	public $type = 'wpctcc-upload';
	
	/**
	 * Render the control's content.
	 *
	 * @since 1.0.0
	 */
	public function render_content() {
			$value = $this->value();
		//	var_dump( $value );
		//	var_dump( $this->wpctcc_type );
			$input_args = $this->args;
			$input_args["value"] = $value;
		//	var_dump( $this->args );

	//	if( $this->wpctcc_type == "generic" ) {
			$input_args["type"] = ( ! isset( $this->args["subtype"] ) ) ? $this->wpctcc_type : $this->args["subtype"];
			WPCT_Interface::gen_field( $this->sett_key,$input_args);
	//	}
	}
	
}
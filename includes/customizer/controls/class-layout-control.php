<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Class to create a control to set a background on an element.
 *
 * @since 1.0.0
 */
class WPCTCC_Layout_Control extends WPCT_Customize_Control {

	/**
	 * Type of this control.
	 * @access public
	 * @var string
	 */
	public $type = 'wpctcc_layout';

	/**
	 * Render the control's content.
	 *
	 * @since 1.0.0
	 */
	public function render_content() {
		//	add_filter( 'wpcnuk_gen_field_return', array( $this,'modify_wpcnuk_return') );
	//	var_dump( $this->type);
		//var_dump( $this);
		
		$v = $this->value();
	//	var_dump( $v );
	//	var_dump( esc_attr( wp_json_encode( $v ) ) );
		$values = json_decode( $v );
		//$values =  $this->value();
	//	var_dump( $values->container );
		wp_enqueue_script( 'json2' );
		wp_enqueue_media();
		
		$container_value = isset( $values->container ) ? $values->container : '';
		$sidebar_value = isset( $values->sidebar ) ? $values->sidebar : '';
		$posts_value = isset( $values->posts ) ? $values->posts : '';
		
		$label = $this->show_label && ! empty( $this->label );
		?>
		<?php
		
		//var_dump( $this->sett_key );
		//$container_value = isset( $values->container ) ? $values->container : 'boxed';
		/*
		WPCT_Interface::gen_field( $this->sett_key . '_container', array(
			'type'    => 'radiogroup',
		//	'subtype' => 'radiogroup',
			'label'   => __( 'Container Type', 'wpcomet-theme' ),
			'options' => array(
				"full" => "Full",
				"boxed" => "Boxed",
			),
		//	'value' => "boxed",
			'value' => $container_value,
			'wrap' => array(
				'input' => array('input-wrap','row','no-gutters'),
			),
			'misc' => array(
				'style' => 'image',
				'prefix' => 'container',
				'extension' => 'png',
				'sublabel_class' => array('col-6 pr-2'),
			),
			'label'   => array(
				'text' => __( 'Container Type', 'gamer-life' ),
				'show' => true,
				//	'class' => array('col-2 pr-1'),
			),		
		),
		'none' );		*/

		//	echo $container_field;
		//	var_dump( $sidebar_value );
		
		WPCT_Interface::gen_field( $this->sett_key . '_sidebar',array(
			'type' => 'radiogroup',
			'options' => array(
				"sbr" => "Sidebar Right",
				"nosb" => "No Sidebar",
				"sbl" => "Sidebar Left",
				"sbrl" => "Double Sidebars",
				"sbrr" => "2 Sidebars Right",
				"sbll" => "2 Sidebars Left",
			),
			'value' => $sidebar_value,
			'wrap' => array(
				'input' => array('input-wrap','row','no-gutters'),
			),
			'misc' => array(
				'style' => 'image',
				'sublabel_class' => array('col-2 pr-2'),
			),
			'label'   => array(
				'text' => __( 'Sidebars', 'gamer-life' ),
				'show' => true,
				//	'class' => array('col-2 pr-1'),
			),
			'affects' => array( $this->sett_key . '_primary_sidebar_style', $this->sett_key . '_secondary_sidebar_style' )
		),
		'none' );

		/*
		WPCT_Interface::gen_field( $this->sett_key . '_primary_sidebar_style',array(
				'type' => 'radiogroup',
				'label'   => array(
					'text' => __( 'Sidebar Primary Style', 'gamer-life' ),
					'show' => true,
				),
				'options' => array(
					"fixed" => "Fixed",
					"off-canvas" => "Off-Canvas",
					"minimized" => "Minimized",
					"compact" => "Compact",
					"hidden" => "Hidden",
				),
				'value' => $posts_value,
				'depends' => array(
					'key' => $this->sett_key . '_sidebar',
					'val' => 'nosb',
					'operator' => '!=',
				)
		),
		'none' );
		
		WPCT_Interface::gen_field( $this->sett_key . '_secondary_sidebar_style',array(
				'type' => 'radiogroup',
				'label'   => array(
					'text' => __( 'Sidebar Secondary Style', 'gamer-life' ),
					'show' => true,
				),
				'options' => array(
					"fixed" => "Fixed",
					"offcanvas" => "Off-Canvas",
					"hidden" => "Hidden",
				),
				'value' => $posts_value,
				'depends' => array(
					'key' => $this->sett_key . '_sidebar',
					'val' => 'nosb',
					'operator' => '!=',
				)
		),
		'none' );*/
		
		WPCT_Interface::gen_field( $this->sett_key . '_posts',array(
			'type' => 'radiogroup',
			'label'   => array(
				'text' => __( 'Posts Layout', 'gamer-life' ),
				'show' => true,
			),
			'wrap' => array(
				'input' => array('input-wrap','row','no-gutters'),
			),
			'options' => array(
				"grid" => "Grid",
				"list" => "Image List",
				"mason" => "Masonry",
			),
			'value' => $posts_value,
			'misc' => array(
				'style' => 'image',
				'sublabel_class' => array('col-3 pr-2'),
			),
		),
		'none' );		
		?>
        <input id="<?php echo $this->sett_key ?>" <?php $this->link(); ?> value='<?php echo esc_attr( $v ); ?>' type="text" class="<?php echo esc_attr( $this->type ); ?>_control wpctcc-value-field" />
		<?php
	}
}
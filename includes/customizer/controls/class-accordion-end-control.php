<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/* 
 WPCT-review: pre 4.0 sub-section accordion work-around, give up the approach by 5.0+ ?
*/

/**
 * Class to output the beginning of a sub-accordion. Must have a Themify_Sub_Accordion_End match.
 *
 * @since 1.0.0
 */
class WPCTCC_Accordion_End_Control extends WPCT_Customize_Control {

	/**
	 * @access public
	 * @var string
	 */
	public $type = 'wpctcc-accordion-end';
	public $collapse = true;
	//public $args = array();
	public $accordion_slug;
	public $accordion_id;

	/**
	 * @param WP_Customize_Manager $manager
	 * @param string               $id
	 * @param array                $args
	 * @param array                $options
	 */
	function __construct( $manager, $id, $args = array(), $options = array() ) {
		parent::__construct( $manager, $id, $args );
		$this->accordion_slug = sanitize_title_with_dashes( $this->label );
		if( isset( $args["collapse"] ) ) $this->collapse = $args["collapse"];
	}

	public function render_content() {
		//	var_dump( $this->collapse  );
		$show = ( $this->collapse ) ? '' : ' show' ;
		$coll_toggle = ( $this->collapse ) ? 'collapsed' : '' ;
		?>
				</div>
            </div>
  		</div><!-- Accordion End -->
		<?php
	}

}
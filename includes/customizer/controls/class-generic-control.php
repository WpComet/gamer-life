<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Class to create a control that injects the markup for an image in an element.
 *
 * @since 1.0.0
 */
class WPCTCC_Generic_Control extends WPCT_Customize_Control {

	/**
	 * Type of this control.
	 * @access public
	 * @var string
	 */
	public $type = 'wpctcc_generic';
}
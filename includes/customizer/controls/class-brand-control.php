<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Class to create a control to set a background on an element.
 *
 * @since 1.0.0
 */
class WPCTCC_Brand_Control extends WPCT_Customize_Control {

	/**
	 * Type of this control.
	 * @access public
	 * @var string
	 */
	public $type = 'wpctcc_brand';

	/**
	 * Render the control's content.
	 *
	 * @since 1.0.0
	 */
	public function render_content() {
		//	add_filter( 'wpcnuk_gen_field_return', array( $this,'modify_wpcnuk_return') );
	//	var_dump( $this->type);
		//var_dump( $this);
		
		$v = $this->value();
	//	var_dump( $v );
	//	var_dump( esc_attr( wp_json_encode( $v ) ) );
		$values = json_decode( $v );
		//$values =  $this->value();
	//	var_dump( $values->container );
		wp_enqueue_script( 'json2' );
		wp_enqueue_media();
		
		$label = $this->show_label && ! empty( $this->label );
		//	var_dump( $this->get_val('type') );
		?>
        
		<?php
		
		WPCT_Interface::gen_field( $this->sett_key . '_type', array(
			'type' => 'radiogroup',
			'options' => array(
				"text" => "Text",
				"image" => "Image",
			),
			'value' => $this->get_val('type'),
			'wrap' => array(
				'input' => array('input-wrap','row','no-gutters'),
			),
			'misc' => array(
				'sublabel_class' => array('mr-2'),
			),
			'label'   => array(
				'text' => __( 'Logo type', 'gamer-life' ),
				'show' => true,
				//	'class' => array('col-2 pr-1'),
			),
			'affects' => array( $this->sett_key . '_image', $this->sett_key . '_text'),
		),
		'none' );
		
		
		WPCT_Interface::gen_field( $this->sett_key . '_image', array(
			'type'    => 'upload',
			'label'   => array( 'text' => __( 'Logo', 'gamer-life' ), 'show' => false ),
			'value' => wpct_theme_get("assets_uri") . '/img/logo-generic.png',
			'wrap' => array(
				'input_inner' => array('input-group'),
			),
			'keywords' => 'brand',
			'depends' => array(
				'key' => $this->sett_key . '_type',
				'val' => 'image',
			),
		),
		'none' );
		
		WPCT_Interface::gen_field( $this->sett_key . '_text', array(
			'label'   => array( 'text' => __( 'Logo text', 'gamer-life' ), 'show' => false ),
			'value' => get_bloginfo( 'name' ),
			'keywords' => 'brand',
			'depends' => array(
				'key' => $this->sett_key . '_type',
				'val' => 'text',
			),
		),
		'none' );
			
		//var_dump( $this->sett_key );
		//$container_value = isset( $values->container ) ? $values->container : 'boxed';
		/*WPCT_Interface::gen_field( $this->sett_key . '_container', array(
			'type'    => 'typography',
			'label'   => __( 'Font', 'gamer-life' ),
		) );*/
		

		//	echo $container_field;
		//	var_dump( $container );
		
		?>
        <input id="<?php echo $this->sett_key ?>" <?php $this->link(); ?> value='<?php echo esc_attr( $v ); ?>' type="text" class="<?php echo esc_attr( $this->type ); ?>_control wpctcc-value-field" />
		<?php
	}
}
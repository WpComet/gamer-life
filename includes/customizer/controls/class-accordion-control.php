<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/* 
 WPCT-review: pre 4.0 sub-section accordion work-around, give up the approach by 5.0+ ?
*/

/**
 * Class to output the beginning of a sub-accordion. Must have a Themify_Sub_Accordion_End match.
 *
 * @since 1.0.0
 */
class WPCTCC_Accordion_Control extends WPCT_Customize_Control {

	/**
	 * @access public
	 * @var string
	 */
	public $type = 'wpctcc-accordion';
	public $collapse = true;
	//public $args = array();
	public $accordion_slug;
	public $accordion_id;

	/**
	 * @param WP_Customize_Manager $manager
	 * @param string               $id
	 * @param array                $args
	 * @param array                $options
	 */
	function __construct( $manager, $id, $args = array(), $options = array() ) {
		parent::__construct( $manager, $id, $args );
		$this->accordion_slug = sanitize_title_with_dashes( $this->label );
		if( isset( $args["collapse"] ) ) $this->collapse = $args["collapse"];
	}

	/**
	 * Display Accordion - parent
	 */
	public function render_content() {
		//	var_dump( $this->collapse  );
		$show = ( $this->collapse ) ? '' : ' show' ;
		$coll_toggle = ( $this->collapse ) ? 'collapsed' : '' ;
		$aria_expanded = ( $this->collapse ) ? 'true' : 'false' ;
		?>
        <!-- Accordion Start -->
    	<div class="wpct-accordion-wrap">
            <div class="card-header lvl0" role="tab" id="heading-<?php echo $this->accordion_slug ?>">
              <h4 class="wpct-acc-head mb-0">
				<a href="#collapse-<?php echo $this->accordion_slug ?>" class="<?php echo $coll_toggle ?>" data-toggle="collapse" href="#collapse-<?php echo $this->accordion_slug ?>" 
				aria-expanded="<?php echo $aria_expanded ?>" aria-controls="collapse-<?php echo $this->accordion_slug ?>">
                 <?php echo  $this->label; ?> <?php _e( 'Settings', 'gamer-life' ); ?>
                </a>
              </h4>
              <a class="btn toggle-level" href="#collapse-<?php echo $this->accordion_slug ?>" data-toggle="tooltip" title="<?php _e( 'Toggle all', 'gamer-life') ?>">
              	<i class="fa fa-sort" aria-hidden="true"></i>
              </a>
            </div>
        
			<div id="collapse-<?php echo $this->accordion_slug ?>" class="collapse<?php echo $show ?> lvl0" role="tabpanel" aria-labelledby="heading-<?php echo $this->accordion_slug ?>">
              <div class="card-block">
              		<ul class="section_settings">
                    </ul>
              </div>
            </div>
        </div>
		<!-- Accordion End -->
		<?php
	}

}
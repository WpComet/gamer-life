<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/* 
 WPCT-review: pre 4.0 sub-section accordion work-around, give up the approach by 5.0+ ?
*/

/**
 * Class to output the beginning of a sub-accordion. Must have a Themify_Sub_Accordion_End match.
 *
 * @since 1.0.0
 */
class WPCTCC_Subaccordion_Control extends WPCT_Customize_Control {

	/**
	 * @access public
	 * @var string
	 */
	public $type = 'wpctcc-subaccordion';
	public $collapse = true;
	public $is_faux = true;
	//public $args = array();
	public $accordion_slug;
	public $accordion_id;

	/**
	 * @param WP_Customize_Manager $manager
	 * @param string               $id
	 * @param array                $args
	 * @param array                $options
	 */
	function __construct( $manager, $id, $args = array(), $options = array() ) {
		parent::__construct( $manager, $id, $args );
		$this->accordion_slug = sanitize_title_with_dashes( $this->label );
		if( isset( $args["collapse"] ) ) $this->collapse = $args["collapse"];
	}

	/**
	 * Display the font dropdown.
	 */
	public function render_content() {
		
	//	return "123";
		//	var_dump( $this->collapse  );
		$id = 'customize-control-' . str_replace('[', '-', str_replace(']', '', $this->id));
		$id_attr = esc_attr( $id );
		$type_cl = strtolower( str_replace( '_', '-', $this->type ) );
		$class = 'wpctcc wpct-subaccordion wpct-customize-control ' . $type_cl;
		$show = ( isset( $this->args["show"] ) ) ? 'show' : '';
		
		$coll_toggle = ( $this->collapse ) ? 'collapsed' : '' ;
		$actionable = true;
		?>
        <!-- Subaccordion Start -->
        <div class="wpct-header lvl1 actionable" role="tab" id="heading-<?php echo $id_attr ?>">
            <h5 class="wpct-subacc-heading">
                <a data-toggle="collapse" href="#collapse-<?php echo $id_attr; ?>" aria-expanded="false" aria-controls="collapse-<?php echo $id_attr; ?>"><?php echo $this->label; ?></a>
            </h5>
            <?php if( $actionable ) : ?>
            <div class="actions-head actions-head-sub btn-group" data-toggle="buttons">
                <?php if( $this->wpctcc_type == "upload" ) : ?>
                <a class="btn btn-sm action-btn subhead-action-btn btn-expand" data-toggle="collapse" href="#<?php echo $this->sett_key?>-preview" aria-expanded="false" aria-controls="logo-image" data-action="control-preview" data-key="<?php echo $this->sett_key?>">
                    <i class="fa fa-picture-o" aria-hidden="true"></i><span>&nbsp;<?php _e('Preview', 'gamer-life') ?></span>
                </a>
                <?php endif; ?>
                
                <?php if( isset( $this->args["hint"] ) ) : ?>
                <a class="btn btn-sm action-btn subhead-action-btn btn-expand" data-toggle="tooltip" href="#" data-trigger="click" role="button" title="<?php echo $this->args["hint"] ?>">
                    <i class="fa fa-info-circle" aria-hidden="true"></i><span>&nbsp;Info</span>
                </a>
                <?php endif; ?>
                <a class="btn btn-sm action-btn subhead-action-btn btn-expand reset-sett" data-toggle="tooltip" href="#" role="button" title="<?php _e( 'Reset this setting to its default value','gamer-life' ) ?>" data-action="reset-setting" data-key="<?php echo $this->sett_key?>">
                    <i class="fa fa-refresh" aria-hidden="true"></i><span>&nbsp;Reset</span>
                </a>
            </div>
            <?php endif; ?>
        </div>
        <div class="subacc-content collapse<?php echo $show?>" id="collapse-<?php echo $id_attr; ?>">
            
        </div>
		<!-- Subaccordion End -->
		<?php
	}

}
<?php

if ( ! defined( 'ABSPATH' ) )	exit;
/**
 * WpComet Carousel Widget.
 *
 * @class 		WPCT_Widget_Carousel
 * @version		1.0.0
 * @package		WPCT/Widgets
 * @category	Class
 * @author 		WpComet
 */

// Creating the widget 
class WPCT_Widget_Carousel extends WP_Widget {
	
	// widget type 'jumbotron','carousel' etc.
	public $wpct_type;
	// default widget data
	public $wpct_data;
	
	function __construct() {
		$lower_classname = strtolower( get_class() );
		$widget_baseid = str_replace("widget_", "", $lower_classname );
		$this->wpct_type = str_replace( "wpct_widget_", "", $lower_classname );
		$this->wpct_data = array(
			"title" => __( 'Hello There !', 'gamer-life' ),
			"detail" => __( "You can provide a little more detail if you want.", 'gamer-life'),
			"buttontext" => __( "Learn More", 'gamer-life'),
			"buttonurl" => '',
		);
			
		parent::__construct(	 
			// Base ID of your widget
			 $widget_baseid,
			// Widget name will appear in UI
			__('WPCT Carousel', 'gamer-life'), 
			 
			// Widget description
			array( 'description' => __( 'Carousel / Slider widget', 'gamer-life' ), ) 
		);
	}
	 
	// Creating widget front-end 
	public function widget( $args, $instance ) {
		$instance = wp_parse_args( $instance, $this->wpct_data );
		// Allow theme and plugins to modify widget output
		foreach( $instance as $k => $v ) {
			$instance[$k] = apply_filters( 'wpct_' . $this->wpct_type . '_widget_' . $k, $v );
		}
		
		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		if ( ! empty( $title ) )
		echo $args['before_title'] . $title . $args['after_title'];
		// This is where you run the code and display the output
		wpct_get_template('widgets/' . $this->wpct_type . '.php', $instance );
		//ob_start(); 
		echo $args['after_widget'];
	}
	
	// Widget Backend 
	public function form( $instance ) {
		// Widget admin form
		$defaults = $this->wpct_data;
		?>

        <?php WPCT_Interface::gen_field( 'title',array(
			"type" => "text",
			"name" => $this->get_field_name( 'title' ),
			"label" => "Title",
			"value" =>	isset( $instance[ 'title' ] ) ? $instance['title'] : $defaults['title'],
		));
		?>

		<?php WPCT_Interface::gen_field( 'hide_title',array(
			"type" => "switch",
			"name" => $this->get_field_name( 'hide_title' ),
			"label" => "Hide Title ?",
			"value" =>	isset( $instance[ 'hide_title' ] ) ? $instance['hide_title'] : "On",
		));
		?>
        
		<?php WPCT_Interface::gen_field( 'source',	array(
			"type" => "select",
			"name" => $this->get_field_name( 'source' ),
			"label" => __( 'Source', 'gamer-life' ),
			"value" => isset( $instance[ 'source' ] ) ? $instance[ 'source' ] : "posts",
			"options" => array(
				"posts" => "Posts",
				"images" => "Images"
			),
		));
		?>

		<?php WPCT_Interface::gen_field( 'categories',	array(
			"type" => "taxonomy",
			"name" => $this->get_field_name( 'content' ),
			"label" => __( 'Categories', 'gamer-life' ),
			"value" => isset( $instance[ 'content' ] ) ? $instance[ 'content' ] : $defaults['content'],
			"misc" => array(
				"tax" => "category"
			)

		));
		?>
        
        <?php WPCT_Interface::gen_field( 'detail',	array(
			"type" => "textarea",
			"name" => $this->get_field_name( 'detail' ),
			"label" => "Details",
			"value" => isset( $instance[ 'detail' ] ) ? $instance[ 'detail' ] : $defaults['detail'],
		));
		?>
        
        <?php WPCT_Interface::gen_field( 'buttontext',array(
			"type" => "url",
			"name" => $this->get_field_name( 'buttontext' ),
			'label'   => __( 'Button Text', 'gamer-life' ),
			"value" => isset( $instance[ 'buttontext' ] ) ? $instance[ 'buttontext' ] : $defaults['buttontext'],
		));
		?>
        
        <?php WPCT_Interface::gen_field( 'buttonurl',array(
			"type" => "url",
			"name" => $this->get_field_name( 'buttonurl' ),
			'label'   => __( 'Button Url', 'gamer-life' ),
			"value" => isset( $instance[ 'buttonurl' ] ) ? $instance[ 'buttonurl' ] : '',
			"placeholder" => get_site_url()
		));
		?>
        
		<?php 
	}
		 
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		foreach( array_keys( $this->wpct_data ) as $data_key ) {
			//if( isset( $instance[$data_key] ) ) $data[$data_key] = apply_filters( 'wpct_' . $this->type . '_widget_' . $data_key, $instance[$data_key] );
			$instance[$data_key] = ( ! empty( $new_instance[$data_key] ) ) ? strip_tags( $new_instance[$data_key] ) : '';
		}
		//$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}
} // Class ends here
?>
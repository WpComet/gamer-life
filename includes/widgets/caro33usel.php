<?php

// Creating the widget 
class WPCT_Widget_Carousel_obsolete extends WP_Widget {
	 
	function __construct() {
		parent::__construct(
		 
		// Base ID of your widget
		'wpct_carousel', 
		 
		// Widget name will appear in UI
		__('WPCT Carousel', 'gamer-life'), 
		 
		// Widget description
		array( 'description' => __( 'Slider widget', 'gamer-life' ), ) 
		);
	}
	 
	// Creating widget front-end
	 
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		 
		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		if ( ! empty( $title ) )
		echo $args['before_title'] . $title . $args['after_title'];
		// This is where you run the code and display the output
		ob_start(); ?>
		<h4>
		  <a class="" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
			Stops <i class="fa fa-chevron-down" aria-hidden="true"></i>
	
		  </a>
		</h4>
		<div class="collapse show" id="collapseExample">
		  <div class="card card-block">
			<?php WPCT_Interface::gen_field( "fli_filt_stops",	array(
				"type" => "multicheck",
				"label" => "Max. Stops",
				"options" => array(
					"0" => "Direct",
					"1" => "1 stop",
					"2" => "2+ stops"			
				),
				"value" => array("0")
			) ) ?>
		  </div>
		</div>
		<?php echo ob_get_clean(); ?>
		<?php
		echo $args['after_widget'];
	}
			 
	// Widget Backend 
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
		$title = $instance[ 'title' ];
		}
		else {
		$title = __( 'New title', 'gamer-life' );
		}
		// Widget admin form
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<?php 
	}
		 
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}
} // Class WPCT_carousel ends here

?>
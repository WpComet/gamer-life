<?php
/**
 * Theme config / Theme object init
 * @author   WpComet
 * @version  1.0.0
 */
if ( ! defined( 'ABSPATH' ) )	exit;

function wpct_init() {
	global $wpcomet_theme;
	$prefix = 'wpctgl_';
	$wpct_theme = array(
		'name' => 'Gamer Life',
		'prefix' => $prefix,
		'registers' => array(
			// $cpt_name => $cpt_args
			'post_types' => array(
				$prefix . 'game' => array(
					'labels' => array(
						'singular_name' => __( 'Game', 'gamer-life' ),
					),
					'menu_icon' => 'dashicons-controls-play'
				)
			),
			'taxonomies' => array(
				$prefix . 'game_platform' => array(
					'post_types' => array( $prefix . 'game'),
					'terms' => array('Windows','Mac OS X','Linux / SteamOS'),
					'args' => array(
						'labels' => array(
							'singular_name' => __( 'Platform', 'gamer-life' ),
							'name' => __( 'Platforms', 'gamer-life' ),
						),
					)
				),
				$prefix . 'game_genre' => array(
					'post_types' => array( $prefix . 'game'),
					'terms' => array('Action','Adventure','Casual','Indie','MMO','Racing','RPG','Simulation','Sports','Strategy'),
					'args' => array(
						'labels' => array(
							'singular_name' => __( 'Genre', 'gamer-life' ),
							'name' => __( 'Genres', 'gamer-life' ),
						),
					)
				),
				$prefix . 'game_category' => array(
					'post_types' => array( $prefix . 'game'),
					'terms' => array('Top Sellers','New Releases','Recently Updated','Upcoming','Specials','Virtual Reality'),
					'args' => array(
						'labels' => array(
							'name' => __( 'Categories', 'gamer-life' ),
							'singular_name' => __( 'Category', 'gamer-life' ),
							
						),
					)
				),
				$prefix . 'game_features' => array(
					'post_types' => array( $prefix . 'game'),
					'terms' => array('Free to Play','Paid'),
					'args' => array(
						'labels' => array(
							'name' => __( 'Feature', 'gamer-life' ),
							'singular_name' => __( 'Features', 'gamer-life' ),
						),
					)
				),
			),
			'settings' => array(
				//	'plugin_settings' => 'wpc_flex_pop_up_plugin_settings',
				//	'helper_settings' => 'wpc_flex_pop_up_helper_settings',
				'notices' => 'wpc_flex_pop_up_notices',
				//	'active_types' => 'wpc_flex_pop_up_active_types',
			),
			'transients' => array(
				//	'setting_messages' => 'wpcgl_setting_msgs',
				'google_fonts' => 'wpcgl_gwf_trans',
			),
			'widgets' => array( 
				'popups' => 'WPCFPU_Widget_Popups',
			),
			'image_sizes' => array(
				$prefix . 'boxart' => array('720','720','1'),
				'desk_vert_med' =>	'desk_vert_med',
				'horizontal' => 'wpcfpu_horizontal'
			),
		),
	);

	$GLOBALS['wpcomet_theme'] = new WPCT_Theme($wpct_theme);
}
add_action( 'after_setup_theme', 'wpct_init', 9 );
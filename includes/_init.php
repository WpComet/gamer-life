<?php if ( ! defined( 'ABSPATH' ) ) {
	 exit;
}
/* Initializes the theme instead of functions.php
 * @author   WpComet
 * @version  1.0.0
 * Min required version: 3.5
 * Recommended min version: 4.0
 */

////// 1- Prepare //////
// Autoloader
require get_template_directory() . '/includes/classes/class-wpct-autoloader.php';
// Essential theme functions.
require get_template_directory() . '/includes/theme-functions.php';
// Config - Construct WPCT_Theme object
require get_template_directory() . '/includes/config.php';
// Add shortcuts too admin bar, admin menu etc.
//require get_template_directory() . '/includes/shortcuts.php';
// Settings
WPCT_Settings::init();

// Custom Post Types
require get_template_directory() . '/includes/pro/custom-post-types.php';

////// 2- Initialize //////
// Admin Includes - non ajax
if( wpct_is_it('admin') && ! defined( 'DOING_AJAX' ) ) {
	
	// Status change Activation - Deactivation
	require get_template_directory() . '/includes/classes/class-wpct-welcome.php';
	
	//WPCT_Welcome::init();
	WPCT_Status::init();
	// Perform Updates if necessary
	WPCT_Update::init();
	
	//	require get_template_directory() . '/includes/classes/class-wpct-welcome.php';
	//	require get_template_directory() . '/includes/admin/admin-scripts.php';
}

// Customizer | You cant load customizer files inside is_admin conditional  | @link: https://core.trac.wordpress.org/ticket/30387 
require get_template_directory() . '/includes/customizer/class-wpct-customizer.php';


////// 3- Launch //////
//// PT1
// Setup the theme environment
require get_template_directory() . '/includes/setup.php';

// Register widget areas and widgets
require get_template_directory() . '/includes/widgetize.php';

// Enqueue scripts & styles
require get_template_directory() . '/includes/enqueue.php';

//// PT2
// Clean up unused or unsecure stuff.
require get_template_directory() . '/includes/extend/cleanup.php';
// Template tags.
require get_template_directory() . '/includes/template-tags.php';
// Plugin compatability for Woocommerce, Jetpack etc.
require get_template_directory() . '/includes/extend/compat.php';
// Misc. stuff
require get_template_directory() . '/includes/extend/xtras.php';
// Hooks
require get_template_directory() . '/includes/classes/class-wpct-hooks.php';
require get_template_directory() . '/includes/hooks.php';

// Ajax includes
if( wpct_is_it('ajax') ) {
	//	require get_template_directory() . '/includes/ajax.php';
	//	require get_template_directory() . '/includes/admin/admin-ajax.php';
}

//	Front end only
if( wpct_is_it('frontend') ) {
	
}

?>
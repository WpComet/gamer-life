<?php
/**
 * Custom Post Types
 *
 * Define post types and taxonomies to register.
 *
 * @author    WpComet
 */

 function wpctgl_register_cpts() {
	 global $wpcomet_theme;
	 $prefix = $wpcomet_theme->prefix;
	 $registers = array(
		'post_types' => array(
			$prefix . 'game' => array(
				'labels' => array(
					'singular_name' => __( 'Game', 'gamer-life' ),
				),
				'rewrite' => array(
					'slug' => 'game'
				),				
				'menu_icon' => 'dashicons-controls-play',
			)
		),
		'taxonomies' => array(
			$prefix . 'game_platform' => array(
				'post_types' => array( $prefix . 'game'),
				'terms' => array('Windows','Mac OS X','Linux / SteamOS'),
				'args' => array(
					'labels' => array(
						'singular_name' => __( 'Platform', 'gamer-life' ),
						'name' => __( 'Platforms', 'gamer-life' ),
					),
					'rewrite' => array(
						'slug' => 'platform'
					),
				)
			),
			$prefix . 'game_genre' => array(
				'post_types' => array( $prefix . 'game'),
				'terms' => array('Action','Adventure','Casual','Indie','MMO','Racing','RPG','Simulation','Sports','Strategy'),
				'args' => array(
					'labels' => array(
						'singular_name' => __( 'Genre', 'gamer-life' ),
						'name' => __( 'Genres', 'gamer-life' ),
					),
					'rewrite' => array(
						'slug' => 'genre'
					),
				)
			),
			$prefix . 'game_category' => array(
				'post_types' => array( $prefix . 'game'),
				'terms' => array('Top Sellers','New Releases','Recently Updated','Upcoming','Specials','Virtual Reality'),
				'args' => array(
					'labels' => array(
						'name' => __( 'Categories', 'gamer-life' ),
						'singular_name' => __( 'Category', 'gamer-life' ),		
					),
					'rewrite' => array(
						'slug' => 'category'
					),
				)

			),
			$prefix . 'game_features' => array(
				'post_types' => array( $prefix . 'game'),
				'terms' => array('Free to Play','Paid'),
				'args' => array(
					'labels' => array(
						'name' => __( 'Feature', 'gamer-life' ),
						'singular_name' => __( 'Features', 'gamer-life' ),
					),
					'rewrite' => array(
						'slug' => 'feature'
					),
				)

			),
		),
	);
	WPCT_Pro_CPT::init($registers);
}
add_action( 'init', 'wpctgl_register_cpts', 4);

 ?>
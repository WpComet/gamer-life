<?php
/**
 * Custom Post Types
 *
 * Registers post types and taxonomies.
 *
 * @class     WPCT_CPT
 * @version   1.0.0
 * @package   WPCT/Classes/
 * @category  Class
 * @author    WpComet
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * WPCT_CPT Class.
 * WPCT-TODO: Convert static methods to construct ?
 * WPCT-TODO: Do Capabilities
 * WPCT-TODO: Disabling adding of CPT terms ?
 */
 
class WPCT_Pro_CPT {	

	public static $prefix;
	public static $post_types;
	public static $taxonomies;
	/**
	 * Hook in methods.
	 */
	public static function init($registers) {
		//die( var_dump( $registers ) );
		//add_action( 'init', array( __CLASS__, 'defines' ), 4 );
		self::$post_types = $registers["post_types"];
		self::$taxonomies = $registers["taxonomies"];

		add_action( 'init', array( __CLASS__, 'register_post_types' ), 5 );
		add_action( 'init', array( __CLASS__, 'register_taxonomies' ), 5 );
		//	add_action( 'init', array( __CLASS__, 'create_terms' ) );
		//	add_action( 'pre_insert_term', array( __CLASS__, 'disable_type_insert' ), 0, 2 );
		//	add_action( 'admin_menu', array( __CLASS__, 'remove_submenu' ) );
		
	}

	// return a readable name of a slug
	public static function readable_name($str,$prefix) {
		// remove prefix from slug
		$name = str_replace($prefix,'',$str);
		//die( var_dump( $name,$str,$prefix ) );
		// remove dashes and underscores
		$name = str_replace( array('-','_'), ' ', $name);
		return ucwords( $name );		
	}
	
	/**
	 * Register post types.
	 */
	public static function register_post_types( $post_types=array(),$prefix='',$post_type_args=array() ) {
		//	var_dump( $wpcomet_theme->registers );
		if( empty( $prefix ) ) $prefix = self::$prefix;
		if( empty( $post_types ) ) $post_types = self::$post_types;
		//	die( var_dump( $post_types ) );
		//	if( empty( $post_type_args ) ) $post_type_args = array_values($post_types);

		foreach( $post_types as $post_type => $post_type_args ) {
			if ( ! post_type_exists($post_type) ) {

				do_action( $prefix . '_register_post_type' );

				$singular = isset( $post_type_args['labels']['singular_name'] ) ? $post_type_args['labels']['singular_name'] : self::readable_name($post_type,$prefix);
				//	die( var_dump( $singular));
				$plural = isset( $post_type_args['labels']['name'] ) ? $post_type_args['labels']['name'] : $singular . 's';
				$slug = isset( $post_type_args['rewrite']['slug'] ) ? $post_type_args['rewrite']['slug'] : $post_type;
				$supports = isset( $post_type_args['supports'] ) ? $post_type_args['supports'] : array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' );
				
				//	die( var_dump($post_type_args) );
				//	die( var_dump($slug) );
				$defaults = array(
					'labels' =>  array(
						'name' => $plural,
						'singular_name' => $singular,
						'menu_name' => $plural,
						'all_items' => sprintf( __('All %s', 'gamer-life'), $plural ),						
						'add_new' => __('Add New', 'gamer-life'),
						'add_new_item' => sprintf( __('Add new %s', 'gamer-life'), $singular ),
						'edit_item' => sprintf( __('Edit %s', 'gamer-life'), $singular ),
						'new_item' => sprintf( __('New %s', 'gamer-life'), $singular ),
						'view_item' => sprintf( __('View %s', 'gamer-life'), $singular ),
						'search_items' => sprintf( __('Search %s', 'gamer-life'), $plural ),
						'not_found' => sprintf( __('No %s found', 'gamer-life'), $plural ),
						'not_found_in_trash' => sprintf( __('No %s found in Trash', 'gamer-life'), $plural ),
						'parent_item_colon' => sprintf( __('Parent %s:', 'gamer-life'), $singular )
					),
					'public' => true,
					'rewrite' => array(
						'slug' => $slug,
					),
					'supports' => $supports,
					'menu_icon' => '',
				);

				// merge user submitted options with defaults
				$post_type_args["labels"] = wp_parse_args( $post_type_args["labels"], $defaults["labels"] );
				$post_type_args = wp_parse_args( $post_type_args, $defaults );
				//	die( var_dump( $post_type_args) );
				$register_pt = register_post_type( $post_type,
					apply_filters( 
						$prefix . '_register_post_type_' . $post_type,
						$post_type_args
					)
				);

				do_action(  $prefix . '_after_register_post_type' );
				
			}
		}
		

	}
	
	/**
	 * Register taxonomies.
	 */
	public static function register_taxonomies( $taxonomies=array(),$prefix='',$taxonomies_args=array() ) {
		global $wpcomet_theme;
		if( empty( $prefix ) ) $prefix = self::$prefix;
		if( empty( $taxonomies ) ) $taxonomies = self::$taxonomies;

		foreach( $taxonomies as $taxonomy => $tax_reg_args ) {
			if ( ! taxonomy_exists($taxonomy) ) {
				$tax_args = isset( $tax_reg_args['args']) ? $tax_reg_args['args'] : array();
				$singular = isset( $tax_args['labels']['singular_name'] ) ? $tax_args['labels']['singular_name'] : self::readable_name($taxonomy,$prefix);
				$plural = isset( $tax_args['labels']['name'] ) ? $tax_args['labels']['name'] : $singular . 's';
				$slug = isset( $tax_args['rewrite']['slug'] ) ? $tax_args['rewrite']['slug'] : $taxonomy;
				
				//	var_dump( die($slug));
						// default labels
				$defaults = array(
					'labels'=> array(
						'name' => $plural,
						'singular_name' => $singular,
						'menu_name' => $plural,
						'all_items' => sprintf( __('All %s', 'gamer-life'), $plural ),
						'edit_item' => sprintf( __('Edit %s', 'gamer-life'), $singular ),
						'view_item' => sprintf( __('View %s', 'gamer-life'), $singular ),
						'update_item' => sprintf( __('Update %s', 'gamer-life'), $singular ),
						'add_new_item' => sprintf( __('Add New %s', 'gamer-life'), $singular ),
						'new_item_name' => sprintf( __('New %s', 'gamer-life'), $singular ),
						'parent_item' => sprintf( __('Parent %s', 'gamer-life'), $plural ),
						'parent_item_colon' => sprintf( __('Parent %s:', 'gamer-life'), $plural ),
						'search_items' => sprintf( __('Search %s', 'gamer-life'), $plural ),
						'popular_items' => sprintf( __('Popular %s', 'gamer-life'), $plural ),
						'separate_items_with_commas' => sprintf( __('Separate %s with commas', 'gamer-life'), $plural ),
						'add_or_remove_items' => sprintf( __('Add or remove %s', 'gamer-life'), $plural ),
						'choose_from_most_used' => sprintf( __('Choose from most used %s', 'gamer-life'), $plural ),
						'not_found' => sprintf( __('No %s found', 'gamer-life'), $plural )
					),
					'hierarchical' => false,
					'rewrite' => array(
						'slug' => $slug
					),					
				);
				//	die( var_dump( $tax_args) );
				$tax_args["labels"] = wp_parse_args( $tax_args["labels"], $defaults["labels"] );				
				$tax_args = wp_parse_args( $tax_args, $defaults );
				//	die( var_dump( $tax_args) );
				
				do_action( $prefix . '_register_taxonomy' );
				
				$register_tax = register_taxonomy( $taxonomy,
					apply_filters( 
						$prefix . '_taxonomy_objects_' . $taxonomy,
						$tax_reg_args['post_types']
					),
					apply_filters( 
						$prefix . '_taxonomy_args_' . $taxonomy,
						$tax_args
					)
				);
				
				do_action(  $prefix . '_after_register_taxonomy' );
				//	var_dump( $taxonomy);
				self::create_terms($tax_reg_args['terms'],$taxonomy);
			}
		}	 
	}

	// wp_insert_term( $term, $taxonomy, $args = array() );
	public static function create_terms($terms,$taxonomy) {
		//	die( var_dump( $terms ) );
		foreach ( $terms as $term ) {
			if( is_array($term) ) {
				$args = $term['args'];
				$name = $term['name'];
			}
			else {
				$args = array();
				$name = $term;
			}

			if ( ! get_term_by( 'slug', sanitize_title( $name ), $taxonomy ) ) {
				// die( var_dump( $term, $taxonomy ) );
				$inserted = wp_insert_term( $name, $taxonomy, $args );
				/*
				if ( is_wp_error( $inserted ) ) {
					$error_string = $inserted->get_error_message();
					WPCT_Logger::
				}*/
				//	die( var_dump( $inserted) );
			}
		}
	}
	
	public static function disable_type_insert( $term, $taxonomy ) {
	   /*if( 'mycustom_post_type' === $taxonomy ) {
		   return new WP_Error( 'term_addition_blocked', __( 'You cannot add terms to this taxonomy', 'gamer-life' ) );
	   }
	   else {
		   return $term;   
	   }*/
	}

	// Remove unnecessary first submenu
	public static function remove_submenu() {
		//global $submenu;
		$cpt_slugs = array_keys(self::$post_types);
		foreach ( $cpt_slugs as $cpt_slug) {
			remove_submenu_page('edit.php?post_type='.$cpt_slug.'','edit.php?post_type='.$cpt_slug.'');		
		}
		//die( var_dump( $submenu) );
	}

}
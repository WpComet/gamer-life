// _eo-review: optimize uploader js - selector find and stuff

jQuery(document).ready(function($){
	$('.color-field').wpColorPicker();

	var wpct_upload;
	var wpct_selector;

	function wpct_add_file(event, selector) {

		var upload = $(".uploaded-file"), frame;
		var $el = $(this);
		wpct_selector = selector;

		event.preventDefault();

		// If the media frame already exists, reopen it.
		if ( wpct_upload ) {
			wpct_upload.open();
		} else {
			// Create the media frame.
			wpct_upload = wp.media.frames.wpct_upload =  wp.media({
				// Set the title of the modal.
				title: $el.data('choose'),

				// Customize the submit button.
				button: {
					// Set the text of the button.
					text: $el.data('update'),
					// Tell the button not to close the modal, since we're
					// going to refresh the page when the image is selected.
					close: false
				}
			});

			// When an image is selected, run a callback.
			wpct_upload.on( 'select', function() {
			//	console.log("22");
			//	console.log(wpct_selector);
				//console.log();
				// Grab the selected attachment.
				var attachment = wpct_upload.state().get('selection').first();
				//console.log(attachment);
				//console.log(attachment.attributes.url);
				console.log(wpct_selector);
				wpct_upload.close();
				wpct_selector.find('.wpc-input.upload').val(attachment.attributes.url).trigger('change');
//									wpct_selector.;

//				wp.customize.trigger('change');
				/*if ( attachment.attributes.type == 'image' ) {
					//	wpct_selector.find('.uplprev').empty().hide().append('<img src="' + attachment.attributes.url + '" class="img-fluid uplprevimg"><a class="uplrem remove-image btn btn-danger btn-xs">X Remove</a>').slideDown('fast');
					wpct_selector.find('.uplprevtoglbl').show();
				}*/
				//wpct_selector.find('.upload-button').unbind().addClass('remove-file').removeClass('upload-button').val("Remove");
				selector.find('.upload-button').unbind().removeClass('upload-button btn-info').addClass('remove-file btn-warning').find("span").text("Remove").find("i").removeClass('fa-upload').addClass('fa-trash');

				wpct_selector.find('.of-background-properties').slideDown();
				wpct_selector.find('.remove-image, .remove-file').on('click', function() {
					wpct_remove_file( $(this).parents('.wpctcc-upload') );
				});
			});

		}

		// Finally, open the modal.
		wpct_upload.open();
	}

	function wpct_remove_file(selector) {
		selector.find('.remove-image').hide();
		//	selector.find('.uplprevtoglbl').hide();
		selector.find('.wpc-input.upload').val('');
		selector.find('.of-background-properties').hide();
		selector.find('.screenshot').slideUp();
		selector.find('.remove-file').unbind().removeClass('remove-file btn-warning').addClass('upload-button btn-info').find("span").text("upload").find("i").removeClass('fa-trash').addClass('fa-upload');
		//selector.find('.remove-file i').removeClass('fa-trash').addClass('fa-upload');
		// We don't display the upload button if .upload-notice is present
		// This means the user doesn't have the WordPress 3.5 Media Library Support
		if ( $('.section-upload .upload-notice').length > 0 ) {
			$('.upload-button').remove();
		}
		selector.find('.upload-button').on('click', function(event) {
			wpct_add_file(event, $(this).parents('.wpctcc-upload'));
		});
	}

	$('.remove-image, .remove-file').live('click', function() {
		 console.log( "remm" );
    	wpct_remove_file( $(this).parents('.outer-wrap'));
    });

    $('.upload-button').click( function( event ) {
    	wpct_add_file(event, $(this).parents('.outer-wrap'));
    });

});
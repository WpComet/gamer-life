(function (exports, $) {
	'use strict';
	
	// Google Font Loader for fonts preview
	var wf = document.createElement('script');
	wf.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js';
	wf.type = 'text/javascript';
	wf.async = 'true';
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(wf, s);
	
	var api = wp.customize,
	initialLoad = true;
	
	window.onbeforeunload = function () {
		//console.log( "beforeunload" );
		//console.log( wp.customize.state() );
		if (!wp.customize.state('saved')()) {
			return wpcometCustomizerControls.confirm_on_unload;
		}
	};
	
	$.urlParam = function(name, url) {
		if (!url) {
		 url = window.location.href;
		}
		var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(url);
		if (!results) { 
			return false;
		}
		return results[1] || false;
	}
	
	function wpct_scrollTo(el) {
		$('html, body').animate({
			scrollTop: el.offset().top - 80
		}, 800);
	}
	
	function collapseAll () {
		$('.wpct-accordion-wrap').find('.collapse').collapse('hide');
	//	$(this).parents('.wpct-accordion-wrap').find('.collapse.subacc-content').collapse('toggle');
	}
	
	function showControl( ctrl ) {
				var ctrl_key = $(this).data("key") + "_ctrl",
			section = $(this).data("section");
			
		$( "#collapse-" + section ).collapse('show');
		//console.log ( "should show" + " #collapse-" + section );
		//console.log ($( "#collapse-customize-control-" + ctrl_key ).collapse('show') );
		$( "#collapse-customize-control-" + ctrl_key ).collapse('show');
					api.control( ctrl_key ).focus();

	}
	function reflowAccordions() {
		// Move subaccordions to their appropriate section accordions
		$( '.wpct-subaccordion, .wpctcc-subaccordion' ).each( function() {
			$( this ).appendTo( $( '#collapse-' + $(this).data("subsection") + ' ul.section_settings') );	
		});
		
		$( 'input[data-subaccordion]' ).each( function() {
			//console.log( "should append");
			//console.log( $( this ).parents('.outer-wrap'));
			//			console.log( 'to li#collapse-customize-control-' +  $(this).data("subaccordion")  + '_subacc_ctrl');

			$( this ).parents('.outer-wrap').appendTo( $( '#collapse-customize-control-' +  $(this).data("subaccordion")  + '_subacc_ctrl' ) );	
		});
	};
	/*
	api.previewer.bind( 'preview-edit', function( data ) {
		var data = JSON.parse( data );
		 console.log( data );
		var control = api.control( data.name );
	
		//control.focus();
	} );	*/	

	api.bind( 'change', function() { // Ready?
		console.log("chngg");


	});
	api.bind( 'ready', function() { // Ready?
		console.log("event.ready");
		reflowAccordions();

		//	$( 'li#accordion-section-wpcomet_theme_options ul.accordion-section-content').data("children",".wpctcc-accordion");
		//	console.log( $( 'li#accordion-section-wpcomet_theme_options ul.accordion-section-content') );

		// Accordion toggle
		$('a.toggle-level').click(function(e) {
			//	$(this).nextUntil(('subacc-content).collapse('toggle')
			e.preventDefault();
			 $(this).parents('.wpct-accordion-wrap').find('.collapse.lvl0').collapse('show');
			 $(this).parents('.wpct-accordion-wrap').find('.collapse.subacc-content').collapse('toggle');
			//console.log("cc");
        });

		

		$( 'li[id*="nav_menu_locations"]' ).each( function() {
			$( this ).appendTo( $( this ).prev().find( '.wpct-subaccordion' ) );
		});


	//	var ctrl_focus = $.urlParam('focus_control');
		//console.log( ctrl_focus );
		//if( ctrl_focus )
		$('[data-toggle="tooltip"]').tooltip();
//		$('.collapse').collapse({'toggle': false});
		$('a[data-action="reset-setting"]').on("click", function () {
			var ri = $(this).find("i.fa").addClass("fa-spin"),
				key = $(this).data("key");			
			$.post(	ajaxurl,
				{
					'action': 'wpcomet_customizer_remove_theme_mod',
					'key': key,
					'nonce': wpcometCustomizerControls.nonce
				},
				function (data) {
					
					console.log( data );
					console.log( ri );
					console.log( key );
					// wrong ? api.preview.send('refresh');
					//api.previewer.refresh();
					/*
					window.location.href = window.location.pathname+"?"+$.param({'focus_control':key,'base':'ball'})

					window.location.href.reload( true );*/
					window.location.search += '&focus_control=' + key;

					if ('removed' === data) {
						// Set timestamp to force live preview reload
						
						//api.state('saved');
						//$field.val(JSON.stringify(control.value[control.id])).trigger('change');
					}
				}
			);
			
			//console.log("collapse-customize-control-logo_ctrl");
			//console.log(  "#collapse-customize-control-" + $(this).data("key") + "_ctrl" );
			$( "#collapse-customize-control-" + $(this).data("key") + "_ctrl").collapse('show');
		});
		
		$('a[data-action="control-preview"]').on("click", function () {	
			//console.log("collapse-customize-control-logo_ctrl");
			//console.log(  "#collapse-customize-control-" + $(this).data("key") + "_ctrl" );
			$( "#collapse-customize-control-" + $(this).data("key") + "_ctrl").collapse('show');
		});

		var wpct_highlight_search = function (string) {
			$("em.wpct-ss-hilite").contents().unwrap();
			$("#wpct-setting-search-results li.match .setting-item.match a").each(function(index, element) {
                var els = $(this).find("span.wpct-si-text");
           		// console.log( els );
				$( els ).each(function () {
					var el = $(this),
					thetext = $(this).text();
					//console.log( el );
					//console.log( thetext );
	
					var matchStart = thetext.toLowerCase().indexOf("" + string.toLowerCase() + "");
						//	console.log(  $(this).text() );
						//	console.log( matchStart );
					if( matchStart != -1 ) {
						var matchEnd = matchStart + string.length - 1;
						var beforeMatch = thetext.slice(0, matchStart);
						var matchText = thetext.slice(matchStart, matchEnd + 1);
						var afterMatch = thetext.slice(matchEnd + 1);
						el.html(beforeMatch + "<em class='wpct-ss-hilite'>" + matchText + "</em>" + afterMatch);
					}
				//	$(this).find('small').html(beforeMatch + "<em>" + matchText + "</em>" + afterMatch);
				});
			});
		};
		
		var colourIsLight = function (r, g, b) {
		  // Counting the perceptive luminance
		  // human eye favors green color... 
		  var a = 1 - (0.299 * r + 0.587 * g + 0.114 * b) / 255;
		  console.log(a);
		  return (a < 0.5);
		}
		
		$("#wpct-setting-search-input").on("keyup click input", function () {
			if (this.value.length > 1) {
				collapseAll();
				/*$("#wpct-setting-search-results li .setting-item").hide().removeClass("match").filter(function () {
					$(this).text().toLowerCase().indexOf($("#wpct-setting-search-input").val().toLowerCase()) != -1;
				}*/
			//	$("#wpct-setting-search-results li .setting-item").find("a,small").filter(':contains(' + $(this).val() + ')').addClass("filted");
			//	console.log( $(this).val() );
				//console.log( $("#wpct-setting-search-results li .setting-item").filter(':contains(' + $(this).val() + ')').addClass("filted") );
		//		$("#wpct-setting-search-results li .setting-item").filter(':contains(' + $(this).val() + ')').addClass("filted");
		//			console.log( $("#wpct-setting-search-results li .setting-item").filter(':contains(' + $("#wpct-setting-search-input").val()  + ')') );
				var matches = $("#wpct-setting-search-results li .setting-item").hide().removeClass("match").filter(function () {
				//	console.log( $(this).text().toLowerCase().indexOf($("#wpct-setting-search-input").val().toLowerCase()) != -1 );
//					console.log( $(this).text() );
//					console.log( $(this) );
					$(this).parent().removeClass("match").hide();
					//$(this).parent().addClass("match").show();
					return $(this).text().toLowerCase().indexOf($("#wpct-setting-search-input").val().toLowerCase()) != -1;
				}).addClass("match").show().parent().addClass("match").show();
				wpct_highlight_search(this.value);
				//console.log( matches.length );
				var noresults_el = $("ul#wpct-setting-search-results li#wpct-ss-nores");
				if( matches.length == 0 ) {
					if( noresults_el.length == 0 ) $("#wpct-setting-search-results").append('<li id="wpct-ss-nores" class="list-group-item"><div class="setti3ng-item">No matches found</div></li>');
					$( noresults_el ).show();
					$( noresults_el ).find(".setting-item").show();
				}
				else {
					//console.log( matches.length );
					//console.log( noresults_el.length );
					noresults_el.remove();
				}
				$("#wpct-setting-search-results").show();
			}
			else {
				$("#wpct-setting-search-results").removeClass("match").hide();
			}
		});

		
		$("#wpct-setting-search-results li a").on("click", function() {
			$("#wpct-setting-search-results").hide();
			var ctrl_key = $(this).data("key") + "_ctrl",
			section = $(this).data("section");
			collapseAll();
			//console.log ( "open: " + "#collapse-" + section, "#collapse-customize-control-" + ctrl_key );
			$( "#collapse-" + section ).collapse('show');
			//console.log ( "should show" + " #collapse-" + section );
			//console.log ($( "#collapse-customize-control-" + ctrl_key ).collapse('show') );
			$( "#collapse-customize-control-" + ctrl_key ).collapse('show');
			//collapse-customize-control-favicon_ctrl

			//api.control( ctrl_key ).focus();
			//wp.customize.control( 'layout_page' ).focus()
			//api.control( $(this).data("key") ).focus();
			//	wpct_scrollTo( $('input#' + $(this).data("key") ) );
			//$('input#' + $(this).data("key") ).focus(); 
		});
		
		$("input:radio.wpc-input").on("change", function() {
			$('input#' + $(this).attr("name") ).val( $(this).val() );
			//console.log( $(this).val() );
		}).trigger('change');
		
		var dependencies = [];
		function check_dependency(el,dep_curr_val) {

			var operators = {
				'+': function(a, b) { return a + b },
				'<': function(a, b) { return a < b },
				'==': function(a, b) { return a == b },
				'!=': function(a, b) { return a != b },
				 // ...
			};
			var depends_attr = $(el).attr("data-depends"),
				depends = $.parseJSON( depends_attr ),
				op = (depends.compare) ? depends.compare : '==',
				dependable =  $(el).attr("id").replace('wrap-', ''),
				input = $("input[name=" + depends.key + "]" ),
				inputval = input.hasClass("input-radiogroup") ? $("input:radio[name=" + depends.key + "]:checked").val() : $("input[name=" + depends.key + "]" ).val();
		
		if( ! dep_curr_val ) dep_curr_val = inputval;

			//	console.log( "checking dependency: " + dependable );
			//	console.log( depends.key );
			//	console.log( depends.val );
			//	console.log( dep_curr_val );
			if( operators[op](dep_curr_val, depends.val)  ) {
				$( el ).slideDown().show();			
			}
			else {
				$( el ).slideUp().hide();
			}
			
			var item = {};
			item ["affectee"] = depends.key;
			item ["dependee"] = dependable;
			item ["val"] = depends.val;
			return item;
		}
		
		function check_dependencies( $elements ) {
			//	console.log( "checking_dependencies...");
			dependencies = [];
		//	affectees = [];
			
				$('div.outer-wrap[data-depends]').each(function(index, element) {
				//	console.log( dependencies );
			//		console.log( $(dependencies) );
				//	check_dependency($(this));
					var item2p = check_dependency($(this));
					//	console.log( item2p );
					dependencies.push( item2p  );

				//	affectees.push( '[name="'+depends.key+'"]' );
				//	affectees.push( depends.key );
					
					//console.log( depends.key );
				//	console.log( depends.val );
				//	console.log( dep_curr_val );
				//	console.log( op );
				});
			//	return affectees;
		}
		check_dependencies();
		//	var affectees = check_dependencies();
		//	console.log( affectees );
		//	console.log( dependencies );
		$.each( dependencies, function( i, dependency ){
			$( '[name="'+ dependency.affectee +'"]' ).on("change", function() {
				check_dependency($("#wrap-" + dependency.dependee),$(this).val());
			});
			//console.log(  );
		});
		
		
		/*
		function checkDependency() {
			
		}
		
		var operators = {
			'+': function(a, b) { return a + b },
			'<': function(a, b) { return a < b },
			'==': function(a, b) { return a == b },
			'!=': function(a, b) { return a != b },
			 // ...
		};
		
		$('div.outer-wrap[data-depends]').each(function(index, element) {
			var dependables = [],
			dependencies = [],
			depends_attr = $(this).attr("data-depends"),
			depends = $.parseJSON( depends_attr ),
			op = (depends.compare) ? depends.compare : '==',
			dep_curr_val = $("input[name=" + depends.key + "]" ).val(),
			dependable =  $(this).attr("id").replace('wrap-', '');
			
			dependables.push(dependable);
			
			console.log( "checking dependancy: " + dependable );
			console.log(  dependables );
			//console.log( depends.key );
			console.log( depends.val );
			console.log( dep_curr_val );
			console.log( op );

			if( operators[op](dep_curr_val, depends.val)  ) {
				$( this ).slideDown().show();			
			}
			else {
				$( this ).slideUp().hide();
			}			     
        });
		
		//$("div.outer-wrap[data-affects] input").on("change", function() {
		$(".wpc-input.affects").on("change", function() {
		//$("div.outer-wrap[data-affects] input").on("change", function() {
			
			var outer = $(this).closest('.outer-wrap'),
				affected = outer.data("affects");

			// Check the affected sections by this input change
			$.each( affected, function( i, aff_key ){
				// console.log( v );
				// wrap-layout_global_sidebar_style
				// var depval = $("ul#sub-accordion-section-wpcomet_theme_options").find("[data-customize-setting-link='" + aff_key + "']").val();
				//	console.log( aff_key );

				

				// Check if dependencies are met
				var dependee = $("div#wrap-" + aff_key ),
					//var dependee = $("ul#sub-accordion-section-wpcomet_theme_options").find("div#wrap-" + aff_key ),
					depends_attr = dependee.attr("data-depends"),
					depends = $.parseJSON( depends_attr );
					//	console.log(  depends );
					//	console.log(  aff_key );
					var dep_curr_val = $("input#" + depends.key ).val(),
					op = (depends.compare) ? depends.compare : '==';
					
					//console.log( depends.key );
					console.log( depends.val );
					console.log( dep_curr_val );
					console.log( op );
					
					
					
					console.log( operators[op](dep_curr_val, depends.val) );
				
				if( operators[op](dep_curr_val, depends.val)  ) {
					$( dependee ).slideDown().show();
					
				}
				else {
					$( dependee ).slideUp().hide();
				}
				// $("input").data-customize-setting-link.val()
				// $layout_global_sidebar_style
				//	groupwrap-layout_global_sidebar_style 
			});
		}).trigger('change');
		*/
	/*
		$('#wpct-setting-search-input').on('keyup', function(){
			var searchTerm = $(this).val().toLowerCase();
			$('#wpcfpu_popup_types_gallery li').each(function(){
				
				$("ul#wpct-setting-search-results").append("<li> Setting " + aresp.indexval + " of "+ aresp.total +" - <b> " + aresp.name + " </b>downloaded successfully</li>");

				if ($(this).filter('[data-search-term *= ' + searchTerm + ']').length > 0 || searchTerm.length < 1) {
					$(this).show();
				} else {
					$(this).hide();
				}
			});
		});*/

	} ); //	end api.bind( 'ready', function() 
	api.control.each( function ( control ) { 
		console.log( control );
		
		/* ... */ } );
	api.bind( 'pane-contents-reflowed', function() {
		console.log( "event.pane-contents-reflowed");
		reflowAccordions();

		if( initialLoad ) {
			$( '#accordion-section-wpcomet_options:visible' ).find( '.accordion-section-title' ).trigger( 'click' );

			$( '.wpct-suba-toggle.wpct-control-title' ).unbind( 'click' ).click( function( e ) {
				e.preventDefault();
				var $sub_accordeon = $( this ).closest( 'li' ).find( '.wpct-control-sub-accordeon' );
				if( $( this ).hasClass( 'topen' ) ) {
					$( this ).removeClass( 'topen' );
					$sub_accordeon.slideUp();
				} else {
					$( this ).addClass( 'topen' );
					$sub_accordeon.slideDown();
				}
			} );

			$('.wpct-control-sub-accordeon').hide();

			$.event.trigger( "wpcomet_customizer_ready" );

			initialLoad = false;
		}
	} );

	
	api.bind( 'auto-hide-tooltips', function() {
		console.log( "hi TT");
		$(document).on('shown.bs.tooltip', function (e) {
		  setTimeout(function () {
			  console.log( "hide TT");
			$(e.target).tooltip('hide');
		  }, 4000);
	   });
	});

	/***************************************************************************
	 *                      WpCometControl Start
	 *
	 * This Model serves as a foundation to derive the other controls.
	 * When initializing an inherited Model, the ready() method must be defined.
	 ***************************************************************************/
	api.WPCTCustomizeControl = api.Control.extend({
		value: {},
		/**
		 * This property must be defined in inherited models.
		 * Must be set to the class of the hidden text field that stores the settings.
		 * Example: .themify_background_control
		 */
		field: '',
		settkey: '',

		isJson: function (str) {
			try {
				JSON.parse(str);
			} catch (e) {
				return false;
			}
			return true;
		},
		
		removeThemeMod: function (id) {
			//	console.log("remm");
		},
		inputTyped: function ($input, key) {
			//	console.log($input);

			var control = this,
			key = $input.attr("id"),
			timer;

			$input.on('keyup', function (control) {
				console.log("typed");
				//control.trigger('change');

				var value = $(this).val();
				clearTimeout(timer);
				timer = setTimeout(function () {
					$.post(
							ajaxurl,
							{
								'action': 'wpcomet_customizer_set_theme_mod',
								'key': key,
								'value': value,
								'nonce': wpcometCustomizerControls.nonce
							},
					function (data) {
						console.log( data );
						if ('saved' === data) {
							// Set timestamp to force live preview reload
							
							api.state('saved');
							//$field.val(JSON.stringify(control.value[control.id])).trigger('change');
						}
					}
					);
				}, 1500);
			});
		},
		beforeReady: function () {
			// remove _ctrl from control key to get setting key and the corresponding input#id
			this.settkey = this.id.slice(0,-5);
			this.input = this.container.find("#" + this.settkey);
			//console.log(  );
			//	console.log( $( this.input).val()  );
			
			// Grab value from the input#id, convert to json object if necessary
			if( $( this.input).val() ) {
				var ctrl_value = $( this.input).val();
/*				console.log( ctrl_value );
				console.log( this.value );
				console.log(  this.isJson(ctrl_value) );
								console.log( $(this).val() );

				console.log( $.parseJSON( ctrl_value )  );*/
				this.value = ( this.isJson(ctrl_value) ) ? $.parseJSON( ctrl_value ) : $(this.input).val();

			}
		},
		colorChange: function( grkey, newval, ctrl_key,  ) {
			var control = this;
			
			control.value[grkey] = newval;
			control.input.val(JSON.stringify(control.value)).trigger('change');
			//$obj.find( 'input.wpctcc-value-field' ).val();
			console.log( control.value );
//			console.log( this.isJson(control.value) );
			console.log( control.field );
			console.log( newval );
			console.log( ctrl_key );
		},
		groupedValChange: function( grkey, newval, ctrl_key,  ) {
			var control = this;
			
			control.value[grkey] = newval;
			control.input.val(JSON.stringify(control.value)).trigger('change');
			//$obj.find( 'input.wpctcc-value-field' ).val();
			console.log( control.value );
//			console.log( this.isJson(control.value) );
			console.log( control.field );
			console.log( newval );
			console.log( ctrl_key );
		},
		
		textChange: function( $obj ) {
			console.log("textChange");
			var control = this;
			//	console.log( $obj.find('input[type=text]') );
			$obj.find('input[type=text]').not('.wpctcc-value-field').bind("keyup change", function(e) {
				control.groupedValChange( $(this).val(), this.attr("id") );
			//	control.groupedValChange( $(this).val(), control.id );
				/*
				console.log( isJson(control.value) );
				control.value[radiogrkey] = selval;
				
				console.log("text input changed");
				console.log( control.input );
				console.log( control.value );
				control.input.val(JSON.stringify(control.value)).trigger('change');
				console.log( control.value );
				console.log( control.value );
				console.log( control.input.value );*/
				//control.input.val(JSON.stringify(control.value)).trigger('change');

			});
		},
		radioChange: function( $obj, group = '' ) {
				var control = this,
				$field = $(control.field, control.container),
				initVal = $obj.find( 'input[type="radio"]:checked' ).val();
				
				var $item = $obj.find(group);
			//	var val = JSON.stringify(control.value[control.id]) : $(this).val();
			// 	var val = JSON.stringify(control.value[control.id]);
			//	console.log( val );
/*			console.log( this.id );
			console.log( control );
			console.log( $obj );
			console.log( control.key );
			console.log( control.field );
			console.log( control.value );*/
			$item.each(function(i) {
				var radio_gr_id = $(this).attr("id");

				$(this).find( 'input[type="radio"]' ).on('click', function () {
					var selval = $(this).val();
					
					if( typeof control.value == "object" ) {
						var radiogrkey = radio_gr_id.replace('inputwrap-' + control.settkey + '_', '');
						//inputwrap-layout_global_container
						console.log( radiogrkey );
					//	console.log( control.value );
						control.value[radiogrkey] = selval;
					//	console.log( control.value );
						
						control.input.val(JSON.stringify(control.value)).trigger('change');
						
					/*	inputwrap-layout_global_container
						control.value[control.id].stamp = new Date().getTime();
						control.input.val
						control.input.val()	*/
						
					/*	
						console.log( selval );
						console.log( radiogrkey );
						console.log( typeof control.value  );
						console.log( control.settkey );
						console.log( control.value );*/
					}
					else {
						control.input.val( selval ).trigger('change');
					}
	
					
	
					//console.log( );
					//var input_id = control.container.data("key");
					//var val = val.
				//	console.log($item.attr("id") );
				//	console.log( control.container.data("key")  );
					//	$(this).data("subsection")
					//console.log(this.val() );
					//	$field.val(JSON.stringify(control.value[control.id])).trigger('change');
					//$field
				});
			});
		}
		//	$field.val(JSON.stringify(control.value[control.id])).trigger('change');

		//wpc-input
		
	});
	//	console.log ( api );

	
	/***************************************************************************
	 * WpCometControl End
	 ***************************************************************************/
	 
/*	 
	api.WPCTCCLayout = api.Control.extend( {
        ready: function () {
            console.log( 'Hello World!' );
        }
    } );

    // `multi_image` is the same as specified in the `$type` property
    // of the control PHP version
    $.extend( api.controlConstructor, {wpctcc_layout: api.WPCTCCLayout} ); */
	
	 // Logo Control
	api.wpctccBrand = api.WPCTCustomizeControl.extend({
	//	field: '.wpctcc-layout',
		ready: function () {
			var control = this;
			console.log ( control.id );
		//	console.log ( control.container );
			control.beforeReady();
			control.radioChange( control.container, '.radio-gr' );
			control.textChange( control.container );
			control.groupedValChange( control.container );
		//	control.input.val(JSON.stringify(control.value)).trigger('change');


		/*	var control = this,
			$field = $(control.field, control.container);
			
			control.setKey();
			control.setVal();
			console.log( this.setting.get() );

			console.log ( control );
			console.log ( "hello" );
			console.log ( control.id );
			console.log ( control.settkey );
			console.log ( control.value );
		console.log ( control );
			console.log ( $field );
			console.log ( control.container );
			console.log ( control.value );*/
			//    $( control.container ).find('.form-group').each(function() {
			//	});
			
		}
	});
	api.controlConstructor.wpctcc_brand = api.wpctccBrand;
	
	// Layout Control
	api.wpctccLayout = api.WPCTCustomizeControl.extend({
	//	field: '.wpctcc-layout',
		ready: function () {
			var control = this;
			//	console.log ( control.id );
			//	console.log ( control.id );
			//	console.log ( control.container );
			control.beforeReady();
			control.radioChange( control.container, '.radio-gr' );

			/*	var control = this,
			$field = $(control.field, control.container);
			
			control.setKey();
			control.setVal();
			console.log( this.setting.get() );

			console.log ( control );
			console.log ( "hello" );
			console.log ( control.id );
			console.log ( control.settkey );
			console.log ( control.value );
			console.log ( control );
			console.log ( $field );
			console.log ( control.container );
			console.log ( control.value );*/
			//    $( control.container ).find('.form-group').each(function() {
			//	});
			
		}
	});
	api.controlConstructor.wpctcc_layout = api.wpctccLayout;
	

	
	// URI Control
	api.wpctccGeneric = api.WPCTCustomizeControl.extend({
		ready: function () {

			//control.textChange( control.container );
			var control = this;
			//type = $(control.input).data("field-type");
			/*	console.log( control );
				console.log( control.id );
				console.log( control.input );
				console.log( control.input );*/
				if( this.hasOwnProperty('input')) {
				//	console.log( control.input );
				//	console.log( control.input.val() );
				}


			control.beforeReady();
			
			control.radioChange( control.container, '.radio-gr' );
			control.inputTyped( control.input );
		}
	});
	api.controlConstructor.wpctcc_generic = api.wpctccGeneric;
	
		/* Debug
	
	// http://riceball.com/observing-the-wordpress-customizer-wp-customize-events/

	//console.log( 'wp.customize in preview frame', wp.customize);
 
    function monitor_events( object_path ) {
        var p = eval(object_path);
        if (p) {
            var k = _.keys(p.topics);
            console.log( object_path + " has events ", k);
            _.each(k, function(a) {
                p.bind(a, function() {
                    console.log( object_path + ' event ' + a, arguments );
                });
            });
        } else {
            console.log(object_path + ' does not exist');
        }
    }
	
    wp.customize.bind('monitor-customizer', function() {
		monitor_events('wp.customize');
		monitor_events('wp.customize.previewer');
		monitor_events('wp.customize.control');
		monitor_events('wp.customize.section');
		monitor_events('wp.customize.panel');
		monitor_events('wp.customize.state');
    });
	*/
	
	
})(wp, jQuery);
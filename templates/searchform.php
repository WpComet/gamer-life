<?php
/**
 * The template for displaying search forms in Underscores.me
 *
 * @package understrap
 */
	//	var_dump( $args );
?>
<form method="get" id="searchform-<?php echo $args["position"]; ?>" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
	<?php if( $args["label"] ) : ?><label class="assistive-text sr-only" for="s"><?php esc_html_e( 'Search', 'gamer-life' ); ?></label><?php endif; ?>
	<div class="input-group">
		<input class="field form-control" id="s" name="s" type="text"
			placeholder="<?php esc_attr_e( 'Search &hellip;', 'bootstrap-ultimate' ); ?>">
		<span class="input-group-btn">
			<input class="submit btn btn-primary" id="searchsubmit" name="submit" type="submit"
			value="<?php esc_attr_e( 'Search', 'bootstrap-ultimate' ); ?>">
	</span>
	</div>
</form>

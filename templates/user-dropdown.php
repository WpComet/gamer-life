<?php
/**
 * User dropdown template
 */
 ?>
 <li class="nav-item dropdown">
	<a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="javascript:void(0)" role="button" aria-haspopup="true" aria-expanded="false">
	<?php 
	$avatar = get_avatar( wpct_curruser("ID"), 24, '', 'wpct_curruser("user_email")', array("class" => "img-avatar") );
	echo $avatar;
	//var_dump( wpct_curruser() );
	 ?>
		<span class="d-md-down-none"><?php echo wpct_curruser("display_name"); ?></span>
	</a>
	<ul class="dropdown-menu dropdown-menu-right">

		<div class="dropdown-header text-center">
			<strong>Account</strong>
		</div>
		<li><?php echo wpct_loginout($_SERVER['REQUEST_URI'], false ) ?></li>
	</ul>
</li>
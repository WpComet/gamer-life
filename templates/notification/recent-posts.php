<?php
/**
 * Notifications template
 *
 */
$notif = wpct_get_mod( "navbar_notifications" );
//var_dump( wpct_get_recent_comments() );
switch ($notif) {
    case "recent-posts":
        $items = wp_get_recent_posts();
		$title = "post_title";
		$id = "ID";
    break;
    case "recent-comments":
        $items = wpct_get_recent_comments();
        $title = "comment_content";
		$id = "comment_ID";

    break;
    default:
        $count = false;
    break;
};
$count = count( $items );

if( ! $count ) return false;
?>
<li class="nav-item d-md-down-none">
    <a class="nav-link dropdown-toggle" href="javascript:void(0)" role="button" id="notifDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icon-bell"></i><span class="badge badge-pill badge-danger"><?php echo $count ?></span></a>
    
    <ul class="dropdown-menu" aria-labelledby="notifDropdown">
    	<?php foreach ( $items as $item ) : 
			switch ($notif) {
   				case "recent-comments":
					$item = (array) $item;

        			$link = get_comment_link( $item[$id] );
				break;
				default:
					$link = get_permalink( $item[$id] );
				break;
			};
		?>

        <li><a href="<?php echo $link ?>" class="dropdown-item"><?php echo wpct_truncate(wp_strip_all_tags($item[$title] , true), 4); ?></a></li>
		<?php endforeach; ?>
    </ul>
</li>
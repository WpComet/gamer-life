<?php
/**
 * Jumbotron template.
 *
 * @version		1.0.0
 * @package		WPCT/Templates
 * @category	Template
 * @author 		WpComet
 */
 
//	var_dump( $args );
?>
<div class="jumbotron">
	<?php if( $args["title"] ) : ?>
	<h1 class="display-3"><?php echo $args["title"] ?></h1>
    <?php endif; ?>
    
   	<?php if( $args["content"] ) : ?>
	<div class="lead"><?php echo $args["content"] ?></div>
    <?php endif; ?>
       
	<hr class="my-4">
    <?php if( $args["detail"] ) : ?>
	<div class="detail"><?php echo $args["detail"] ?></div>
    <?php endif; ?>
    
    <?php if( $args["buttontext"] ) : 
		$buttonuri = ( $args["buttonurl"] ) ?  $args["buttonurl"] : "#"; ?>
    <p class="lead">
        <a class="btn btn-primary btn-lg" href="<?php echo $buttonuri ?>" role="button"><?php echo $args["buttontext"] ?></a>
    </p>
    <?php endif; ?>
</div>

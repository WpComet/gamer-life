<?php
/**
 * The template for displaying search forms in Underscores.me
 *
 * @package understrap
 */
 
//	var_dump( $args );
$brand = wpct_get_mod("brand");
//$logotext = isset( $brand["text"] ) ? $brand["text"] : get_bloginfo( 'name' );
$logotype = wpct_get_mod("brand_type");
$logotext = wpct_get_mod("brand_text");
//	var_dump( $logotype );
?>

<?php	if( $logotype == "image" ) :
//$logoimg = ( isset( $logo_args->image ) ) ? $logo_args->image : false;
$logoimg = wpct_get_mod("brand_image");
//$logoimg = $brand["image"];	
$is_image = ( ! $logoimg ) ? false : preg_match( '/(^.*\.jpg|jpeg|png|gif*)/i', $logoimg );
?>
	<img src="<?php echo $logoimg ?>" alt="<?php echo $logotext ?>" class="img-fluid" />
<?php else : ?>
	<span class="h2 site-title"><?php echo $logotext ?></span>
<?php endif; ?>
<?php
/**
 * Breadcrumbs Template
 *
 */
 
 // Get the query & post information
global $post,$wp_query;  

// Settings
$separator          = '';
$breadcrums_id      = 'breadcrumbs';
$breadcrums_class   = 'breadcrumbs';
$home_title         = '<i class="fa fa-home" aria-hidden="true"></i> Home';
  
// If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
$custom_taxonomy    = 'product_cat';
?>

<ol class="breadcrumb"> 
	<?php // Home page ?>
	<li class="breadcrumb-item item-home"><a class="bread-link bread-home" href="<?php echo get_home_url() ?>" title="Homepage"><?php echo $home_title ?></a></li>
    
    <?php if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) { ?>	  
		<li class="breadcrumb-item item-current active item-archive"><strong class="bread-current bread-archive"><?php echo get_the_archive_title() ?></strong></li>
	<?php } elseif ( is_archive() && is_tax() && !is_category() && !is_tag() )  {
		// If post is a custom post type 
		$post_type = get_post_type();
		  
		// If it is a custom post type display name and link
		if($post_type != 'post') {
			$post_type_object = get_post_type_object($post_type);
			$post_type_archive = get_post_type_archive_link($post_type);
			?>
			<li class="breadcrumb-item item-cat item-custom-post-type-<?php echo $post_type ?>">
				<a class="bread-cat bread-custom-post-type-<?php echo $post_type ?>" href="<?php echo $post_type_archive ?>" title="<?php echo $post_type_object->labels->name ?>">
				<?php echo $post_type_object->labels->name ?>
				</a>
			</li>
			<?php $custom_tax_name = get_queried_object()->name; ?>
			<li class="breadcrumb-item item-current active item-archive"><strong class="bread-current bread-archive"><?php echo $custom_tax_name ?></strong></li>
		<?php } ?>
    <?php } elseif ( is_single() ) {
		// If post is a custom post type
		$post_type = get_post_type();	  
		// If it is a custom post type display name and link
		if($post_type != 'post') : 
			$post_type_object = get_post_type_object($post_type);
			$post_type_archive = get_post_type_archive_link($post_type);
			?>
			<li class="breadcrumb-item item-cat item-custom-post-type-<?php echo $post_type ?>">
            	<a class="bread-cat bread-custom-post-type-<?php echo $post_type ?>" href="<?php echo $post_type_archive ?>" title="<?php echo $post_type_object->labels->name ?>">
				<?php echo $post_type_object->labels->name ?>
                </a>
            </li> 
		<?php endif; ?>   
		<?php 
		// Get post category info
		$category = get_the_category();
		 
		if( !empty($category) ) {	  
			$cat_vals = array_values($category);
			// Get last category post is in
			$last_category = end($cat_vals);
			  
			// Get parent any categories and create array
			$get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
			$cat_parents = explode(',',$get_cat_parents);
			  
			// Loop through parent categories and store in variable $cat_display
			$cat_display = '';
			foreach($cat_parents as $parents) {
				$cat_display .= '<li class="breadcrumb-item item-cat">'.$parents.'</li>';
			}
		}
		  
		// If it's a custom post type within a custom taxonomy
		$taxonomy_exists = taxonomy_exists($custom_taxonomy);
		if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {	   
			$taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
			$cat_id         = $taxonomy_terms[0]->term_id;
			$cat_nicename   = $taxonomy_terms[0]->slug;
			$cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
			$cat_name       = $taxonomy_terms[0]->name;	   
		}
		  
		// Check if the post is in a category
		if( ! empty( $last_category ) ) {
			echo $cat_display; ?>
			<li class="breadcrumb-item item-current active item-<?php echo $post->ID ?>">
            	<strong class="bread-current bread-<?php echo $post->ID ?>" title="<?php echo get_the_title() ?>"><?php echo get_the_title() ?></strong>
            </li>
		<?php } 
		elseif ( !empty($cat_id) ) {
		// Else if post is in a custom taxonomy
		?>
			<li class="breadcrumb-item item-cat item-cat-<?php echo $cat_id ?> item-cat-<?php echo $cat_nicename ?>">
            	<a class="bread-cat bread-cat-<?php echo $cat_id ?> bread-cat-<?php echo $cat_nicename ?>" href="<?php echo $cat_link ?>" title="<?php echo $cat_name ?>"><?php echo $cat_name ?></a>
            </li>
			<li class="breadcrumb-item item-current active item-<?php echo $post->ID ?>">
            	<strong class="bread-current bread-<?php echo $post->ID ?>" title="<?php echo get_the_title() ?>"><?php echo get_the_title() ?></strong>
            </li> 
		<?php } else { ?>		  
			<li class="breadcrumb-item item-current active item-<?php echo $post->ID ?>">
            	<strong class="bread-current bread-<?php echo $post->ID ?>" title="<?php echo get_the_title() ?>"><?php echo get_the_title() ?></strong>
            </li>	  
		<?php } ?>
	<?php } elseif ( is_category() ) {
		// Category page ?>
		<li class="breadcrumb-item item-current active item-cat"><strong class="bread-current bread-cat"><?php echo single_cat_title('', false) ?></strong></li>
		   
	<?php } elseif ( is_page() ) { 
		// Standard page
		if( $post->post_parent ) { 
			   
			// If child page, get parents 
			$anc = get_post_ancestors( $post->ID );
			   
			// Get parents in the right order
			$anc = array_reverse($anc);
			   
			// Parent page loop
			if ( !isset( $parents ) ) $parents = null;
			foreach ( $anc as $ancestor ) {
				$parents .= '<li class="breadcrumb-item item-parent item-parent-<?php echo $ancestor ?>"><a class="bread-parent bread-parent-<?php echo $ancestor ?>" href="<?php echo get_permalink($ancestor) ?>" title="<?php echo get_the_title($ancestor) ?>"><?php echo get_the_title($ancestor) ?></a></li>';
			}
			   
			// Display parent pages
			echo $parents;
			   
			// Current page
			?>
			<li class="breadcrumb-item item-current active item-<?php echo $post->ID ?>"><strong title="<?php echo get_the_title() ?>"> <?php echo get_the_title() ?></strong></li>
			<?php } else {
			// Just display current page if not parents ?>
			<li class="breadcrumb-item item-current active item-<?php echo $post->ID ?>"><strong class="bread-current bread-<?php echo $post->ID ?>"> <?php echo get_the_title() ?></strong></li>   
		<?php } ?>   
	<?php } elseif ( is_tag() ) { 
		// Tag page
		   
		// Get tag information
		$term_id        = get_query_var('tag_id');
		$taxonomy       = 'post_tag';
		$args           = 'include=' . $term_id;
		$terms          = get_terms( $taxonomy, $args );
		$get_term_id    = $terms[0]->term_id;
		$get_term_slug  = $terms[0]->slug;
		$get_term_name  = $terms[0]->name;
		   
		// Display the tag name
		?>
		<li class="breadcrumb-item item-current active item-tag-<?php echo $get_term_id ?> item-tag-<?php echo $get_term_slug ?>">
        	<strong class="bread-current bread-tag-<?php echo $get_term_id ?> bread-tag-<?php echo $get_term_slug ?>"><?php echo $get_term_name ?></strong>
        </li>
	   
	<?php } elseif ( is_day() ) { 
		// Day archive ?>
 
		<?php // Year link ?>
		<li class="breadcrumb-item item-year item-year-<?php echo get_the_time('Y') ?>"><a class="bread-year bread-year-<?php echo get_the_time('Y') ?>" href="<?php echo get_year_link( get_the_time('Y') ) ?>" title="<?php echo get_the_time('Y') ?>"><?php echo get_the_time('Y') ?> Archives</a></li>
		   
		<?php // Month link ?>
		<li class="breadcrumb-item item-month item-month-<?php echo get_the_time('m') ?>"><a class="bread-month bread-month-<?php echo get_the_time('m') ?>" href="<?php echo get_month_link( get_the_time('Y'), get_the_time('m') ) ?>" title="<?php echo get_the_time('M') ?>"><?php echo get_the_time('M') ?> Archives</a></li>
		   
		<?php // Day display ?>
		<li class="breadcrumb-item item-current active item-<?php echo get_the_time('j') ?>"><strong class="bread-current bread-<?php echo get_the_time('j') ?>"> <?php echo get_the_time('jS') ?> <?php echo get_the_time('M') ?> Archives</strong></li>
		   
	<?php } elseif ( is_month() ) { 
		// Month Archive ?>
		   
		<?php // Year link ?>
		<li class="breadcrumb-item item-year item-year-<?php echo get_the_time('Y') ?>"><a class="bread-year bread-year-<?php echo get_the_time('Y') ?>" href="<?php echo get_year_link( get_the_time('Y') ) ?>" title="<?php echo get_the_time('Y') ?>"><?php echo get_the_time('Y') ?> Archives</a></li>
		   
		<?php // Month display ?>
		<li class="breadcrumb-item item-month item-month-<?php echo get_the_time('m') ?>"><strong class="bread-month bread-month-<?php echo get_the_time('m') ?>" title="<?php echo get_the_time('M') ?>"><?php echo get_the_time('M') ?> Archives</strong></li>
		   
	<?php } elseif ( is_year() ) { 
		// Display year archive ?>
		<li class="breadcrumb-item item-current active item-current active-<?php echo get_the_time('Y') ?>"><strong class="bread-current bread-current-<?php echo get_the_time('Y') ?>" title="<?php echo get_the_time('Y') ?>"><?php echo get_the_time('Y') ?> Archives</strong></li>
		   
	<?php } elseif ( is_author() ) { 
		// Auhor archive
		   
		// Get the author information
		global $author;
		$userdata = get_userdata( $author );
		// Display author name ?>
		<li class="breadcrumb-item item-current active item-current active-<?php echo $userdata->user_nicename ?>">
        	<strong class="bread-current bread-current-<?php echo $userdata->user_nicename ?>" title="<?php echo $userdata->display_name ?>">Author: <?php echo $userdata->display_name ?></strong>
        </li>
	   
	<?php } elseif ( get_query_var('paged') ) { 
		// Paginated archives
		?>
		<li class="breadcrumb-item item-current active item-current active-<?php echo get_query_var('paged') ?>">
        	<strong class="bread-current bread-current-<?php echo get_query_var('paged') ?>" title="Page <?php echo get_query_var('paged') ?>"><?php _e('Page') ?> <?php echo get_query_var('paged') ?></strong>
        </li>
		   
	<?php } elseif ( is_search() ) { 
		// Search results page ?>
		<li class="breadcrumb-item item-current active item-current active-<?php echo get_search_query() ?>">
        	<strong class="bread-current bread-current-<?php echo get_search_query() ?>" title="Search results for: <?php echo get_search_query() ?>">Search results for: <?php echo get_search_query() ?></strong>
        </li>
	   
	<?php } elseif ( is_404() ) { 
		// 404 page ?>
		<li>Error 404</li>
	<?php } ?>
</ol>
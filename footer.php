<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

?>

<div class="wrapper app-footer" id="wrapper-footer">

	<div class="<?php // echo esc_attr( $container ); ?>">

		<div class="row">

			<div class="col-md-12">

				<footer class="site-footer" id="colophon">
	<?php // var_dump( wpct_get("footer_style") ); ?>

					<div class="site-info">

							<a href="<?php  echo esc_url( __( 'http://wordpress.org/','understrap' ) ); ?>"><?php printf( 
							/* translators:*/
							esc_html__( 'Proudly powered by %s', 'understrap' ),'WordPress' ); ?></a>
								<span class="sep"> | </span>
					
							<?php printf( // WPCS: XSS ok.
							/* translators:*/
								esc_html__( 'Theme: %1$s by %2$s.', 'gamer-life' ), wpct_theme_get( 'name' ),  '<a href="'.esc_url( __('https://wpcomet.com', 'gamer-life')).'">WpComet</a>' ); ?> 
				
							(<?php printf( // WPCS: XSS ok.
							/* translators:*/
								esc_html__( 'Version: %1$s', 'gamer-life' ), wpct_theme_get( 'version' ) ); ?>)
					</div><!-- .site-info -->

				</footer><!-- #colophon -->

			</div><!--col end -->

		</div><!-- row end -->

	</div><!-- container end -->

</div><!-- #wrapper-footer end -->


<?php if( wpct_theme_get("sub_theme") != "coreui" ) : ?></div><!-- #page --><?php endif;?>

<?php wp_footer(); ?>

</body>

</html>


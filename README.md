# SEE: [Official Demo](https://demo.wpcomet.com/gamer-life) | Read: [Official Docs Page](https://docs.wpcomet.com/gamer-life/)

## Requirements ##
### Min. required WordPress version: 3.5.2
- PHP & MySQL etc. requirements are the same as that of the WP version you are running unless stated otherwise.

## Recommendations ##
### Recommended WordPress version: 4.0

## How do I get set up? ##
### Install the theme
- Upload the theme via FTP or WP admin interface
- Activate the theme
- Go to customizer using the activation notice or Customizer > Gamer Life
- Customize away !

## Page Templates
- Blank

## Contribution guidelines ##
### Submit an Idea
### Submit a problem
### Contributing / Getting Involved

## Who do I talk to? ##
- All contact details at wpcomet.com

## Changelog
See [changelog](CHANGELOG.md)

## Credits & Thanks
- Font Awesome: http://fontawesome.io/license (Font: SIL OFL 1.1, CSS: MIT License)
- Bootstrap: http://getbootstrap.com | https://github.com/twbs/bootstrap/blob/master/LICENSE (Code licensed under MIT documentation under CC BY 3.0.)
- jQuery: https://jquery.org | (Code licensed under MIT)
- WP Bootstrap Navwalker by Edward McIntyre: https://github.com/twittem/wp-bootstrap-navwalker | GNU GPL
- Bootstrap Gallery Script based on Roots Sage Gallery: https://github.com/roots/sage/blob/5b9786b8ceecfe717db55666efe5bcf0c9e1801c/lib/gallery.php
- And all the other vendors whose names are not listed here but available under /vendors !
<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */
 
global $wpcomet_theme;
$theme_mods = wpct_get_mods();
$view = $wpcomet_theme->view;
$is_home = in_array( $view, array('home-static','home') );
$sidebar_pref = wpct_get_layout_el("sidebar");
$has_sidebar_primary = $sidebar_pref != "nosb";
$has_sidebar_secondary = in_array( $sidebar_pref, array("sbrr","sbrl","sbll") );
get_header();
//	var_dump( $is_home );
?>

<div class="wrapper<?php echo wpct_class_attr("indexwrap") ?>" id="wrapper-index">
	
	<?php if( in_array( $sidebar_pref,array("sbl","sbrl","sbll") ) ) : ?>
        <?php wpct_get_sidebar( 'primary' ); ?>
        <?php if( $sidebar_pref == "sbll" ) : ?>
            <?php wpct_get_sidebar( 'secondary' ); ?>
        <?php endif; ?>
    <?php endif; ?>
        
	<main class="main tse-scrollable" id="site-main" tabindex="-1">
        <div class="site-main tse-content" id="content">
            <?php if( wpct_get("breadcrumbs_show") ) : ?>
            <?php get_template_part( 'templates/breadcrumbs', 'none' ); ?>
            <?php endif; ?>             
			<?php if ( $is_home  ) : ?>
            <!-- Showcase Area begin --> 
			<div class="row" id="featurerow">
				<?php the_widget( 'WPCT_Widget_Jumbotron', array(
						'content' => sprintf( __( 'You are seeing this because you have no widgets in <code>%s</code> widget area.', 'gamer-life' ), 'Showcase - Primary' ),
						'detail' => sprintf( __( 'Place a widget in <code>%s</code> widget area or simply turn off Demo Mode in theme settings.', 'gamer-life' ), 'Showcase - Primary' )
                	)
                 );
                 
                // wpct_edit_link("demo_mode");
                ?> 
                <div id="intro-primary" class="intro widget-area" role="complementary">
				<?php if ( is_active_sidebar( 'intro_primary' ) ) : ?>
                    
                        <?php dynamic_sidebar( 'intro_primary' ); ?>

                <?php elseif ( is_active_sidebar( 'intro_primary' ) ) : ?>
                    </div><!-- #primary-sidebar -->
                <?php endif; ?>
                    <?php get_template_part( 'templates/hero' ); ?>
                    <?php get_template_part( 'templates/slider' ); ?>
            </div><!-- Showcase end #featurerow -->
			<?php endif; ?>
                      
            <div id="loop-wrapper" class="wrapper">

            <?php WPCT_Interface::gen_field( 'categories',	array(
			"type" => "taxonomy",
			//"name" => $this->get_field_name( 'content' ),
			"label" => __( 'Categories', 'gamer-life' ),
			"value" => isset( $instance[ 'content' ] ) ? $instance[ 'content' ] : $defaults['content'],
			"misc" => array(
				"tax" => "category"
			)

        ));
        ?>
                <?php if ( have_posts() ) : ?>

                    <?php /* Start the Loop */ ?>
                    <?php while ( have_posts() ) : the_post(); ?>
                        <?php
                        /*
                         * Include the Post-Format-specific template for the content.
                         * If you want to override this in a child theme, then include a file
                         * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                         */
                        
                        $content = ( $view != '' ) ? $view : get_post_format();
                        get_template_part( 'templates/loop/content', $content );
                        ?>

                    <?php endwhile; ?>

                <?php else : ?>
                    <?php get_template_part( 'templates/loop/content', 'none' ); ?>
                <?php endif; ?>
            </div>
            
            <!-- The pagination component -->
            <?php get_template_part( 'templates/pagination' ); ?>

        </div><!-- #content -->
        
    	<?php if( $has_sidebar_primary ) : ?><a class="navbar-toggler sidebar-toggler mobile-sidebar-toggler" href="javascript:void(0)"><i class="fa fa-caret-left" aria-hidden="true"></i></a><?php endif;?>
    	<?php if( $has_sidebar_secondary ) : ?><a class="navbar-toggler aside-menu-toggler mobile-aside-menu-toggler" href="javascript:void(0)"><i class="fa fa-caret-right" aria-hidden="true"></i></a><?php endif;?>
    </main><!-- .main#site-main -->
    

    <?php if( in_array( $sidebar_pref,array("sbr","sbrr","sbrl") ) ) : ?>
        <?php if( $sidebar_pref == "sbr" ) : ?>
            <?php  wpct_get_sidebar( 'primary' ); ?>
        <?php else : ?>
            <?php wpct_get_sidebar( 'secondary', array(	'class' => ' aside-menu','tag' => 'aside') ); ?>
        <?php endif; ?>
    <?php endif; ?>


    <?php //  wpct_debugdump() ?>
</div><!-- #wrapper-index.wrapper end -->

<?php get_footer(); ?>
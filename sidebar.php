<?php
/**
 * Sidebar template.
 */
//var_dump( $args );
?>
<?php if(!post_password_required()):?>

<?php wpct_sidebar_before(); // hook ?>

	<div id="sidebar-<?php echo $args["name"] ?>" class="widget-area tse-scrollable <?php echo $args["class"] ?>" role="complementary">
		<div class="sidebar-content px-2 py-1 tse-content">
        <?php wpct_sidebar_start(); // hook ?>

		<?php if ( ! dynamic_sidebar( 'sidebar-' . $args["name"]  ) ) : ?>
        	<?php echo wpct_get_default_widgets( 'sidebar-' . $args["name"] ) ?>

		<?php endif; ?>

        <?php wpct_sidebar_end(); // hook ?>
		</div>
    </div><!-- /#sidebar-end -->

    <?php wpct_sidebar_after(); // hook ?>
<?php endif;?>